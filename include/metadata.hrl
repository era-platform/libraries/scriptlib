%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc

%% ====================================================================
%% Define types
%% ====================================================================

-type(jsonvalue() :: binary() | integer() | string() | boolean()).
-type(jsonobject() :: [{Key::binary(), Value::jsonvalue() | jsonobject() | jsonlist()}]).
-type(jsonlist() :: [Value::jsonvalue() | jsonobject() | jsonlist()]).
-type(defaultvalue() :: null | undefined | integer() | string()).
-type(compmetadata() :: [{JsonKey::binary(),MapDescr::map()}]).

-define(AllVarTypes,255).
-define(AllArgTypes,255).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(SCR_META_COMPONENTS, scriptlib_sm_metadata_components).
-define(SCR_META_DYNLISTS, scriptlib_sm_metadata_dynamic_list).
-define(SCR_META_EXPR, scriptlib_sm_metadata_expr).
-define(SCR_META_FUNS, scriptlib_sm_metadata_funs).
-define(SCR_META_UTILS, scriptlib_sm_metadata_utils).
-define(SCR_META_TRANS_VERIFY, scriptlib_sm_metadata_translator_verify).


