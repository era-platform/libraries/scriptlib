%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc

%% ====================================================================
%% Define types
%% ====================================================================

% ----------------------
-record(meta, {
    functions :: list(), % of {funname::binary(), F::function()} % custom expression functions
    components :: [{Code::integer(), Module::atom()}],           % custom mapping to component modules
    scripts :: function(),                                       % custom function to find embedded script
    script_opts :: function(),                                   % RP-1460 options of script item in dc (loglevel, etc)
    %
    scripts_prior :: map(),           % 201120 to start svc async scripts with same search function
    %
    monitor :: map(),                  % monitor to notify of script general events by external functions, typed by event_keys (ex. script_start, script_stop, component_start for queue)
    fun_res :: function(),             % RP-1220 function to send messages to owner pid by some components (ex. Operation)
    callback_funs :: map()             % RP-1481 function to handle every component before start in external logic of owner and change State (fun(Type::integer(),State::#state_scr{}) -> NewState#state_scr{}).
}).

-record(variable, {
    id,
    name,
    type,
    location,
    value
}).

-record(component, {
    id,
    type,
    module,
    data,
    name,
    state
}).

-record(state_scr, {
    version=1,
    domain :: binary(), % owner domain
    scriptid :: binary(), % id of executor (rDlg-006-ABCDEF)
    type :: ivr|svc|chatbot,
    loglevel = 0 :: integer(), % 0..3
    limits :: map(), % RP-501
    %
    script, % script metadata
    code, % code of script
    scriptstarttime, % current script start time
    objects, %
    variables = [], % local variable values
    active_component, % current component
    cref, % unique internal component reference
    queue = [], % queue of messages
    stack = [], % stack of scripts
    store, % dict of local data
    %
    meta :: #meta{}, % scripts, components, getters, setters
    selfpid, % pid of scriptmachine
    ownerpid, % pid of owner
    monitor_ref, % monitor owner pid ref
    %
    %trace_counter = 0, % count of components
    %trace_script = [], % full scripts trace
    %trace_comp = [], % full components trace
    %
    map, % #{app, dlgnum, init_callid, opts, store_data, last_out_callid_idx, timer_migration_ref, cb_funs}
    fun_events = undefined, % to notify direct owner
    %
    ref, % unique internal reference
    %
    starttime, % scriptmachine start time
    finaltime, % scriptmachine fin time
    %
    timersec_funs_acc = [] :: list(), % {function/1,Acc} list to exec every second and accumulate
    timersec_ref, % timersec ref
    %
    stopfuns = [] :: list(), % functions/0 to exec on stop
    stopreason, % reason of fin
    post_branch = false :: boolean(), % component start with type stopping launched
    terminating = false :: false | {true, stop} | {true, down}, % for async terminate
    terminating_timer = undefined :: undefined | reference(),
    %
    cross_messages = [] :: [{FromScriptRef::binary(), Message::tuple()}],
    %
    external_data :: term()  % RP-1481
}).

%% expression cmd internal record
-record(cmd_state,{action,
    object,
    property,
    params,
    default,
    timeout=3000
}).

-record(scrmonitor,{pid,
    id,
    type,
    scrcode,
    name,
    domain,
    ownerpid,
    starttime,
    extra = #{}
}).

-define(ScrMonEtsKey, 'scriptlib_monitor').

-type state_scr() :: #state_scr{}.
-type variable() :: #variable{}.
-type component() :: #component{}.
-type encstring() :: {binary(), binary()}.
-type argvalue() :: integer() | float() | encstring().

-define(Post_Normal,normal). % normal
-define(Post_Stop,stop).
-define(Post_TransferNotSet,transfer_not_set). % transfer not set
-define(Post_ComponentNotFound,component_not_found). % transfer set to unknown id
-define(Post_ModuleNotFound,module_not_found). % unknown component type
-define(Post_Down,owner_down). % owner pid found down
-define(Post_Timeout,timeout). % timeout of execution
-define(Post_MaxCount,maxcount). % limit of coumponents
-define(Post_MaxDuration,maxduration). % limit of execution duration
-define(Post_Error,error). % any other exception

%% ====================================================================
%% Define modules
%% ====================================================================

-define(SCRSRV, scriptlib_sm_srv).

-define(SCR_COMP, scriptlib_sm_components).

-define(SCRU, scriptlib_sm_utils).

-define(SCR_VAR, scriptlib_sm_vars).
-define(SCR_ARG, scriptlib_sm_args).

-define(SCR_EXPR, scriptlib_sm_expressions).
-define(SCR_EXPRU, scriptlib_sm_expression_utils).
-define(SCR_EXPRF, scriptlib_sm_expression_funs).
-define(SCR_EXPR_COMP, scriptlib_sm_expression_component).
-define(SCR_EXPR_STORAGE, scriptlib_sm_expression_storage).
-define(JSON, scriptlib_sm_expression_json_modify).

-define(SCR_LIMIT, scriptlib_sm_limit).

-define(SCR_FILE, scriptlib_sm_file).

-define(SCR_METADATA, scriptlib_sm_metadata_build).
-define(SCR_COMPONENTS_DEFAULT, scriptlib_sm_metadata_components_default).

%% ====================================================================
%% Define trace logs
%% ====================================================================

-define(SCR_CRASH(S,Fmt,Args), scriptlib_sm_tracer:log_crash(S,Fmt,Args)).
-define(SCR_CRASH(S,Text), scriptlib_sm_tracer:log_crash(S,Text)).

-define(SCR_ERROR(S,Fmt,Args), scriptlib_sm_tracer:log_error(S,Fmt,Args)).
-define(SCR_ERROR(S,Text), scriptlib_sm_tracer:log_error(S,Text)).

-define(SCR_WARN(S,Fmt,Args), scriptlib_sm_tracer:log_warning(S,Fmt,Args)).
-define(SCR_WARN(S,Text), scriptlib_sm_tracer:log_warning(S,Text)).

-define(SCR_INFO(S,Fmt,Args), scriptlib_sm_tracer:log_info(S,Fmt,Args)).
-define(SCR_INFO(S,Text), scriptlib_sm_tracer:log_info(S,Text)).

-define(SCR_TRACE(S,Fmt,Args), scriptlib_sm_tracer:log_trace(S,Fmt,Args)).
-define(SCR_TRACE(S,Text), scriptlib_sm_tracer:log_trace(S,Text)).

-define(SCR_DEBUG(S,Fmt,Args), scriptlib_sm_tracer:log_debug(S,Fmt,Args)).
-define(SCR_DEBUG(S,Text), scriptlib_sm_tracer:log_debug(S,Text)).
