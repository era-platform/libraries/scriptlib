%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc Script variable routines

-module(scriptlib_sm_vars).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([find/2,
         get_type/2,
         %
         get/2,
         get_value/3, get_value_implicitly/2,
         set_value/3, set_value/4, set_value_implicitly/3,
         extract_local/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------------
%% find variable record by id or variable props
%% ------------------------------------------
-spec find(V::binary()|list(), State::state_scr()) -> undefined | #variable{}.
%% ------------------------------------------
find(Id, #state_scr{variables=Vars}) when is_binary(Id); is_atom(Id) ->
    case lists:keyfind(Id, #variable.id, Vars) of
        false -> undefined;
        #variable{}=Var -> Var
    end;
find(V, #state_scr{}=State) when is_list(V) ->
    case lists:keyfind(<<"id">>,1,V) of
        false -> undefined;
        {_,Id} -> get(Id,State)
    end.

%% ------------------------------------------
%% get type of variable
-spec get_type(V::binary()|list(), State::state_scr()) -> undefined | {ok, Value::argvalue()|undefined}.
%% ------------------------------------------
get_type(V, State) ->
    case find(V, State) of
        undefined ->
            undefined;
        #variable{type=T} -> T
    end.

%% ------------------------------------------
%% get (by variable)
-spec get(V::binary()|list(), State::state_scr()) -> undefined | {ok, Value::argvalue()|undefined}.
%% ------------------------------------------
get(V, State) when is_binary(V); is_list(V) ->
    case find(V, State) of
        undefined ->
            %% ?SCR_TRACE:log_warning(State, "get: Variable '~500tp' not found", [V]),
            undefined;
        #variable{}=Var -> get(Var,State)
    end;
get(#variable{location='local',value=Value},_State) -> {ok, Value};
get(#variable{location=_Type,name=_Name},_State) ->
    % TODO: non local variable types
    {ok, undefined}.

%% ------------------------------------------
%% get (by variable, default if not found)
-spec get_value(V::binary()|variable(), Default::term(), State::#state_scr{}) -> Res::argvalue()|term().
%% ------------------------------------------
get_value(V, Default, #state_scr{}=State) ->
    case get(V, State) of
        undefined -> Default;
        {ok,undefined} ->
            %% ?SCR_TRACE:log_warning(State, "get_value: Variable '~500tp' undefined", [V]),
            Default;
        {ok,Value} -> Value
     end.

%% ------------------------------------------
-spec get_value_implicitly(PropertyName::binary(), State::state_scr()) -> {ok, Value::argvalue()|undefined} | error.
%% ------------------------------------------
get_value_implicitly(PropertyName, #state_scr{variables=Vars}) when is_binary(PropertyName) ->
    case lists:keyfind(PropertyName, #variable.name, Vars) of
        #variable{location='local',value=Value} -> {ok, Value};
        _ ->
            %% ?SCR_TRACE:log_warning(State, "get_value: Property '~500tp' not found", [PropertyName]),
            error
    end.

%% ------------------------------------------
%% set (by variable)
%% ------------------------------------------
-spec set_value(Id::binary()|list(), Value::argvalue()|binary(), State::state_scr()) -> {ok, State1::state_scr()} | error.
%% ------------------------------------------
set_value(<<>>, _Value, State) -> {ok, State};
set_value(V, Value, #state_scr{variables=Vars}=State)
  when is_binary(Value)
  orelse (is_tuple(Value) andalso tuple_size(Value)==2 andalso is_binary(element(1,Value)) andalso is_binary(element(2,Value)))
  orelse is_number(Value)
  orelse Value==undefined ->
    case find(V,State) of
        undefined ->
            %% ?SCR_TRACE:log_warning(State, "set_value: Variable '~500tp' not found", [V]),
            error;
        #variable{id=Id,location='local'}=Var ->
            %% ?SCR_TRACE:trace(State, "set_value: local var '~500tp' = ~500tp", [V, Value]),
            {ok,State#state_scr{variables=lists:keystore(Id,#variable.id,Vars,Var#variable{value=update_value(Var,Value)})}};
        #variable{location=_Type,name=_Name}=_Var ->
            %% ?SCR_TRACE:trace(State, "set_value: store:~500tp:~500tp var '~500tp' = ~500tp", [Type, Name, V, Value]),
            % TODO: non local variable types
            {ok, State}
    end.

%% ------------------------------------------
%% set (by component)
%% ------------------------------------------
-spec set_value(PropertyKey::binary(), Value::argvalue()|binary(), CompData::list(), State::state_scr()) -> {ok, State1::state_scr()} | error.
%% ------------------------------------------
set_value(PropertyKey, Value, CompData, State) when is_binary(PropertyKey) ->
    case lists:keyfind(PropertyKey, 1, CompData) of
        false ->
            %% ?SCR_TRACE:log_warning(State, "set_value: Property '~500tp' not found", [PropertyKey]),
            {ok, State};
        {_,Var} ->
            set_value(Var, Value, State)
    end.

%% ------------------------------------------
-spec set_value_implicitly(PropertyName::binary(), Value::argvalue()|binary(), State::state_scr()) -> {ok, State1::state_scr()} | error.
%% ------------------------------------------
set_value_implicitly(PropertyName, Value, #state_scr{variables=Vars}=State) when is_binary(PropertyName) ->
    case lists:keyfind(PropertyName, #variable.name, Vars) of
        #variable{location='local',id=Id} -> set_value(Id, Value, State);
        _ ->
            %% ?SCR_TRACE:log_warning(State, "set_value: Property '~500tp' not found", [PropertyName]),
            error
    end.

%% ------------------------------------------
-spec extract_local(State::state_scr()) -> [{Id::binary(), Value::argvalue()|binary()}].
%% ------------------------------------------
extract_local(#state_scr{variables=Vars}=_State) ->
    lists:foldl(fun(#variable{location='local',id=Id,name=Name,value=Val}, Acc) -> [{Id,Name,Val}|Acc];
                   (_, Acc) -> Acc
                end, [], Vars).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private @vartype
update_value(#variable{type='numeric'}=Var, {BVal,_Enc}) when is_binary(BVal) -> update_value(Var,BVal);
update_value(#variable{type='numeric'}, Value) ->
    case catch ?SCR_EXPRU:n(Value) of
        {'EXIT',_} -> ?SCRU:default_value('numeric');
        N -> N
    end;
update_value(#variable{type='string'}, {_BVal,_Enc}=Value) when is_binary(_BVal), is_binary(_Enc) -> Value;
update_value(#variable{type='string'}=Var, Value) ->
    case catch ?SCR_EXPRU:b(Value) of
        {'EXIT',_} -> update_value_encoding(Var,?SCRU:default_value('string'));
        S -> update_value_encoding(Var,S)
    end;
update_value(#variable{type='datetime'}=Var, {BVal,_Enc}) -> update_value(Var,BVal);
update_value(#variable{type='datetime'}, Value) ->
    case catch ?SCR_EXPRU:parse_datetime(?SCR_EXPRU:b(Value)) of
        {'EXIT',_} -> ?SCRU:default_value('datetime');
        {{2000,1,1}, {0,0,0}, 0} -> ?SCRU:default_value('datetime');
        _ -> ?SCR_EXPRU:b(Value)
    end;
update_value(_Var, Value) -> Value.

%% @private @encstring
update_value_encoding(_Var,Value) when is_binary(Value) -> ?SCRU:to_utf8(Value);
update_value_encoding(_Var,{Value,Enc}) when is_binary(Value), is_binary(Enc) -> {Value,Enc};
update_value_encoding(_Var,Value) -> Value.
