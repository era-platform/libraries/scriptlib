%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.08.2021
%%% @doc Component ASSIGN.
%%%      Used to assign arguments to variables.

-module(scriptlib_sm_component_assign).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/1]).

-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% Metadata functions
%% ====================================================================

%% --------------------------------
%% returns descriptions of used properties
%% --------------------------------
-spec metadata(ScriptType::binary()) -> ComponentDescription::map().
%% --------------------------------
metadata(_ScriptType) ->
    #{
        defaultName => <<"Присвоение"/utf8>>,
        hint => <<"Присвоение значения переменной"/utf8>>,
        group => management,
        properties => [
            #{
                key => <<"mode">>,
                type => <<"list">>,
                title => <<"Режим"/utf8>>,
                items => [
                    {0,<<"single">>,<<"Одиночный"/utf8>>},
                    {1,<<"multi">>,<<"Множественный"/utf8>>}],
                multiView => key,
                default => 0 },
            #{
                key => <<"variable">>,
                type => <<"variable">>,
                title => <<"Переменная"/utf8>>,
                editor => true },
            #{
                key => <<"value">>,
                type => <<"argument">>,
                title => <<"Значение"/utf8>>,
                multiView => key,
                editor => true },
            #{
                key => <<"operations">>,
                type => <<"argument">>,
                title => <<"Операции"/utf8>>,
                multiView => key,
                itemtype => [
                    {<<"variable">>, #{type => <<"variable">>}},
                    {<<"value">>, #{type => <<"argument">>}}],
                filter => <<"mode == 1">> },
            #{
                key => <<"transfer">>,
                type => <<"transfer">>,
                title => <<"Переход"/utf8>>,
                multiView => key,
                transferCtrlButton => 1,
                transferDigitButton => 1,
                transferTitle => <<"">> },
            #{
                key => <<"name">>,
                type => <<"string">>,
                title => <<"Имя"/utf8>>,
                editor => true },
            #{
                key => <<"info">>,
                type => <<"string">>,
                title => <<"Описание"/utf8>>,
                editor => true }]
    }.

%% ====================================================================
%% Callback functions
%% ====================================================================

init(#state_scr{active_component=#component{data=Data0}=_Comp}=State) ->
    Data1 = case lists:keysearch(<<"value">>, 1, Data0) of
                false -> lists:append([{<<"value">>,undefined}], Data0);
                _ -> Data0
            end,
    State1 = case ?SCR_COMP:get(<<"mode">>, Data1, 0) of
                 0 ->
                     assign(?BU:extract_required_props([<<"variable">>,<<"value">>], Data1), State);
                 _ ->
                     Operations = ?SCR_COMP:get(<<"operations">>, Data1, []),
                     [Items] = ?BU:extract_optional_default([{<<"items">>, []}], Operations),
                     lists:foldl(fun(Op, StateAcc) when is_list(Op) ->
                                         assign(?BU:extract_required_props([<<"variable">>,<<"value">>], Op), StateAcc)
                                 end, State, Items)
             end,
    ?SCR_COMP:next(<<"transfer">>, State1).


%% handle_event(_Event, State) ->
%%     {ok, State}.
%%
%% terminate(State) ->
%%     {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

assign([Var, Arg], State) ->
    % integer() | float() | encstring()
    Value = case {?SCR_ARG:get_value(Arg, undefined, State), ?SCR_VAR:get_type(Var, State)} of
                {undefined,undefined} -> undefined;
                {undefined,numeric} -> 0;
                {undefined,string} -> <<>>;
                {undefined,datetime} -> <<"2000-01-01T00:00:00Z">>;
                {undefined,_} -> <<>>;
                {V,_} -> V
            end,
    case ?SCR_VAR:set_value(Var, Value, State) of
        {ok, State1} -> State1;
        _ -> State
    end.
