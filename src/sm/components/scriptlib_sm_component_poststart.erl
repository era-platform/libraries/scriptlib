%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc Component POST START.
%%%      Used to find startpoint of script's post branch by StopReason.
%%%      Different stop reasons could initiate different branches.
%%%      Unique stop-reason component has higher priority than all-reason component.

-module(scriptlib_sm_component_poststart).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/1]).

-export([init/1]).

-export([check_transition/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% Metadata functions
%% ====================================================================

%% --------------------------------
%% returns descriptions of used properties
%% --------------------------------
-spec metadata(ScriptType::binary()) -> ComponentDescription::map().
%% --------------------------------
metadata(_ScriptType) ->
    #{
        defaultName => <<"Пост-старт"/utf8>>,
        hint => <<"Старт ветви пост-обработки"/utf8>>,
        group => management,
        properties => [
            #{
                key => <<"transitionOption">>,
                type => <<"list">>,
                title => <<"Вариант перехода"/utf8>>,
                items => [
                    {0,<<"all">>,<<"Все"/utf8>>},
                    {1,<<"normal">>,<<"Корректное завершение"/utf8>>},
                    {2,<<"transfer_not_set">>,<<"Нет ветки перехода"/utf8>>},
                    {3,<<"component_not_found">>,<<"Компонент не найден"/utf8>>},
                    {4,<<"module_not_found">>,<<"Модуль не найден"/utf8>>},
                    {5,<<"stop">>,<<"Внешняя остановка"/utf8>>},
                    {6,<<"timeout">>,<<"Таймаут"/utf8>>},
                    {7,<<"error">>,<<"Ошибка компонента"/utf8>>},
                    {8,<<"ownerdown">>,<<"Падение владельца"/utf8>>},
                    {9,<<"maxcount">>,<<"Лимит числа компонентов"/utf8>>},
                    {10,<<"maxduration">>,<<"Лимит времени выполнения"/utf8>>}],
                default => 0,
                filter => <<"type == 1">> },
            #{
                key => <<"ttl">>,
                type => <<"argument">>,
                title => <<"Время жизни, сек"/utf8>>,
                multiView => key,
                editor => true,
                default => 60,
                filter => <<"type == 1">> },
            #{
                key => <<"transfer">>,
                type => <<"transfer">>,
                title => <<"Переход">>,
                multiView => key,
                transferCtrlButton => 1,
                transferDigitButton => 1,
                transferTitle => <<"">> },
            #{
                key => <<"name">>,
                type => <<"string">>,
                title => <<"Имя"/utf8>>,
                editor => true },
            #{
                key => <<"info">>,
                type => <<"string">>,
                title => <<"Описание"/utf8>>,
                editor => true }]
    }.

%% ====================================================================
%% Callback functions
%% ====================================================================

init(#state_scr{}=State) ->
    State1 = init_async_terminate(State),
    ?SCR_COMP:next(<<"transfer">>, State1).

%% handle_event(_Event, State) ->
%%     {ok, State}.
%%
%% terminate(State) ->
%%     {ok, State}.

%% ====================================================================
%% API functions fof sub interface
%% ====================================================================

%% --------------------------------
-spec check_transition(StopReason::atom(), State::#state_scr{}) -> true | false | default.
%% --------------------------------
check_transition(StopReason, #state_scr{active_component=#component{data=Data}=_Comp}=_State) ->
    case ?SCR_COMP:get(<<"transitionOption">>, Data, 0) of
        0 -> default; % if no other poststart found
        V -> check_option(StopReason,V)
    end.

%% @private
check_option(StopReason,V) ->
    Key = case V of
              1 -> ?Post_Normal;
              2 -> ?Post_TransferNotSet;
              3 -> ?Post_ComponentNotFound;
              4 -> ?Post_ModuleNotFound;
              5 -> ?Post_Stop;
              6 -> ?Post_Timeout;
              7 -> ?Post_Error;
              8 -> ?Post_Down;
              9 -> ?Post_MaxCount;
              10 -> ?Post_MaxDuration
          end,
    Key==StopReason.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------------------
%% @private
%% --------------------------------
init_async_terminate(#state_scr{active_component=#component{data=Data},ref=Ref}=State) ->
    TimerRef = case ?SCR_ARG:get_int(<<"ttl">>, Data, -1, State) of
                   Sec when Sec > 0 -> erlang:send_after(Sec * 1000, self(), {async_terminating_timeout, Ref});
                   _ -> undefined
               end,
    State#state_scr{terminating_timer=TimerRef}.
