%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.08.2021
%%% @doc Component COMPARE.
%%%      Used to choose one of 2 branches by Arguments compare.

-module(scriptlib_sm_component_compare).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/1]).

-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% Metadata functions
%% ====================================================================

%% --------------------------------
%% returns descriptions of used properties
%% --------------------------------
-spec metadata(ScriptType::binary()) -> ComponentDescription::map().
%% --------------------------------
metadata(_ScriptType) ->
    #{
        defaultName => <<"Сравнение"/utf8>>,
        hint => <<"Сравнение двух аргументов"/utf8>>,
        group => management,
        properties => [
            #{
                key => <<"arg1">>,
                type => <<"argument">>,
                title => <<"Аргумент 1"/utf8>>,
                multiView => key,
                editor => true },
            #{
                key => <<"arg2">>,
                type => <<"argument">>,
                title => <<"Аргумент 2"/utf8>>,
                multiView => key,
                editor => true },
            #{
                key => <<"compareType">>,
                type => <<"list">>,
                title => <<"Тип сравнения"/utf8>>,
                items => [
                    {0,"<"},
                    {1,"=<"},
                    {2,"=="},
                    {3,">="},
                    {4,">"},
                    {5,"=/="}],
                multiView => key,
                default => 2 },
            #{
                key => <<"transferTrue">>,
                type => <<"transfer">>,
                title => <<"Переход, правда"/utf8>>,
                multiView => key,
                transferCtrlButton => 1,
                transferDigitButton => 1,
                transferTitle => <<"Правда">> },
            #{
                key => <<"transferFalse">>,
                type => <<"transfer">>,
                title => <<"Переход, ложь"/utf8>>,
                multiView => key,
                transferCtrlButton => 1,
                transferDigitButton => 2,
                transferTitle => <<"Ложь">> },
            #{
                key => <<"name">>,
                type => <<"string">>,
                title => <<"Имя"/utf8>>,
                editor => true },
            #{
                key => <<"info">>,
                type => <<"string">>,
                title => <<"Описание"/utf8>>,
                editor => true }]
    }.

%% ====================================================================
%% Callback functions
%% ====================================================================

init(#state_scr{active_component=#component{data=Data}=_Comp}=State) ->
    % integer() | float() | encstring()
    Arg1 = ?SCR_ARG:get_value(<<"arg1">>, Data, undefined, State),
    Arg2 = ?SCR_ARG:get_value(<<"arg2">>, Data, undefined, State),
    {A1,A2} = sync_encoding(Arg1,Arg2), % @encstring
    Cmp = ?SCR_COMP:get(<<"compareType">>, Data, -1),
    case compare(Cmp, A1, A2) of
        true -> ?SCR_COMP:next(<<"transferTrue">>, State);
        false -> ?SCR_COMP:next(<<"transferFalse">>, State)
    end.

%% handle_event(_Event, State) ->
%%     {ok, State}.
%%
%% terminate(State) ->
%%     {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% compare. convert types
compare(Cmp, Arg1, Arg2) when is_number(Arg1) andalso is_number(Arg2) ->
    compare_num(Cmp, Arg1, Arg2);
compare(Cmp, Arg1, Arg2) when is_binary(Arg1) andalso is_binary(Arg2) ->
    TI1 = try_convert_to_num(Arg1),
    TI2 = try_convert_to_num(Arg2),
    case TI1/=undefined andalso TI2/=undefined of
        true -> compare_num(Cmp, TI1, TI2);
        false -> compare_string(Cmp, Arg1, Arg2)
    end;
compare(Cmp, Arg1, Arg2) when is_number(Arg1) andalso is_binary(Arg2) ->
    case try_convert_to_num(Arg2) of
        undefined -> compare_string(Cmp, ?BU:to_binary(Arg1), Arg2);
        NArg2 -> compare_num(Cmp, Arg1, NArg2)
    end;
compare(Cmp, Arg1, Arg2) when is_binary(Arg1) andalso is_number(Arg2) ->
    case try_convert_to_num(Arg1) of
        undefined -> compare_string(Cmp, Arg1, ?BU:to_binary(Arg2));
        NArg1 -> compare_num(Cmp, NArg1, Arg2)
    end;
%compare(_Cmp, Arg1, Arg2) when Arg1==undefined orelse Arg2==undefined ->
%    false;
compare(Cmp, undefined, undefined) ->
    compare(Cmp, <<>>, <<>>);
compare(Cmp, undefined, Arg2) ->
    compare(Cmp, <<>>, Arg2);
compare(Cmp, Arg1, undefined) ->
    compare(Cmp, Arg1, <<>>);
%
compare(Cmp, Arg1, Arg2) ->
    compare_sametype(Cmp, Arg1, Arg2).

% compare. same types
compare_num(Cmp, Arg1, Arg2) -> compare_sametype(Cmp, Arg1, Arg2).
compare_string(Cmp, Arg1, Arg2) -> compare_sametype(Cmp, Arg1, Arg2).

compare_sametype(-1, _, _) -> false;
compare_sametype(0, Arg1, Arg2) -> Arg1 < Arg2;
compare_sametype(1, Arg1, Arg2) -> Arg1 =< Arg2;
compare_sametype(2, Arg1, Arg2) -> Arg1 == Arg2;
compare_sametype(3, Arg1, Arg2) -> Arg1 >= Arg2;
compare_sametype(4, Arg1, Arg2) -> Arg1 > Arg2;
compare_sametype(5, Arg1, Arg2) -> Arg1 /= Arg2.

% convert to number
try_convert_to_num(Arg) ->
    try ?BU:to_int(Arg)
    catch error:_ ->
              try ?BU:to_float(Arg)
              catch error:_ -> undefined
              end
    end.

% @encstring
sync_encoding(undefined,undefined) -> {undefined,undefined};
sync_encoding(undefined,Arg2) -> {undefined,Arg2};
sync_encoding(Arg1,undefined) -> {Arg1,undefined};
sync_encoding(Arg1,Arg2) when is_binary(Arg1) -> sync_encoding(?SCRU:to_utf8(Arg1),Arg2);
sync_encoding(Arg1,Arg2) when is_binary(Arg2) -> sync_encoding(Arg1,?SCRU:to_utf8(Arg2));
sync_encoding(Arg1,Arg2) when is_number(Arg1), is_number(Arg2) -> {Arg1,Arg2};
sync_encoding(Arg1,{S2,_}) when is_number(Arg1), is_binary(S2) -> {Arg1,S2};
sync_encoding({S1,_},Arg2) when is_binary(S1), is_number(Arg2) -> {S1,Arg2};
sync_encoding({S1,E},{S2,E}) when is_binary(S1), is_binary(S2), is_binary(E) -> {S1,S2};
sync_encoding({S1,_}=Arg1,{S2,_}=Arg2) when is_binary(S1), is_binary(S2) -> {?SCRU:to_utf8(Arg1),?SCRU:to_utf8(Arg2)}.
