%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.08.2021
%%% @doc Component EXEC SCRIPT.
%%%      Used to start async script in parallel or sub script in current scriptmachine using stack.

-module(scriptlib_sm_component_execscript).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/1]).

-export([init/1, handle_event/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

-define(StartScriptTimeout, 5000).

%% ====================================================================
%% Metadata functions
%% ====================================================================

%% --------------------------------
%% returns descriptions of used properties
%% --------------------------------
-spec metadata(ScriptType::binary()) -> ComponentDescription::map().
%% ---------------------------------
metadata(_ScriptType) ->
    #{
        defaultName => <<"Запуск сценария"/utf8>>,
        hint => <<"Запуск вложенного или асинхронного сценария"/utf8>>,
        group => management,
        properties => [
            #{
                key => <<"startMode">>,
                type => <<"list">>,
                title => <<"Режим"/utf8>>,
                items => [
                    {0,<<"sub">>,<<"Вложенный"/utf8>>},
                    {1,<<"async">>,<<"Асинхронный служебный"/utf8>>}],
                default => 0 },
            #{
                key => <<"scriptMode">>,
                type => <<"list">>,
                title => <<"Источник сценария"/utf8>>,
                items => [
                    {0,<<"code_sel">>,<<"Из списка"/utf8>>},
                    {1,<<"code">>,<<"Код сценария"/utf8>>}],
                default => 1 },
            #{
                key => <<"scriptCodeSync">>,
                type => <<"cachedlist">>,  %% #364 @ceceron
                title => <<"Сценарий"/utf8>>,
                filter => <<"(scriptMode == 0) && (startMode == 0)">>,
                editor => true,
                listHashCode => <<"samescriptproj">>,
                itemViewMode => <<"string">>,
                itemSaveType => <<"string">> },
            #{
                key => <<"scriptCodeAsync">>,
                type => <<"cachedlist">>,
                title => <<"Сценарий"/utf8>>,
                filter => <<"(scriptMode == 0) && (startMode == 1)">>,
                editor => true,
                listHashCode => <<"svcscriptproj">>,
                itemViewMode => <<"string">>,
                itemSaveType => <<"string">> },
            #{
                key => <<"scriptCodeStr">>,
                type => <<"argument">>,
                title => <<"Код сценария"/utf8>>,
                multiView => key,
                editor => true,
                filter => <<"(scriptMode == 1)">> },
            #{
                key => <<"uuidVariable">>,
                type => <<"variable">>,
                title => <<"Ид в переменную"/utf8>>,
                filter => <<"startMode == 1">>},
            #{
                key => <<"takeOver">>,
                type => <<"list">>,
                title => <<"Возврат управления"/utf8>>,
                items => [
                    {0,<<"no">>,<<"Нет"/utf8>>},
                    {1,<<"yes">>,<<"Да"/utf8>>}],
                default => 0,
                filter => <<"startMode == 0">> },
            #{
                key => <<"param1">>,
                type => <<"argument">>,
                title => <<"Параметр 1"/utf8>>,
                multiView => key,
                editor => true,
                filter => <<"startMode == 1">> },
            #{
                key => <<"param2">>,
                type => <<"argument">>,
                title => <<"Параметр 2"/utf8>>,
                multiView => key,
                editor => true,
                filter => <<"(startMode == 1) && (param1 != null)">> },
            #{
                key => <<"param3">>,
                type => <<"argument">>,
                title => <<"Параметр 3"/utf8>>,
                multiView => key,
                editor => true,
                filter => <<"(startMode == 1) && (param2 != null)">> },
            #{
                key => <<"param4">>,
                type => <<"argument">>,
                title => <<"Параметр 4"/utf8>>,
                multiView => key,
                editor => true,
                filter => <<"(startMode == 1) && (param3 != null)">> },
            #{
                key => <<"param5">>,
                type => <<"argument">>,
                title => <<"Параметр 5"/utf8>>,
                multiView => key,
                editor => true,
                filter => <<"(startMode == 1) && (param4 != null)">> },
            #{
                key => <<"transfer">>,
                type => <<"transfer">>,
                title => <<"">>,
                multiView => key,
                transferCtrlButton => 1,
                transferDigitButton => 1,
                transferTitle => <<"">> },
            #{
                key => <<"transferError">>,
                type => <<"transfer">>,
                title => <<"Переход, ошибка"/utf8>>,
                multiView => key,
                filter => <<"startMode == 1">>,
                transferCtrlButton => 1,
                transferDigitButton => 2,
                transferTitle => <<"Ошибка"/utf8>> },
            #{
                key => <<"name">>,
                type => <<"string">>,
                title => <<"Имя"/utf8>>,
                editor => true },
            #{
                key => <<"info">>,
                type => <<"string">>,
                title => <<"Описание"/utf8>>,
                editor => true }]
    }.

%% ====================================================================
%% Callback functions
%% ====================================================================

init(#state_scr{active_component=#component{data=Data}}=State) ->
    Mode = mode(?SCR_COMP:get(<<"startMode">>, Data, 0)),
    NextId = ?SCR_COMP:get(<<"transfer">>, Data, -1),
    ScriptCode = case ?SCR_COMP:get(<<"scriptMode">>, Data, 0) of
                     0 ->
                         case Mode of
                             sub -> ?SCR_COMP:get(<<"scriptCodeSync">>, Data, <<>>);
                             async -> ?SCR_COMP:get(<<"scriptCodeAsync">>, Data, <<>>)
                         end;
                     1 -> ?SCR_ARG:get_string(<<"scriptCodeStr">>, Data, <<>>, State)
                 end,
    case Mode of
        sub -> sub(NextId, ScriptCode, State);
        async -> async(ScriptCode, State);
        _ -> {error,{invalid_params,<<"Script error. Invalid startMode">>}}
    end.


%% --------------------------------------
handle_event({async_execscript_started, SvcId}, #state_scr{active_component=#component{data=Data}}=State) ->
    case ?SCR_VAR:set_value(<<"uuidVariable">>, SvcId, Data, State) of
        {ok, UpState} -> ?SCR_COMP:next(<<"transfer">>, UpState);
        Error ->
            ?SCR_TRACE(State, "~500tp -- set_result: set_value 'uuidVariable' ERROR: ~500tp",[?MODULE, Error]),
            ?SCR_COMP:next([<<"transferError">>,<<"transfer">>], State)
    end;

handle_event({async_execscript_error}, State) ->
    ?SCR_COMP:next([<<"transferError">>,<<"transfer">>], State);

handle_event(_Event, State) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

mode(0) -> sub;
mode(1) -> async;
mode(_) -> undefined.

%% ------------------------------------------------
%%
%% ------------------------------------------------
sub(NextId, ScriptCode, #state_scr{loglevel=LogLevel, active_component=#component{data=Data}}=State) ->
    TakeOver = ?BU:to_bool(?SCR_COMP:get(<<"takeOver">>, Data, 0)),
    #state_scr{code=CurCode, scriptstarttime=CurTime, stack=Stack, limits=Limits}=State,
    case TakeOver of
        true when length(Stack)>1000 -> {error,{invalid_operation,<<"Script error. Stack overload">>}};
        true ->
            StackItem = #{code => CurCode,
                          next => NextId,
                          starttime => CurTime,
                          loglevel => LogLevel, % RP-1460
                          countpassed => maps:get(countpassed,Limits)},
            State1 = State#state_scr{stack = [StackItem|Stack],
                                     limits = Limits#{countpassed:=0}}, % RP-501
            {'script', ScriptCode, State1};
        false ->
            State1 = State#state_scr{stack=[]},
            {'script', ScriptCode, State1}
    end.

%% ------------------------------------------------
%%
%% ------------------------------------------------
async(ScriptCode, #state_scr{variables=Variables,scriptid=Id,type=ScrType,loglevel=LogLevel,meta=Meta}=State) ->
    StartParam = startparams(State),
    #state_scr{domain=Domain}=State,
    Self = self(),
    LocalVars = lists:filter(fun(#variable{location='local'}) -> true; (_) -> false end, Variables),
    _Pid = spawn(fun() -> init_async(Domain, ScriptCode, Self, LocalVars, StartParam, Meta, {ScrType, Id, LogLevel}) end),
    {ok, State}.

%% @private
init_async(Domain, ScriptCode, ScriptPid, Variables, StartParam, Meta, LogInfo) ->
    init_async(Domain, ScriptCode, ScriptPid, 5, 100, Variables, StartParam, Meta, LogInfo).

%% @private
init_async(Domain, ScriptCode, ScriptPid, 0, _, _, _, _, {ScrType, ScrId, LogLevel}) ->
    ?SCR_WARN({LogLevel, Domain, ScriptCode, ScrType, ScrId}, "exec script init() failed"),
    catch (?SCR_COMP:event(ScriptPid, {async_execscript_error}));
init_async(Domain, Code, ScriptPid, AttemptsLeft, Sleep, Variables, StartParam, Meta, {ScrType, ScrId, LogLevel}=LogInfo) ->
    {ScriptCode, _DestDomain} = case string:split(Code, <<"@">>) of [Code] -> {Code, <<>>}; [C, D] -> {C, D} end,
    Opts3 = StartParam#{<<"code">> => ScriptCode,
                        <<"domain">> => Domain,
                        <<"variables">> => Variables,
                        <<"scripts_prior">> => Meta#meta.scripts_prior}, % 201120 to start svc async scripts with same search function
    LogTag = {LogLevel, Domain, ScriptCode, ScrType, ScrId},
    case ?CFG:start_async_script(Domain, ScriptCode, Variables) of
        undefined ->
            ?SCR_TRACE(LogTag, "exec script async: undefined"),
            timer:sleep(Sleep),
            init_async(Domain, Code, ScriptPid, AttemptsLeft-1, Sleep, Variables, StartParam, Meta, LogInfo);
        {ok, Info} when is_map(Info) ->
            SvcId = maps:get(id,Info),
            ?SCR_TRACE(LogTag, "exec script async: Started on node ~tp", [node()]),
            catch (?SCR_COMP:event(ScriptPid, {async_execscript_started, SvcId})),
            ok;
        {error, Reason} ->
            ?SCR_TRACE(LogTag, "exec script async: svcscr start error: ~tp", [Reason]),
            timer:sleep(Sleep),
            init_async(Domain, Code, ScriptPid, AttemptsLeft-1, Sleep, Variables, StartParam, Meta, LogInfo);
        Other ->
            ?SCR_TRACE(LogTag, "exec script async: call_node(~tp, {,sync_start_svcscript,},,) Other => ~tp", [Opts3, Other]),
            timer:sleep(Sleep),
            init_async(Domain, Code, ScriptPid, AttemptsLeft-1, Sleep, Variables, StartParam, Meta, LogInfo)
    end.

%% ------------------------------------------------
%%
%% ------------------------------------------------
-spec startparams(State::#state_scr{}) -> Params::map().
%% ------------------------------------------------
startparams(#state_scr{active_component=#component{data=Data}=_Comp}=State) ->
    Param = [],
    Param1 = lists:append([{<<"startparam1">>,?SCR_ARG:get_string(<<"param1">>, Data, undefined, State)}], Param),
    Param2 = lists:append([{<<"startparam2">>,?SCR_ARG:get_string(<<"param2">>, Data, undefined, State)}], Param1),
    Param3 = lists:append([{<<"startparam3">>,?SCR_ARG:get_string(<<"param3">>, Data, undefined, State)}], Param2),
    Param4 = lists:append([{<<"startparam4">>,?SCR_ARG:get_string(<<"param4">>, Data, undefined, State)}], Param3),
    Param5 = lists:append([{<<"startparam5">>,?SCR_ARG:get_string(<<"param5">>, Data, undefined, State)}], Param4),
    %
    FilterParam = lists:filter(fun({_,undefined}) -> false; (_) -> true end, Param5),
    maps:from_list(FilterParam).
