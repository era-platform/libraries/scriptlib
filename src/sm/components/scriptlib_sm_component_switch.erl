%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.08.2021
%%% @doc Component SWITCH MENU.
%%%      Used to switch next sub branch by argument value.

-module(scriptlib_sm_component_switch).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/1]).

-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% --------------------------------
%% returns descriptions of used properties
%% --------------------------------
-spec metadata(ScriptType::binary()) -> ComponentDescription::map().
%% --------------------------------
metadata(_ScriptType) ->
    #{
        defaultName => <<"Меню"/utf8>>,
        hint => <<"Меню-разветвитель по значениям"/utf8>>,
        group => management,
        properties => [
            #{
                key => <<"arg">>,
                type => <<"argument">>,
                title => <<"Аргумент"/utf8>>,
                multiView => key,
                editor => true },
            #{
                key => <<"values">>,
                type => <<"menu">>,
                title => <<"Значения"/utf8>>,
                itemtype => [{<<"value">>, #{type => <<"argument">>}},
                             {<<"transfer">>, #{type => <<"transfer">>, hint => auto }}] },
            #{
                key => <<"transferFalse">>,
                type => <<"transfer">>,
                title => <<"Переход, прочее"/utf8>>,
                multiView => key,
                transferCtrlButton => 1,
                transferDigitButton => 1,
                transferTitle => <<"Прочее">> },
            #{
                key => <<"name">>,
                type => <<"string">>,
                title => <<"Имя"/utf8>>,
                editor => true },
            #{
                key => <<"info">>,
                type => <<"string">>,
                title => <<"Описание"/utf8>>,
                editor => true }]
    }.

%% ====================================================================
%% Callback functions
%% ====================================================================

init(#state_scr{active_component=#component{data=Data}=_Comp}=State) ->
    % integer() | float() | encstring()
    Arg = ?SCR_ARG:get_value(<<"arg">>, Data, undefined, State),
    Cases = ?SCR_COMP:get(<<"values">>, Data, []),
    [Cases1] = ?BU:extract_optional_default([{<<"items">>, []}], Cases),
    F = fun(Case, undefined) when is_list(Case) ->
                [TestArg] = ?BU:extract_optional_props([<<"value">>], Case),
                TestVal = ?SCR_ARG:get_value(TestArg, undefined, State),
                {A,T} = sync_encoding(Arg,TestVal), % @encstring
                case equal(A, T) of
                    true -> [Transfer] = ?BU:extract_optional_props([<<"transfer">>], Case), Transfer;
                    false -> undefined
                end;
           (_, Acc) -> Acc
        end,
    case lists:foldl(F, undefined, Cases1) of
        undefined -> ?SCR_COMP:next(<<"transferFalse">>, State);
        TransferId -> ?SCR_COMP:next(TransferId, State)
    end.

%% handle_event(_Event, State) ->
%%     {ok, State}.
%%
%% terminate(State) ->
%%     {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% compare. convert types
equal(Arg1, Arg2) when is_number(Arg1) andalso is_number(Arg2) -> Arg1 == Arg2;
equal(Arg1, Arg2) when is_binary(Arg1) andalso is_binary(Arg2) ->
    TI1 = try_convert_to_num(Arg1),
    TI2 = try_convert_to_num(Arg2),
    case TI1/=undefined andalso TI2/=undefined of
        true -> TI1==TI2;
        false -> Arg1 == Arg2
    end;
equal(Arg1, Arg2) when is_number(Arg1) andalso is_binary(Arg2) ->
    case try_convert_to_num(Arg2) of
        undefined -> ?BU:to_binary(Arg1) == Arg2;
        NArg2 -> Arg1 == NArg2
    end;
equal(Arg1, Arg2) when is_binary(Arg1) andalso is_number(Arg2) ->
    case try_convert_to_num(Arg1) of
        undefined -> Arg1 == ?BU:to_binary(Arg2);
        NArg1 -> NArg1 == Arg2
    end;
equal(Arg1, Arg2) when Arg1==undefined orelse Arg2==undefined -> false;
equal(Arg1, Arg2) -> Arg1 == Arg2.

% convert to number
try_convert_to_num(Arg) ->
    try ?BU:to_int(Arg)
    catch error:_ ->
              try ?BU:to_float(Arg)
              catch error:_ -> undefined
              end
    end.

% @encstring
sync_encoding(undefined,undefined) -> {undefined,undefined};
sync_encoding(undefined,Arg2) -> {undefined,Arg2};
sync_encoding(Arg1,undefined) -> {Arg1,undefined};
sync_encoding(Arg1,Arg2) when is_binary(Arg1) -> sync_encoding(?SCRU:to_utf8(Arg1),Arg2);
sync_encoding(Arg1,Arg2) when is_binary(Arg2) -> sync_encoding(Arg1,?SCRU:to_utf8(Arg2));
sync_encoding(Arg1,Arg2) when is_number(Arg1), is_number(Arg2) -> {Arg1,Arg2};
sync_encoding(Arg1,{S2,_}) when is_number(Arg1), is_binary(S2) -> {Arg1,S2};
sync_encoding({S1,_},Arg2) when is_binary(S1), is_number(Arg2) -> {S1,Arg2};
sync_encoding({S1,E},{S2,E}) when is_binary(S1), is_binary(S2), is_binary(E) -> {S1,S2};
sync_encoding({S1,_}=Arg1,{S2,_}=Arg2) when is_binary(S1), is_binary(S2) -> {?SCRU:to_utf8(Arg1),?SCRU:to_utf8(Arg2)}.
