%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc Component STOP.
%%%      Used to break current branch of script correctly.
%%%      Define if script should return back to stack.

-module(scriptlib_sm_component_stop).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/1]).

-export([init/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% Metadata functions
%% ====================================================================

%% --------------------------------
%% returns descriptions of used properties
%% --------------------------------
-spec metadata(ScriptType::binary()) -> ComponentDescription::map().
%% --------------------------------
metadata(_ScriptType) ->
    #{
        defaultName => <<"Стоп"/utf8>>,
        hint => <<"Остановка ветви"/utf8>>,
        group => management,
        properties => [
            #{
                key => <<"callStack">>,
                type => <<"list">>,
                title => <<"Возврат управления"/utf8>>,
                items => [
                    {0,<<"no">>,<<"Нет"/utf8>>},
                    {1,<<"yes">>,<<"Да"/utf8>>}],
                multiView => key,
                default => 0
             },
            #{
                key => <<"name">>,
                type => <<"string">>,
                title => <<"Имя"/utf8>>,
                editor => true },
            #{
                key => <<"info">>,
                type => <<"string">>,
                title => <<"Описание"/utf8>>,
                editor => true }]
    }.

%% ====================================================================
%% Callback functions
%% ====================================================================

init(#state_scr{active_component=#component{data=Data}}=State) ->
    CallStack = ?BU:to_bool(?SCR_COMP:get(<<"callStack">>, Data, 0)),
    case CallStack of
        false -> {stop, State};
        true -> {stack, State}
    end.

%% handle_event(_Event, State) ->
%%     {ok, State}.
%%
%% terminate(State) ->
%%     {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================


