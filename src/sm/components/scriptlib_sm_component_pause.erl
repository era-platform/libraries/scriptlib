%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 06.08.2021
%%% @doc Component PAUSE.
%%%      Used to pause script handling for timeout in ms.

-module(scriptlib_sm_component_pause).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/1]).

-export([init/1, handle_event/2, terminate/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% Metadata functions
%% ====================================================================

%% --------------------------------
%% returns descriptions of used properties
%% --------------------------------
-spec metadata(ScriptType::binary()) -> ComponentDescription::map().
%% --------------------------------
metadata(_ScriptType) ->
    #{
        defaultName => <<"Пауза"/utf8>>,
        hint => <<"Пауза"/utf8>>,
        group => management,
        properties => [
            #{
                key => <<"timeoutMs">>,
                type => <<"argument">>,
                title => <<"Время, мс"/utf8>>,
                multiView => key,
                editor => true },
            #{
                key => <<"transfer">>,
                type => <<"transfer">>,
                title => <<"Переход"/utf8>>,
                multiView => key,
                transferCtrlButton => 1,
                transferDigitButton => 1,
                transferTitle => <<"">> },
            #{
                key => <<"name">>,
                type => <<"string">>,
                title => <<"Имя"/utf8>>,
                editor => true },
            #{
                key => <<"info">>,
                type => <<"string">>,
                title => <<"Описание"/utf8>>,
                editor => true }]
    }.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% --------------------------------------
init(#state_scr{active_component=#component{data=Data}=Comp}=State) ->
    case ?SCR_ARG:get_int(<<"timeoutMs">>, Data, 0, State) of
        N when N < 0 -> ?SCR_COMP:next(<<"transfer">>,State);
        Timeout ->
            Ref = make_ref(),
            case Timeout of
                0 -> ?SCR_COMP:event(self(), {'timeout',Ref});
                _ -> ?SCR_COMP:event_after(Timeout, {'timeout',Ref})
            end,
            {ok, State#state_scr{active_component=Comp#component{state=Ref}}}
    end.

%% --------------------------------------
handle_event({'timeout',Ref}, #state_scr{active_component=#component{state=Ref}}=State) ->
    ?SCR_COMP:next(<<"transfer">>,State);

handle_event(_Ev, State) -> {keep,State}.

%% --------------------------------------
terminate(State) ->
    % {async, State}
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================


