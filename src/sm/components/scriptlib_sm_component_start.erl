%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc Component START.
%%%      Used to find start point of script's main branch.

-module(scriptlib_sm_component_start).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/1]).

-export([init/1]).

-export([check_transition/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

-define(DEFINE_VARS_DEFAULT, 0).
-define(DEFINE_VARS, 1).

%% ====================================================================
%% Metadata functions
%% ====================================================================

%% --------------------------------
%% returns descriptions of used properties
%% --------------------------------
-spec metadata(ScriptType::binary()) -> ComponentDescription::map().
%% --------------------------------
metadata(_ScriptType) ->
    #{
        defaultName => <<"Старт"/utf8>>,
        hint => <<"Старт"/utf8>>,
        group => management,
        properties => [
            #{
                key => <<"startParam">>,
                type => <<"variable">>,
                title => <<"Параметр запуска"/utf8>>,
                multiView => key },
            #{
                key => <<"defineVars">>,
                type => <<"list">>,
                items => [
                    {0,<<"no">>,<<"Нет"/utf8>>},
                    {1,<<"yes">>,<<"Да"/utf8>>}],
                title => <<"Инициализировать переменные"/utf8>>,
                multiView => key,
                default => 1 },
            #{
                key => <<"transfer">>,
                type => <<"transfer">>,
                title => <<"Переход"/utf8>>,
                multiView => key,
                transferCtrlButton => 1,
                transferDigitButton => 1,
                transferTitle => <<"">> },
            #{
                key => <<"name">>,
                type => <<"string">>,
                title => <<"Имя"/utf8>>,
                editor => true },
            #{
                key => <<"info">>,
                type => <<"string">>,
                title => <<"Описание"/utf8>>,
                editor => true }]
    }.

%% ====================================================================
%% Callback functions
%% ====================================================================

init(#state_scr{active_component=#component{data=Data},stack=_Stack}=State) ->
    DefineVars = ?SCR_COMP:get(<<"defineVars">>, Data, ?DEFINE_VARS_DEFAULT),
    State1 = define_variables(DefineVars, State),
    State2 = store_start_param(State1),
    ?SCR_COMP:next(<<"transfer">>, State2).

%% handle_event(_Event, State) ->
%%     {ok, State}.
%%
%% terminate(State) ->
%%     {ok, State}.

%% ====================================================================
%% API functions of sub interface
%% ====================================================================

%% --------------------------------
-spec check_transition(SomeReason::term(), State::#state_scr{}) -> Type::atom().
%% --------------------------------
check_transition(_SomeReason, _State) ->
    true.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------------------
%% @private
%% --------------------------------
-spec define_variables(DefineVars::integer(), State::#state_scr{}) -> State1::#state_scr{}.
%% --------------------------------
define_variables(?DEFINE_VARS, #state_scr{variables=Vars}=State) ->
    Vars1 = lists:map(fun(#variable{location='local',
                                    value=undefined,
                                    type=T}=V) -> V#variable{value=?SCRU:default_value(T)};
                         (V) -> V
                      end, Vars),
    State#state_scr{variables=Vars1};
define_variables(_, State) -> State.

%% --------------------------------
%% @private
%% --------------------------------
-spec store_start_param(State::#state_scr{}) -> State1::#state_scr{}.
%% --------------------------------
store_start_param(#state_scr{active_component=#component{data=Data}=_Comp,meta=#meta{functions=Funs}=_Meta}=State) ->
    case lists:keysearch({startparam,1}, 1, Funs) of
        false -> State;
        {value, {_,StartParamFun}} ->
            case erlang:is_function(StartParamFun,1) of
                false -> State;
                _ ->
                    case erlang:apply(StartParamFun, [1]) of
                        undefined -> State;
                        Result ->
                            case ?SCR_VAR:set_value(<<"startParam">>,Result,Data,State) of
                                error -> State;
                                {ok, UpState} -> UpState
                            end end end end.
