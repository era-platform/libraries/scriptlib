%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc Dummy component REGION.
%%%      Used for script editor only.

-module(scriptlib_sm_component_region).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    metadata/1,
    description/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% Metadata functions
%% ====================================================================

%% --------------------------------
%% returns descriptions of used properties
%% --------------------------------
-spec metadata(ScriptType::binary()) -> ComponentDescription::map().
%% --------------------------------
metadata(_ScriptType) ->
    #{
        defaultName => <<"Регион"/utf8>>,
        hint => <<"Регион"/utf8>>,
        isRegion => true,
        typeStr => <<"_Comment">>,
        groupCode => <<"basic">>,
        groupName => <<"Базовые"/utf8>>,
        backColor => 16#FFFFFF,
        properties => [
            #{
                key => <<"text">>,
                type => <<"text">>,
                title => <<"Текст"/utf8>>,
                default => <<>> },
            #{
                key => <<"foreColor">>,
                type => <<"color">>,
                title => <<"Цвет текста"/utf8>>,
                multiView => key,
                default => 16#808080 },
            #{
                key => <<"backColor">>,
                type => <<"color">>,
                title => <<"Цвет фона"/utf8>>,
                multiView => key,
                default => 16#FBF8D9 }]
    }.

%% ---------------------------------------
%% returns descriptions of used properties
%% ---------------------------------------
description(Lang) ->
    [
        {Lang, 2, {"text","Текст",false,"text",false,common,<<>>}},
        {Lang, 8, {"foreColor","Цвет текста",true,"foreColor",false,common,16#808080}},
        {Lang, 8, {"backColor","Цвет фона",true,"backColor",false,common,16#FBF8D9}}
    ].

%% ====================================================================
%% Callback functions
%% ====================================================================

%% init(#state_scr{active_component=#component{data=_Data}=_Comp}=State) ->
%%     {ok, State}.
%%
%% handle_event(_Event, State) ->
%%     {ok, State}.
%%
%% terminate(State) ->
%%     {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================
