%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc Limitation functions of script-machine props

-module(scriptlib_sm_limit).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    merge_limits/2,
    initialize_limits/1
]).

-export([
    on_start/1,
    after_component/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

-define(AddedCountForPost, 200).
-define(AddedDurationSecForPost, 60).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------
%% merge domain and current instance limits
%% --------------------------
-spec merge_limits(DomainScrLimits::map(),InstanceScrLimits::map()) -> ScrLimits::map().
%% --------------------------
merge_limits(DomainScrLimits,InstanceScrLimits) ->
    AllKeys = lists:usort(maps:keys(DomainScrLimits)++maps:keys(InstanceScrLimits)),
    F = fun(Key,Acc) ->
                DK = maps:get(Key,DomainScrLimits,undefined),
                IK = maps:get(Key,InstanceScrLimits,undefined),
                IsSameT = is_same_types(DK,IK),
                case {DK,IK} of
                    {Dv,Iv} when Dv==undefined;Iv==undefined -> Acc#{Key=>Dv};
                    {Dv,Iv} when IsSameT==true,Key==<<"script_pause_between_components">>,Dv>Iv -> Acc#{Key=>Dv};
                    {Dv,Iv} when IsSameT==true,Key==<<"script_pause_between_components">>,Dv=<Iv -> Acc#{Key=>Iv};
                    {Dv,Iv} when IsSameT==true,Dv=<Iv,Dv==-1 -> Acc#{Key=>Iv};
                    {Dv,Iv} when IsSameT==true,Dv=<Iv -> Acc#{Key=>Dv};
                    {Dv,Iv} when IsSameT==true,Iv=<Dv,Iv<0 -> Acc#{Key=>Dv}; % Iv==-1 and just in case any other negative value would work the same.
                    {Dv,Iv} when IsSameT==true,Iv=<Dv -> Acc#{Key=>Iv};
                    {Dv,_} -> Acc#{Key=>Dv}
                end end,
    lists:foldl(F,#{},AllKeys).

%% @private
is_same_types(V1,V2) ->
    Funs = [fun is_atom/1, fun is_binary/1,
            fun is_integer/1, fun is_tuple/1,
            fun is_boolean/1, fun is_float/1, fun is_list/1],
    lists:any(fun(F) -> F(V1)==true andalso F(V2)==true end, Funs).

%% --------------------------
%% apply atstart limits
%%  should be executed on script start
%% --------------------------
-spec initialize_limits(map()) -> map().
%% --------------------------
initialize_limits(ScrLimits) ->
    #{pause => maps:get(<<"script_pause_between_components">>,ScrLimits,0),
      maxduration => maps:get(<<"script_duration_sec">>,ScrLimits,-1),
      maxcount => maps:get(<<"script_limit_component_count">>,ScrLimits,-1),
      countpassed => 0,
      scriptlimits => ScrLimits}.

%% --------------------------
%% when script machine is starting
%%  could restrict start
%% --------------------------
-spec on_start(State::#state_scr{}) -> {ok,State1::#state_scr{}} | {error,Reason::term()}.
%% --------------------------
on_start(State) -> {ok,State}.

%% --------------------------
%% apply runtime limits
%%  should be executed after every component
%% --------------------------
-spec after_component(State::#state_scr{}) -> {ok,State1::#state_scr{}} | {stop,Reason::term(),State::#state_scr{}}.
%% --------------------------
after_component(#state_scr{limits=undefined}=State) -> {ok,State};
after_component(#state_scr{limits=Limits}=State) ->
    Count = maps:get('countpassed',Limits) + 1,
    State1 = State#state_scr{limits = Limits#{'countpassed':=Count}},
    apply_count(Count,State1).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ====================================================
%% after component
%% ====================================================

%% --------------------------
%% @private
%% apply limits on component count
%% --------------------------
apply_count(Count,#state_scr{limits=Limits}=State) ->
    MaxCount = maps:get('maxcount',Limits),
    Count = maps:get('countpassed',Limits),
    case Count > MaxCount of
        true when MaxCount >= 0 ->
            ?SCR_WARN(State, "Interrupting script by component count limit: ~p", [MaxCount]),
            stop('maxcount',State#state_scr{limits=Limits#{'maxcount':=MaxCount+?AddedCountForPost}});
        _ ->
            apply_pause(Count,State)
    end.

%% --------------------------
%% @private
%% apply limits on pause between components
%% --------------------------
apply_pause(Count,#state_scr{limits=Limits}=State) ->
    case maps:get(pause,Limits) of
        P when P =< 0 -> ok;
        P when P > 100 -> timer:sleep(P);
        P -> do_limit_pause(P,Count)
    end,
    apply_duration(Count,State).

%% -----
%% @private
%% sleeps after every component for multiple 15 ms and mix several components by whole pause by rem of 15
%% -----
do_limit_pause(Pause,Count) ->
    Rem = Pause rem 15,
    X = Pause - Rem,
    Sleep = case a(Rem) of
                {0,0} -> X;
                {C,T} when Count rem C == 0 -> X + T;
                _ -> X
            end,
    case Sleep of
        0 -> ok;
        15 -> timer:sleep(14);
        _ -> timer:sleep(Sleep)
    end.

%% -----
%% @private
%% gets value of pause (rem of 15)
%% return tuple {A,B} where A is component count, B is sleep pause
%% -----
a(0) -> {0,0};
a(1) -> {15,15};
a(2) -> {7,15};
a(3) -> {5,15};
a(4) -> {8,30};
a(5) -> {3,15};
a(6) -> {8,45};
a(7) -> {2,15};
a(8) -> {2,15};
a(9) -> {2,15};
a(10) -> {3,30};
a(11) -> {3,30};
a(12) -> {5,60};
a(13) -> {1,15};
a(14) -> {1,15}.


%% --------------------------
%% @private
%% apply limits on script duration in seconds
%% --------------------------
apply_duration(_Count,#state_scr{limits=Limits, starttime=TS}=State) ->
    case maps:get('maxduration',Limits) of
        MaxDurationSec when MaxDurationSec >= 0 ->
            case timer:now_diff(os:timestamp(), TS) > MaxDurationSec*1000*1000 of
                false -> {ok,State};
                true ->
                    ?SCR_WARN(State, "Interrupting script by duration limit: ~p sec", [MaxDurationSec]),
                    stop('maxduration',State#state_scr{limits=Limits#{'maxduration':=MaxDurationSec+?AddedDurationSecForPost}})
            end;
        _ -> {ok,State}
    end.

%% --------------------------
%% stops script machine
%% --------------------------
stop(Reason,State) -> {stop,Reason,State}.
