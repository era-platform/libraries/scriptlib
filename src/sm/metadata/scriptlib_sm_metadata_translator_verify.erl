%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 05.08.2021.
%%% @doc Check component properties metadata.
%%%      Pure utilities to verify result of component property types metadata generation.
%%%      Used when translating to oktell's scripteditor metadata

-module(scriptlib_sm_metadata_translator_verify).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([verify_metadata/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").
-include("metadata.hrl").

-import(?SCR_META_UTILS, [color/1, u/1]).

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------------------------------------------
%% checks component metadata to satisfy result of metadata to scripteditor (types, list items, keys).
%% note, that scripteditor metadata could contain extra keys. But it should contain keys and filters from component's metadata.
%% -----------------------------------------------------
-spec verify_metadata(CMeta::map())
      -> {error,Reason::term()} | {ok,NewCMeta::map()}.
%% -----------------------------------------------------
verify_metadata(CMeta) ->
    CT = maps:get('type',CMeta),
    try
        {ok,CMeta1} = verify_basic(CT,CMeta),
        verify_properties(CT,CMeta1)
    catch
        throw:R:StackTrace ->
            ?LOG('$crash', "Verify component ~p failed: ~120tp,~n\tStack: ~120tp", [CT,StackTrace]), R;
        E:R:StackTrace ->
            ?LOG('$crash', "Verify component ~p failed: {~120tp,~120tp},~n\tStack: ~120tp", [CT,E,R,StackTrace]), {error,R}
    end.

%% ====================================================================
%% Internal functions
%% Verify fields of component
%% ====================================================================

%% defaultName
%% hint
%% groupCode | from group
%% groupName | from group
%% backColor | from group
%% [isRegion = false]
%% [typeStr = <<>>]

verify_basic(_CT,CMeta) ->
    case maps:get('defaultName',CMeta,undefined) of
        undefined -> throw({error,{invalid_metadata,?BU:strbin("Property 'defaultName' not found",[])}});
        DefaultName ->
            CMeta1 = ensure(hint,CMeta,DefaultName),
            CMeta2 = case maps:get('group',CMeta1,undefined) of
                         undefined ->
                             GC = maps:get('groupCode',CMeta1,undefined),
                             GN = maps:get('groupName',CMeta1,undefined),
                             BC = maps:get('backColor',CMeta1,undefined),
                             case lists:all(fun({K,Fun}) -> Fun(K) end, [{GC,fun is_binary/1},{GN,fun is_binary/1},{BC,fun is_number/1}]) of
                                 true -> CMeta1;
                                 false -> throw({error,{invalid_metadata,?BU:strbin("Some of properties {'groupCode','groupName','backColor'} are invalid",[])}})
                             end;
                         Group ->
                             maps:merge(CMeta1,group(Group))
                     end,
            {ok,CMeta2}
    end.

%% ====================================================================
%% Internal functions
%% Verify fields of properties
%% ====================================================================

%% @private
verify_properties(CT,CMeta) ->
    case maps:get('properties',CMeta,undefined) of
        undefined -> throw({error,{invalid_metadata,?BU:strbin("Property 'properties' not found",[])}});
        Properties when not is_list(Properties) -> throw({error,{invalid_metadata,?BU:strbin("Invalid property 'properties' type. Expected: list of maps",[])}});
        Properties -> verify_properties_1(Properties,CT,CMeta)
    end.

%% @private
verify_properties_1(Properties,CT,CMeta) ->
    % filter script mismatch properties
    ScriptType = ?CFG:script_type(),
    Ffilter = fun(P) ->
                   case maps:get('scriptType',P,undefined) of
                       undefined -> true;
                       _ when ScriptType==undefined -> true;
                       ScriptTypes when is_list(ScriptTypes) -> lists:member(ScriptType,ScriptTypes);
                       ScriptType -> true;
                       _ -> false
                   end end,
    Properties1 = lists:filter(Ffilter, Properties),
    % make hashtable of scripteditor props
    Hasht = lists:foldl(fun(P,Acc) when is_map(P) ->
                               case maps:get('key',P,undefined) of
                                   undefined -> throw({error,{invalid_metadata,?BU:strbin("Property 'key' not found",[])}});
                                   Key when not is_binary(Key) -> throw({error,{invalid_metadata,?BU:strbin("Invalid property 'key' type. Expected: binary()",[])}});
                                   Key ->
                                       case maps:get(Key,Acc,undefined) of
                                           undefined -> Acc#{Key => P};
                                           _ -> throw({error,{invalid_metadata,?BU:strbin("Property 'key' duplicate: '~ts'",[Key])}})
                                       end end;
                           (_,_) -> throw({error,{invalid_metadata,?BU:strbin("Invalid property description. Expected: map()",[])}})
                        end, maps:new(), Properties1),
    % check name and info
    check_name(CT,Hasht),
    % fold all component metadata props to satisfy scripteditor
    R = lists:foldl(fun(P, {ok,Hasht1}) -> check_key(P,Hasht1);
                       (_, {error,_}=Err) -> Err
                    end, {ok,Hasht}, Properties1),
    case R of
        {error,_}=Err -> Err;
        {ok,Hasht2} ->
            PSRes = lists:reverse(lists:foldl(fun(P,Acc) ->
                                                    Key = maps:get('key',P),
                                                    PRes = maps:get(Key,Hasht2),
                                                    [PRes|Acc]
                                             end, [], Properties1)),
            {ok,CMeta#{'properties' => PSRes}}
    end.

%%% ----------------------
%%% fields for properties:
%%% ----------------------
%%% - typeId,
%%% - typeName
%%% - jsonKey
%%% - name
%%% - group
%%% - defaultValue
%%% - allowDirectEdit
%%% - canMultipleView
%%% - multiViewHashCode
%%% - visibleCondition
%%% fetTransfer
%%% - transferCtrlButton
%%% - transferDigitButton
%%% - transferTitle
%%% fetCachedList
%%% - listHashCode
%%% - itemViewMode
%%% - itemSaveType
%%% fetYesNo ( -> to fetCombo)
%%% - fetCombo
%%% - comboItems (#{script,code,value,title})
%%% fetVariable
%%% - varType
%%% fetArgument
%%% - argType
%%% - varType
%%% fetAttachedFile
%%% - fileSubDirectory static
%%% - fileDescription
%%% - fileExtensions
%%% - fileMimes
%%% ---------

%% @private
%% checks name and info properties
check_name(<<"region">>,_) -> ok;
check_name(_CT,Hasht) ->
    check_field({<<"name">>,<<"string">>},Hasht),
    check_field({<<"info">>,<<"string">>},Hasht).

% @private
check_field({Key,Type},Hasht) ->
    case maps:find(Key,Hasht) of
        error -> throw({error,{property_not_found,Key}});
        {_,P} ->
            case maps:get('type',P,undefined) of
                undefined -> throw({error,{type_not_found,Key}});
                Type -> ok;
                _ -> throw({error,{type_mismatch,Key}})
            end end.

%% ---------
%% @private
%% checks scripteditor metadata by key of component's metadata
check_key(PropMap,Hasht) ->
    Key = maps:get('key',PropMap),
    PropMapAcc = maps:get(Key,Hasht),
    check_key_1(Key,PropMapAcc,Hasht).

%% @private
check_key_1(Key,PropMap,Hasht) ->
    case maps:get('type',PropMap,undefined) of
        undefined -> {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type not found", [Key])}};
        Type when is_binary(Type) -> check_key_2(Type,Key,PropMap,Hasht)
    end.

%% ---------
%% ensure common property fields
%% ---------
check_key_2(Type,Key,PropMap,Hasht) ->
    % typeId, (from type)
    % typeName (from type->id->name)
    % jsonKey (from key)
    % name (from title)
    % group
    % defaultValue (from default)
    % allowDirectEdit (from editor)
    % canMultipleView (from multiView)
    % multiViewHashCode (from multiView,key)
    % visibleCondition (from filter)
    {CanMultiview,MultiviewHC} = case maps:get('multiView',PropMap,undefined) of
                                     undefined -> {false,Key};
                                     key -> {true,Key};
                                     HC when is_binary(HC) -> {true,HC}
                                 end,
    PropMap1 = (maps:without([multiView],PropMap))#{
        canMultipleView => CanMultiview,
        multiViewHashCode => MultiviewHC},
    Ensures = [
        {editor, false},
        {title, Key},
        {typeName, type_name(type_id(Type))}],
    PropMap2 = lists:foldl(fun({K,Def},Acc) -> ensure(K,Acc,Def) end, PropMap1, Ensures),
    check_key_3(Type,Key,PropMap2,Hasht).

%% ---------
%% property group,
%% default value
%% ---------
check_key_3(Type,Key,PropMap,Hasht) ->
    Group = maps:get('group',PropMap,common),
    Default = maps:get('default',PropMap,null),
    PropMap1 = PropMap#{'group' => pgroup(Group),
                        'default' => default_value(Default)},
    check_key_4(Type,Key,PropMap1,Hasht).

%% ---------
%% filter
%% ---------
check_key_4(Type,Key,PropMap,Hasht) ->
    PropMap1 = case maps:get('filter',PropMap,undefined) of
                   undefined -> PropMap;
                   Filter when is_binary(Filter) ->
                       PropMap#{'filter' => u(?BU:to_list(Filter))}
               end,
    check_key_5(Type,Key,PropMap1,Hasht).


%% ---------
%% ensure special keys for property type
%% ---------

%% transfer (0)
check_key_5(<<"transfer">>=T,Key,PropMap,Hasht) ->
    % transferTitle,
    % transferCtrlButton
    % transferDigitButton
    PropMap1 = ensure('transferTitle',PropMap,<<"">>),
    PropMap2 = ensure('transferCtrlButton',PropMap1,1),
    PropMap3 = ensure('transferDigitButton',PropMap2,1),
    % TODO: duplicates
    % TODO: data types
    check_key_6(T,Key,PropMap3,Hasht);
%% yesno (9)
check_key_5(<<"yesno">>,Key,PropMap,Hasht) ->
    PropMap1 = PropMap#{'type' => <<"list">>,
                        'items' => [{0,<<"no">>,<<"Нет"/utf8>>},
                                    {1,<<"yes">>,<<"Да"/utf8>>}]},
    check_key_5(<<"list">>,Key,PropMap1,Hasht); % !!!
%% variable (11)
check_key_5(<<"variable">>=T,Key,PropMap,Hasht) ->
    % varType,
    PropMap1 = ensure('varType',PropMap,?AllVarTypes),
    % TODO: data type
    check_key_6(T,Key,PropMap1,Hasht);
%% argument (12)
check_key_5(<<"argument">>=T,Key,PropMap,Hasht) ->
    % varType,
    % argType
    PropMap1 = ensure('varType',PropMap,?AllVarTypes),
    PropMap2 = ensure('argType',PropMap1,?AllArgTypes),
    % TODO: data types
    check_key_6(T,Key,PropMap2,Hasht);
%% list (10)
check_key_5(<<"list">>=T,Key,PropMap,Hasht) ->
    % comboItems => [#{script,code,value,title}]
    case maps:get('items',PropMap,undefined) of
        undefined -> {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type '~ts' requred field '~ts'", [Key,T,items])}};
        CI when not is_list(CI) -> {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type '~ts' expected field '~ts' of [{Idx,Code}] | [{Idx,Code,Title}] | [{ScriptTypes,Idx,Code}] | [{ScriptTypes,Idx,Code,Title}] when Idx::integer(), Code::binary(), Title::binary(), ScriptTypes::[binary()]", [Key,T,items])}};
        [] -> {error,{fld_invalid_format,{Key,T,items}}};
        CI ->
            F = fun(Code,Value,Title) ->
                     #{%script => Script,
                       code => Code,
                       value => Value,
                       title => Title}
                end,
            ScriptType = ?CFG:script_type(),
            CI1 = lists:foldl(fun(_,{error,_}=Err) -> Err;
                                 ({Idx,Code},Acc) when is_integer(Idx) -> [F(Code,Idx,Code) | Acc];
                                 ({Idx,Code,Title},Acc) when is_integer(Idx) -> [F(Code,Idx,Title) | Acc];
                                 ({_,Idx,Code,Title},Acc) when is_integer(Idx), ScriptType==undefined -> [F(Code,Idx,Title) | Acc];
                                 ({_,Idx,Code},Acc) when is_integer(Idx), ScriptType==undefined -> [F(Code,Idx,Code) | Acc];
                                 ({ScriptTypes,Idx,Code,Title},Acc) when is_integer(Idx) ->
                                     case lists:member(ScriptType,ScriptTypes) of
                                         true -> [F(Code,Idx,Title) | Acc];
                                         false -> Acc
                                     end;
                                 ({ScriptTypes,Idx,Code},Acc) when is_integer(Idx) ->
                                     case lists:member(ScriptType,ScriptTypes) of
                                         true -> [F(Code,Idx,Code) | Acc];
                                         false -> Acc
                                     end;
                                 (_,_) -> {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type '~ts' has invalid items. Expected: [{Idx,Code}] | [{Idx,Code,Title}] | [{ScriptTypes,Idx,Code}] | [{ScriptTypes,Idx,Code,Title}] when Idx::integer(), Code::binary(), Title::binary(), ScriptTypes::[binary()]", [Key,T])}}
                              end, [], CI),
            case CI1 of
                {error,_}=Err -> Err;
                _ ->
                    PropMap1 = PropMap#{'items' => lists:reverse(CI1)},
                    check_key_6(T,Key,PropMap1,Hasht)
            end end;
%% cachedlist (5)
check_key_5(<<"cachedlist">>=T,Key,PropMap,Hasht) ->
    % listHashCode,
    % itemViewMode
    % itemSaveType
    case maps:get('listHashCode',PropMap,undefined) of
        undefined -> {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type '~ts' requred field '~ts'", [Key,T,listHashCode])}};
        HC when not is_binary(HC) ->  {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type '~ts' expected field '~ts' of binary()", [Key,T,listHashCode])}};
        _HC ->
            PropMap1 = ensure('itemViewMode',PropMap,<<"string">>),
            PropMap2 = ensure('itemSaveType',PropMap1,<<"string">>),
            % TODO: duplicates
            % TODO: data types
            check_key_6(T,Key,PropMap2,Hasht)
    end;
%% file (18)
check_key_5(<<"file">>=T,Key,PropMap,Hasht) ->
    % fileSubDirectory,
    % fileDescription
    % fileExtensions
    % fileMimes
    PropMap1 = ensure('fileSubDirectory',PropMap,<<"static">>),
    PropMap2 = ensure('fileDescription',PropMap1,<<"">>),
    PropMap3 = ensure('fileExtensions',PropMap2,[]),
    PropMap4 = ensure('fileMimes',PropMap3,[]),
    % TODO: duplicates
    % TODO: data types
    check_key_6(T,Key,PropMap4,Hasht);
%% menu (14)
check_key_5(T,Key,PropMap,Hasht) when T==<<"menu">>; T==<<"assigns">> ->
    case maps:get('itemtype',PropMap,undefined) of
        undefined -> {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type '~ts' requred field '~ts'", [Key,T,itemtype])}};
        ItemType when not is_list(ItemType) -> {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type '~ts' expected field '~ts' of [{binary(),map()}]", [Key,T,itemtype])}};
        [] -> {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type '~ts' expected field '~ts' of non-empty list", [Key,T,itemtype])}};
        ItemType ->
            F = fun(_, {error,_}=Err) -> Err;
                   ({Name,#{type:=_Tp}}, ok) when is_binary(Name) -> ok;
                   (_, ok) -> {error,{invalid_property,?BU:strbin("Property '~ts' invalid: type '~ts' expected field '~ts' of [{binary(),map()}] when map contains 'type'", [Key,T,itemtype])}}
                end,
            case lists:foldl(F, ok, ItemType) of
                ok -> check_key_6(T,Key,PropMap,Hasht);
                {error,_}=Err -> Err
            end end;
%% other
check_key_5(Type,Key,PropMap,Hasht) -> check_key_6(Type,Key,PropMap,Hasht).

%% -----
check_key_6(Type,Key,PropMap,Hasht) -> check_key_x(Type,Key,PropMap,Hasht).

%% ---------
%% map property values to metadata names
%% ---------
check_key_x(_Type,Key,PropMap,Hasht) ->
    Changes = [
        {filter, visibleCondition},
        {key, jsonKey},
        {title, name},
        {type, typeId},
        {default, defaultValue},
        {editor, allowDirectEdit}],
    PropMap1 = lists:foldl(fun({K1,K2},Acc) -> change_key(K1,K2,Acc) end, PropMap, Changes),
    {ok,maps:put(Key,PropMap1,Hasht)}.

%% ===============================================================

%% @private
ensure(Key,Map,Default) ->
    Map#{Key => maps:get(Key,Map,Default)}.

%% @private
change_key(Key,Key,PropMap) -> PropMap;
change_key(Key0,Key1,PropMap) ->
    case maps:is_key(Key0,PropMap) of
        false -> PropMap;
        true ->
            Val = maps:get(Key0,PropMap),
            (maps:without([Key0],PropMap))#{Key1 => Val}
    end.

%% ----------------------------
%% property type name by id (oktell's script editor types)
%% ----------------------------
type_name(0) -> <<"fetTransfer">>;                  % выбор компонента
type_name(1) -> <<"fetText">>;                      % простой текст
type_name(2) -> <<"fetLargeText">>;                 % задание большого текста (в отдельном окне)
type_name(3) -> <<"fetInterruptString">>;           % последовательности прерывания
type_name(4) -> <<"fetXMLContentForm">>;            % Форма формирования XML-запроса
type_name(5) -> <<"fetCachedList">>;                % Кэшируемый список динамических объектов (голоса, сценарии, сценарии SVC, группы клиентов, пользователи, проекты, задачи, входящие задачи, счетчики, динамические шлюзы)
type_name(6) -> <<"fetDynamicParametrizedList">>;   % Список динамических объектов, зависимых от выбранных значений в других полях. Запрос на сервер посылается всякий раз когда изменяется значение парентов.
type_name(7) -> <<"fetRoutes">>;                    % перечень направлений (маршрутов и внешних линий)
type_name(8) -> <<"fetColor">>;                     % Форма выбора цвета
type_name(9) -> <<"fetYesNo">>;                     % Да/нет
type_name(10) -> <<"fetFillCombo">>;                % произвольное заполнение ComboBox из атрибута. выход в CIntString. По факту только Int
type_name(11) -> <<"fetVariable">>;                 % выбор переменной-назначения
type_name(12) -> <<"fetArgument">>;                 % выбор аргумента
type_name(13) -> <<"fetOperations">>;               % множественные операции присвоения
type_name(14) -> <<"fetMenu">>;                     % форма выбора значений меню
type_name(15) -> <<"fetMailbox">>;                  % адресат голосовой почты, уведомления, прочих рассылок
type_name(16) -> <<"fetValueGrouping">>;            % список возможных значений и преобразования
type_name(17) -> <<"fetSQL">>;                      % вызов формы ввода SQL запроса
type_name(18) -> <<"fetAttachedFile">>;             % Форма выбора прикрепленного файла (указывается расширения возможные, описание типа файлов, подкаталог для хранения в сценарии)
type_name(19) -> <<"fetWebFormSrc">>;               % Форма выбора источника веб-формы
type_name(20) -> <<"fetPluginApi">>;                % плагин-модуль/метод/параметры
type_name(21) -> <<"fetPluginModuleForm">>;         % список плагинов подключенных, формы диалоговые
type_name(22) -> <<"fetFormPropertiesPlugin">>;     % компонент задания свойств параметров плагина (pluginform, plugin, visualform/dynamic)
type_name(23) -> <<"fetFormPropertiesDlgForm">>;    % компонент задания свойств диалоговой форме (dlgform)
type_name(24) -> <<"fetFormPropertiesDlgWebForm">>; % компонент задания свойств веб-форме диалога общего образца (webdlgform)
type_name(25) -> <<"fetDynamicProperties">>;        % компонент связи с Bpium. Динамический набор свойств (с сервера загружается в момент входа)
type_name(26) -> <<"fetParametersKV">>;             % параметры name-value для SQL запроса и др.
type_name(27) -> <<"fetParametersV">>.              % параметры value для SQL запроса и др.

%%
type_id(<<"transfer">>) -> 0; % <<"fetTransfer">>;
type_id(<<"string">>) -> 1; % <<"fetText">>;
type_id(<<"text">>) -> 2; % <<"fetLargeText">>;
type_id(<<"interrupt">>) -> 3; % <<"fetInterruptString">>;
%type_id(<<"">>) -> 4; % <<"fetXMLContentForm">>;
type_id(<<"cachedlist">>) -> 5; % <<"fetCachedList">>;
type_id(<<"dynlist">>) -> 6; % <<"fetDynamicParametrizedList">>;
%type_id(<<"">>) -> 7; % <<"fetRoutes">>;
type_id(<<"color">>) -> 8; % <<"fetColor">>;
type_id(<<"yesno">>) -> 9; % <<"fetYesNo">>;
type_id(<<"list">>) -> 10; % <<"fetFillCombo">>;
type_id(<<"variable">>) -> 11; % <<"fetVariable">>;
type_id(<<"argument">>) -> 12; % <<"fetArgument">>;
type_id(<<"assigns">>) -> 13; % <<"fetOperations">>;
type_id(<<"menu">>) -> 14; % <<"fetMenu">>;
type_id(<<"recipient">>) -> 15; % <<"fetMailbox">>;
%type_id(<<"">>) -> 16; % <<"fetValueGrouping">>;
type_id(<<"sql">>) -> 17; % <<"fetSQL">>;
type_id(<<"attach">>) -> 18; % <<"fetAttachedFile">>;
%type_id(<<"">>) -> 19; % <<"fetWebFormSrc">>;
%type_id(<<"">>) -> 20; % <<"fetPluginApi">>;
%type_id(<<"">>) -> 21; % <<"fetPluginModuleForm">>;
%type_id(<<"">>) -> 22; % <<"fetFormPropertiesPlugin">>;
%type_id(<<"">>) -> 23; % <<"fetFormPropertiesDlgForm">>;
%type_id(<<"">>) -> 24; % <<"fetFormPropertiesDlgWebForm">>;
%type_id(<<"">>) -> 25; % <<"fetDynamicProperties">>;
type_id(<<"paramskv">>) -> 26; % <<"fetParametersKV">>;
type_id(<<"paramsv">>) -> 27. % <<"fetParametersV">>.

%% ----------------------------
%% groups of properties
%% ----------------------------
pgroup(common) -> <<"Общие"/utf8>>;
pgroup(request) -> <<"Запрос"/utf8>>;
pgroup(connect) -> <<"Подключение"/utf8>>;
pgroup(data) -> <<"Данные"/utf8>>;
pgroup(response) -> <<"Ответ"/utf8>>;
pgroup(undefined) -> u("Прочие");
pgroup(Group) when is_atom(Group) -> <<"Прочие"/utf8>>;
pgroup(Group) when is_list(Group) -> ?BU:to_binary(u(Group));
pgroup(Group) when is_binary(Group) -> ?BU:to_binary(Group).

%% ----------------------------
%% build default value
%% ----------------------------
default_value(V) when is_list(V) -> u(V);
default_value(V) when is_integer(V) -> V;
default_value(V) when is_binary(V) -> V;
default_value(V) when is_number(V) -> V;
default_value(null) -> null;
default_value(undefined) -> null.

%% ----------------------------
%%
%% ----------------------------
group(basic=G) ->
    #{groupCode => ?BU:to_binary(G),
      groupName => <<"Базовые"/utf8>>,
      backColor => color(16#FFFFFF)};

group(management=G) ->
    #{groupCode => ?BU:to_binary(G),
      groupName => <<"Управление"/utf8>>,
      backColor => color(16#ECF4E8)};

group(data=G) ->
    #{groupCode => ?BU:to_binary(G),
      groupName => <<"Работа с данными"/utf8>>,
      backColor => color(16#E8EAEC)};

group(messaging=G) ->
    #{groupCode => ?BU:to_binary(G),
      groupName => <<"Обмен сообщениями"/utf8>>,
      backColor => color(16#F7F2E6)};

group(_) ->
    #{groupCode => <<"common">>,
      groupName => <<"Разное"/utf8>>,
      backColor => color(16#FFFFFF)}.