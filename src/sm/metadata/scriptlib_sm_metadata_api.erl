%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 05.08.2021
%%% @doc API handler for script editor

-module(scriptlib_sm_metadata_api).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/0]).

-export([
     check_expression/3,
     check_template/3,
     expression_to_template/3,
     template_to_expression/3
]).

-export([get_dynamic_list/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").
-include("metadata.hrl").

%% ====================================================================
%% API functions
%% Metadata
%% ====================================================================

%% ---------------------------------
%% return metadata of script machine
%% ---------------------------------
-spec metadata() -> map().
%% ---------------------------------
metadata() -> ?SCR_METADATA:metadata().

%% ====================================================================
%% API functions
%% Expressions check and transform
%% ====================================================================

%% ---------------------------------
%% check expression
%% ---------------------------------
-spec check_expression(ScriptTypeId::integer(), Expr::binary(), Vars::list()) -> ok | {error,Reason::term()} | {error,Reason::binary()}.
%% ---------------------------------
check_expression(ScriptTypeId,Expr,Vars) ->
    ?SCR_META_EXPR:check_expression(ScriptTypeId, Expr, Vars).

%% ---------------------------------
%% check template
%% ---------------------------------
-spec check_template(ScriptTypeId::integer(), Templ::binary(), Vars::list()) -> ok | {error,Reason::term()} | {error,Reason::binary()}.
%% ---------------------------------
check_template(ScriptTypeId,Templ,Vars) ->
    ?SCR_META_EXPR:check_template(ScriptTypeId, Templ, Vars).

%% ---------------------------------
-spec expression_to_template(ScriptTypeId::integer(), Expr::binary(), Vars::list()) -> {ok,Templ::binary()} | {error,Reason::term()}.
%% ---------------------------------
expression_to_template(ScriptTypeId,Expr,Vars) ->
    ?SCR_META_EXPR:expression_to_template(ScriptTypeId,Expr,Vars).

%% ---------------------------------
-spec template_to_expression(ScriptTypeId::integer(), Templ::binary(), Vars::list()) -> {ok,Expr::binary()} | {error,Reason::term()}.
%% ---------------------------------
template_to_expression(ScriptTypeId,Expr,Vars) ->
    ?SCR_META_EXPR:template_to_expression(ScriptTypeId,Expr,Vars).

%% ====================================================================
%% API functions
%% Dynamic lists data
%% ====================================================================

%% ---------------------------------
%% return dynamic list by code (from properties of components)
%% ---------------------------------
-spec get_dynamic_list(Domain::term(), ScriptType::term(), Key::string()) -> jsonlist().
%% ---------------------------------
get_dynamic_list(Domain,ScriptType,Key) ->
    ?SCR_META_DYNLISTS:get_dynamic_list(Domain,ScriptType,Key).
