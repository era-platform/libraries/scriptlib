%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc Dyamic lists in metadata

-module(scriptlib_sm_metadata_dynamic_list).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_dynamic_list/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").
-include("metadata.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

get_dynamic_list(Domain,ScriptType,Key) ->
    case ?BU:to_binary(Key) of
        <<"component#",Rest/binary>> ->
            case binary:split(Rest,<<"#">>) of
                [ModuleB,SubKey] ->
                    Module = ?BU:to_atom_new(ModuleB),
                    case ?BU:function_exported(Module,get_dynamic_list,3) of
                        true -> Module:get_dynamic_list(Domain,ScriptType,SubKey);
                        false -> []
                    end;
                _ -> []
            end;
        _ ->
            case ?CFG:get_dynamic_list(Domain,ScriptType,Key) of
                undefined -> [];
                V -> V
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================