%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc

-module(scriptlib_sm_metadata_expr).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    expression_to_template/3,
    template_to_expression/3
]).

-export([
    check_expression/3,
    check_template/3
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").
-include("metadata.hrl").

%% ====================================================================
%% API functions
%% Transform expression <-> template
%% ====================================================================

%% --------------------------------------------
%% transform expression to template
%% --------------------------------------------
expression_to_template(_ScriptTypeId,Expr,_Vars) ->
    % states: empty, plus, expr, exprlus, quot, dblquot, {bracket,Cnt}
    try
        Templt = lists:foldl(fun($',{empty,Acc}) -> {quot,Acc};
                                ($",{empty,Acc}) -> {dblquot,Acc};
                                ($',{{plus,_},Acc}) -> {quot,Acc};
                                ($",{{plus,_},Acc}) -> {dblquot,Acc};
                                ($',{expr,Acc}) -> {quot,[$}|Acc]};
                                ($",{expr,Acc}) -> {dblquot,[$}|Acc]};
                                ($',{{exprplus,_},Acc}) -> {quot,[$}|Acc]};
                                ($",{{exprplus,_},Acc}) -> {dblquot,[$}|Acc]};
                                ($',{quot,Acc}) -> {empty,Acc};
                                ($",{dblquot,[$\\|Acc]}) -> {dblquot,[$"|Acc]};
                                ($",{dblquot,Acc}) -> {empty,Acc};
                                %
                                ($ ,{empty,Acc}) -> {empty,Acc};
                                ($\t,{empty,Acc}) -> {empty,Acc};
                                ($\r,{empty,Acc}) -> {empty,Acc};
                                ($\n,{empty,Acc}) -> {empty,Acc};
                                %
                                ($ ,{expr,Acc}) -> {expr,Acc};
                                ($\t,{expr,Acc}) -> {expr,Acc};
                                ($\r,{expr,Acc}) -> {expr,Acc};
                                ($\n,{expr,Acc}) -> {expr,Acc};
                                % plus
                                ($ ,{{plus,1},Acc}) -> {{plus,s1},Acc};
                                ($\t,{{plus,1},Acc}) -> {{plus,s1},Acc};
                                ($\r,{{plus,1},Acc}) -> {{plus,s1},Acc};
                                ($\n,{{plus,1},Acc}) -> {{plus,s1},Acc};
                                %
                                ($ ,{{plus,2},Acc}) -> {{plus,s2},Acc};
                                ($\t,{{plus,2},Acc}) -> {{plus,s2},Acc};
                                ($\r,{{plus,2},Acc}) -> {{plus,s2},Acc};
                                ($\n,{{plus,2},Acc}) -> {{plus,s2},Acc};
                                %
                                ($ ,{{plus,_}=Plus,Acc}) -> {Plus,Acc};
                                ($\t,{{plus,_}=Plus,Acc}) -> {Plus,Acc};
                                ($\r,{{plus,_}=Plus,Acc}) -> {Plus,Acc};
                                ($\n,{{plus,_}=Plus,Acc}) -> {Plus,Acc};
                                % exprplus
                                ($ ,{{exprplus,1},Acc}) -> {{exprplus,s1},Acc};
                                ($\t,{{exprplus,1},Acc}) -> {{exprplus,s1},Acc};
                                ($\r,{{exprplus,1},Acc}) -> {{exprplus,s1},Acc};
                                ($\n,{{exprplus,1},Acc}) -> {{exprplus,s1},Acc};
                                %
                                ($ ,{{exprplus,2},Acc}) -> {{exprplus,s2},Acc};
                                ($\t,{{exprplus,2},Acc}) -> {{exprplus,s2},Acc};
                                ($\r,{{exprplus,2},Acc}) -> {{exprplus,s2},Acc};
                                ($\n,{{exprplus,2},Acc}) -> {{exprplus,s2},Acc};
                                %
                                ($ ,{{exprplus,_}=ExprPlus,Acc}) -> {ExprPlus,Acc};
                                ($\t,{{exprplus,_}=ExprPlus,Acc}) -> {ExprPlus,Acc};
                                ($\r,{{exprplus,_}=ExprPlus,Acc}) -> {ExprPlus,Acc};
                                ($\n,{{exprplus,_}=ExprPlus,Acc}) -> {ExprPlus,Acc};
                                %
                                ($+,{empty,Acc}) -> {{plus,1},Acc};
                                ($+,{expr,Acc}) -> {{exprplus,1},Acc};
                                ($+,{{plus,1},Acc}) -> {{plus,2},Acc};
                                ($+,{{plus,2},_}) -> throw({error,"Invalid syntax near ++"});
                                ($+,{{plus,s1},_}) -> throw({error,"Invalid syntax near + "});
                                ($+,{{plus,s2},_}) -> throw({error,"Invalid syntax near ++ "});
                                ($+,{{exprplus,1},Acc}) -> {{exprplus,2},Acc};
                                ($+,{{exprplus,2},_}) -> throw({error,"Invalid syntax near ++"});
                                ($+,{{exprplus,s1},_}) -> throw({error,"Invalid syntax near + "});
                                ($+,{{exprplus,s2},_}) -> throw({error,"Invalid syntax near ++ "});

                                %
                                ($(,{expr,Acc}) -> {{bracket,1},[$(|Acc]};
                                ($(,{{bracket,B},Acc}) -> {{bracket,B+1},[$(|Acc]};
                                ($),{{bracket,1},Acc}) -> {expr,[$)|Acc]};
                                ($),{{bracket,B},Acc}) -> {{bracket,B-1},[$)|Acc]};
                                ($),{expr,_}) -> throw({error,"Invalid syntax near )"});
                                ($),{{plus,_},_}) -> throw({error,"Invalid syntax near )"});
                                ($),{{exprplus,_},_}) -> throw({error,"Invalid syntax near )"});
                                ($(,{{plus,_},_}) -> throw({error,"Invalid syntax near ("});
                                ($(,{{exprplus,_},_}) -> throw({error,"Invalid syntax near ("});
                                %
                                (${,{quot,Acc}) -> {quot,[${,$\\|Acc]};
                                (${,{dblquot,Acc}) -> {dblquot,[${,$\\|Acc]};
                                ($},{quot,Acc}) -> {quot,[$},$\\|Acc]};
                                ($},{dblquot,Acc}) -> {dblquot,[$},$\\|Acc]};
                                %
                                (Ch,{empty,Acc}) -> {expr,[Ch,${|Acc]};
                                (Ch,{quot,Acc}) -> {quot,[Ch|Acc]};
                                (Ch,{dblquot,Acc}) -> {dblquot,[Ch|Acc]};
                                (Ch,{{plus,_},Acc}) -> {expr,[Ch,${|Acc]};
                                (Ch,{expr,Acc}) -> {expr,[Ch|Acc]};
                                (Ch,{{exprplus,_},Acc}) -> {expr,[Ch,$+|Acc]};
                                (Ch,{{bracket,B},Acc}) -> {{bracket,B},[Ch|Acc]};
                                %
                                (Ch,_) -> throw({error,"Invalid syntax near " ++ [Ch]})
                             end, {empty,[]}, ?BU:to_unicode_list(Expr)),
        case Templt of
            {expr,T} -> {ok,?BU:to_binary(lists:reverse([$}|T]))};
            {empty,T} -> {ok,?BU:to_binary(lists:reverse(T))};
            _ -> throw({error,"Unexpected ending"})
        end
    catch
        throw:{error,_}=Err -> Err
    end.

%% --------------------------------------------
%% transform template to expression
%% --------------------------------------------
template_to_expression(_ScriptTypeId,Templ,_Vars) ->
    % states: empty, expr, string, bracket.
    try
        Expr = lists:foldl(fun(${,{empty,Acc}) -> {expr,Acc};
                              ($},{expr,Acc}) -> {string,[$",$+|Acc]};
                              ($",{empty,Acc}) -> {string,[$",$\\,$"|Acc]}; %% #364
                              (Ch,{empty,Acc}) -> {string,[Ch,$"|Acc]};
                              ($(,{expr,Acc}) -> {{bracket,1},[$(|Acc]};
                              ($(,{{bracket,N},Acc}) -> {{bracket,N+1},[$(|Acc]};
                              ($),{{bracket,1},Acc}) -> {expr,[$)|Acc]};
                              ($),{{bracket,N},Acc}) -> {{bracket,N-1},[$)|Acc]};
                              %
                              (Ch,{expr,Acc}) -> {expr,[Ch|Acc]};
                              (Ch,{{bracket,N},Acc}) -> {{bracket,N},[Ch|Acc]};
                              %
                              %($",{string,Acc}) -> {string,lists:reverse("dblquot()")++Acc};
                              ($",{string,Acc}) -> {string,[$",$\\|Acc]};
                              (${,{string,[$\\|Acc1]}) -> {string,[${|Acc1]};
                              ($},{string,[$\\|Acc1]}) -> {string,[$}|Acc1]};
                              (${,{string,Acc}) -> {expr,[$+,$"|Acc]};
                              ($},{string,_}) -> throw({error,"Invalid syntax near }"});
                              (Ch,{string,Acc}) -> {string,[Ch|Acc]}
                           end, {empty,[]}, ?BU:to_unicode_list(Templ)),
        case Expr of
            {string,T} -> case T of [$",$+,$+|RT] -> {ok,?BU:to_binary(lists:reverse(RT))}; _ -> {ok,?BU:to_binary(lists:reverse([$"|T]))} end;
            {empty,T} -> {ok,?BU:to_binary(lists:reverse(T))};
            _ -> throw({error,"Unexpected ending"})
        end
    catch
        throw:{error,_}=Err -> Err
    end.

%% ====================================================================
%% API functions
%% Check expression, template
%% ====================================================================

%% --------------------------------------------
%% check expression
%% --------------------------------------------
check_expression(_ScriptType,Expr,Vars) ->
    %% -- Variables --
    % default var values by types
    FVal = fun(T) -> ?SCRU:default_value(?SCRU:map_vartype(T)) end,
    % build vars
    Vars1 = lists:foldl(fun(V,Acc) ->
                                [VN,VT] = ?BU:extract_optional_props([<<"name">>,<<"type">>],V),
                                [#variable{id=?BU:newid(),name=VN,type=VT,location='local',value=FVal(VT)}|Acc]
                        end, [], Vars),
    %% -- Functions --
    % metamodule
    MetaModule = ?CFG:meta_module(),
    % meta
    MetaF = ?SCR_META_FUNS:meta_expr(),
    MetaF1 = case ?BU:function_exported(MetaModule,meta_expr,0) of
                 false -> MetaF;
                 true -> MetaModule:meta_expr() ++ MetaF
             end,
    MetaF2 = lists:map(fun({FName,Params}) ->
                               P1 = case binary:split(?BU:to_binary(Params), <<",">>, [global]) of [<<>>] -> []; P2 -> P2 end,
                               Fun = case PLen=length(P1) of
                                         0 -> fun() -> <<>> end;
                                         1 -> fun(_) -> <<>> end;
                                         2 -> fun(_,_) -> <<>> end;
                                         3 -> fun(_,_,_) -> <<>> end;
                                         4 -> fun(_,_,_,_) -> <<>> end;
                                         5 -> fun(_,_,_,_,_) -> <<>> end
                                     end,
                               {{?BU:to_atom_new(FName),PLen},Fun}
                       end, MetaF1),
    %% -- State --
    State = #state_scr{variables=Vars1,
                       meta=#meta{functions=MetaF2}},
    %% -- Apply --
    try
        _X = ?SCR_EXPR:apply(Expr,MetaF2,State),
        ok
    catch _:_R -> {error,<<"Invalid expression">>}
    end.

%% --------------------------------------------
%% check expression
%% --------------------------------------------
check_template(ScriptTypeId,Templ,Vars) ->
    {ok,Expr} = template_to_expression(ScriptTypeId,Templ,Vars),
    check_expression(ScriptTypeId,Expr,Vars).

%% ====================================================================
%% Internal functions
%% ====================================================================
