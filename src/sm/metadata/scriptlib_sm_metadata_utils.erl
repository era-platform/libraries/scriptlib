%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc Utils of script metadata

-module(scriptlib_sm_metadata_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    color/1,
    u/1
]).

%% ====================================================================
%% Define
%% ====================================================================

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------------
%% build color
%% ----------------------------------
color(black) -> 0;
color(white) -> color({255,255,255});
color(gray) -> color({128,128,128});
color(red) -> color({255,0,0});
color(dark) -> color({128,0,0});
color(green) -> color({0,255,0});
color(darkgreen) -> color({0,128,0});
color(blue) -> color({0,0,255});
color(darkblue) -> color({0,0,128});
color(orange) -> color({255,255,0});
color({R,G,B}) when R>=0, R<256, G>=0, G<256, B>=0, B<256 -> (R*256 + G)*256 + B;
color(I) when is_integer(I), I>=0, I=<16#FFFFFF -> I.

%% ----------------------------------
%% build unicode string
%% ----------------------------------
u(S) when is_list(S) -> unicode:characters_to_binary(S);
u(B) when is_binary(B) -> B.

%% ====================================================================
%% Internal functions
%% ====================================================================
