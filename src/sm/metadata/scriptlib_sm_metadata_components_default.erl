%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc

-module(scriptlib_sm_metadata_components_default).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    default_components/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

default_components() ->
    #{
        <<"region">> => scriptlib_sm_component_region,
        <<"start">> => scriptlib_sm_component_start,
        <<"poststart">> => scriptlib_sm_component_poststart,
        <<"stop">> => scriptlib_sm_component_stop
    }.

%% ====================================================================
%% Internal functions
%% ====================================================================