%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 05.08.2021
%%% @doc Prepare metadata of components

-module(scriptlib_sm_metadata_components).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([objects/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").
-include("metadata.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------------
%% return all objects metadata (build metadata)
%% object:{defaultname,hint,serialIndex,[isRegion],image_base64,type,scriptType,groupCode,groupName,backColor,properties}
%% properties:{typeId,typeName,jsonKey,name,group,defaultValue,allowDirectEdit,canMultipleView,multiViewHashCode}
%% -------------------------------
-spec objects() -> map().
%% -------------------------------
objects() ->
    DefaultComponents = ?SCR_COMPONENTS_DEFAULT:default_components(),
    ExComponents = case ?CFG:components() of
                       Map when is_map(Map) -> Map;
                       _ -> #{}
                   end,
    AvailableComponents = maps:to_list(maps:merge(DefaultComponents,ExComponents)),
    % build component map
    ScriptType = ?CFG:script_type(),
    R = lists:foldr(fun({CT,CM},Acc) ->
                          FErr = fun({error,_}=Err) -> ?OUT('$error', "Script metadata. Component ~120tp, ~ts: ~500tp", [CT, CM, Err]) end,
                          case ?BU:function_exported(CM,metadata,1) of
                              false -> FErr({error,{invalid,?BU:strbin("Component ~ts:metadata/0 is not exported",[CM])}}), Acc;
                              true ->
                                  case CM:metadata(ScriptType) of
                                      V when not is_map(V) -> FErr({error,{invalid,?BU:strbin("Component ~ts:metadata/0 return unexpected type. Expected map()",[CM])}}), Acc;
                                      CMeta ->
                                          Visible = maps:get('visible',CMeta,true),
                                          Show = case maps:get('scriptType',CMeta,undefined) of
                                                     undefined -> Visible;
                                                     _ when ScriptType==undefined -> Visible;
                                                     AllowedTypes when is_list(AllowedTypes) -> lists:member(ScriptType,AllowedTypes) andalso Visible;
                                                     ScriptType -> Visible;
                                                     _ -> false
                                                 end,
                                          case Show of
                                              false -> Acc;
                                              true ->
                                                  CMeta1 = CMeta#{'type' => CT},
                                                  CMeta2 = case maps:get('image_base64',CMeta1,undefined) of
                                                               undefined -> CMeta1#{'image_base64' => image(CT)};
                                                               _ -> CMeta1
                                                           end,
                                                  % CMeta2#{scriptType => ?All},
                                                  case ?SCR_META_TRANS_VERIFY:verify_metadata(CMeta2) of
                                                      {error,_}=Err -> FErr(Err), Acc;
                                                      {ok,CMeta3} -> [CMeta3|Acc]
                                                  end end end end end, [], AvailableComponents),
    R.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------------------
%% images for components
%% ----------------------------------------------

%% -------------------------------
%% from destination module. if there is no image, so from current priv dir by typeid
%% -------------------------------
-spec image(CT::binary()) -> ImageB64::binary().
%% -------------------------------
image(CT) ->
    case ?CFG:icon(CT) of
        undefined -> image_fromdisk(CT);
        Data -> Data
    end.

%% -------------------------------
%% from current priv dir, for common and other components by typeid
%% -------------------------------
image_fromdisk(CT) ->
    Exts = [".png",".svg"],
    Res = lists:foldl(fun(_,{ok,_}=Acc) -> Acc;
                         (Ext,_) -> image_fromdisk(CT,Ext)
                      end, undefined, Exts),
    case Res of
        {error,_} -> image_default();
        {ok,Data} -> base64:encode(Data)
    end.

image_fromdisk(CT,Ext) ->
    Dir = ?CFG:icons_path(),
    FName = ?BU:to_list(CT) ++ Ext,
    ?BLfile:read_file(filename:join([Dir,FName])).

%% -------------------------------
%% default image (question, etc)
%% -------------------------------
image_default() -> <<>>.
