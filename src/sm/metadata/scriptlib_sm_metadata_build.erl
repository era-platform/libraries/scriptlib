%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc Facade script metadata module.
%%%      Prepares common metadata for script editor.

-module(scriptlib_sm_metadata_build).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([metadata/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").
-include("metadata.hrl").

-import(?SCR_META_UTILS, [color/1, u/1]).

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------
-spec metadata() -> map().
%% -------------------------
metadata() ->
    #{
        varTypes => var_types(),
        varLocations => var_locations(),
        argTypes => arg_types(),
        expressionFunctions => ?SCR_META_FUNS:expression_functions(),
        objects => ?SCR_META_COMPONENTS:objects()
    }.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------------------------------------
-spec var_types() -> map().
%% -------------------------------------------------------------------
var_types() ->
    [
        x_vartype(1,{<<"vtDouble">>,<<"Число"/utf8>>,black,true,true}),
        x_vartype(2,{<<"vtString">>,<<"Строка"/utf8>>,black,true,true}),
        x_vartype(4,{<<"vtDateTime">>,<<"Дата/время"/utf8>>,black,true,true})
    ].

%% @private
x_vartype(Type,{TypeStr,Name,Color,CanModify,CanSetLoc})
  when is_integer(Type), is_binary(TypeStr), is_binary(Name), is_boolean(CanModify), is_boolean(CanSetLoc) ->
    #{
        type => Type,
        typeStr => u(TypeStr),
        name => ?BU:to_binary(u(Name)),
        color => color(Color),
        canModify => CanModify,
        canSetLocation => CanSetLoc
    }.

%% -------------------------------------------------------------------
-spec var_locations() -> map().
%% -------------------------------------------------------------------
var_locations() ->
    [
        x_varloc(0,{<<"Local">>,black,7,<<"Локальная"/utf8>>})
    ].

%% @private
x_varloc(Type,{TypeStr,Color,AllowedVT,Name})
  when is_integer(Type), is_binary(TypeStr), is_integer(AllowedVT), is_binary(Name) ->
    #{
        type => Type,
        typeStr => u(TypeStr),
        color => color(Color),
        allowedVarTypes => AllowedVT,
        name => ?BU:to_binary(u(Name))
    }.

%% -------------------------------------------------------------------
-spec arg_types() -> map().
%% -------------------------------------------------------------------
arg_types() ->
    [
        x_argtype(1,{<<"atConstant">>,<<"Константа"/utf8>>}),
        x_argtype(2,{<<"atVariable">>,<<"Переменная"/utf8>>}),
        x_argtype(8,{<<"atExpression">>,<<"Выражение"/utf8>>}),
        x_argtype(32,{<<"atTemplate">>,<<"Шаблон"/utf8>>})
    ].

%% @private
x_argtype(Type,{TypeStr,Name})
  when is_integer(Type), is_binary(TypeStr), is_binary(Name) ->
    #{
        type => Type,
        typeStr => u(TypeStr),
        name => ?BU:to_binary(u(Name))
    }.