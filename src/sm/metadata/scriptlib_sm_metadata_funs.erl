%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc Prepare metadata of funs (expressions)

-module(scriptlib_sm_metadata_funs).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([expression_functions/0]).
-export([meta_expr/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").
-include("metadata.hrl").

-import(?SCR_META_UTILS, [color/1, u/1]).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------
%% returns all of expression functions
%% ---------------------
-spec expression_functions() -> map().
%% ---------------------
expression_functions() ->
    In = internal_functions(),
    case ?CFG:expression_functions() of
        [_|_]=Ex -> merge(In,Ex);
        _ -> In
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
merge(In,Ex) ->
    F = fun F(As,Bs,[]) -> (As--Bs) ++ Bs;
            F(As,Bs,[{Key,ListKey}|Rest]) ->
                   lists:foldl(fun(B,Acc) ->
                                    KeyVal = maps:get(Key,B),
                                    FA = fun(A,false) ->
                                                case maps:get(Key,A) of
                                                    KeyVal -> A;
                                                    _ -> false
                                                end;
                                            (_,AccA) -> AccA
                                         end,
                                    case lists:foldl(FA, false, As) of
                                        false -> Acc ++ [B];
                                        A -> (Acc--[A]) ++ A#{ListKey => F(maps:get(ListKey,A),maps:get(ListKey,B),Rest)}
                                    end end, As, Bs)
        end,
    F(In,Ex,[{code,groups},{code,functions}]).

%% @private
internal_functions() ->
    [
        #{
            code => <<"common">>,
            name => <<"Общие функции"/utf8>>,
            groups => [
                g(<<"math">>, <<"Математические"/utf8>>, f(expr(common,math))),
                g(<<"string">>, <<"Строковые"/utf8>>, f(expr(common,string))),
                g(<<"datetime">>, <<"Дата/время"/utf8>>, f(expr(common,datetime))),
                g(<<"transform">>, <<"Преобразования"/utf8>>, f(expr(common,transform))),
                g(<<"hashcoode">>, <<"Хеш-суммы"/utf8>>, f(expr(common,hashcode))),
                g(<<"path">>, <<"Файлы и пути"/utf8>>, f(expr(common,path))),
                g(<<"svcfunc">>, <<"Служебные функции"/utf8>>, f(expr(common,svcfunc))),
                g(<<"access">>, <<"Доступ"/utf8>>, f(expr(common,access))),
                g(<<"config">>, <<"Конфигурация"/utf8>>, f(expr(common,config))) ]},
        #{
            code => <<"constants">>,
            name => <<"Константы и символы"/utf8>>,
            groups => [
                g(<<"const">>, <<"Константы"/utf8>>, f(expr(const,const))),
                g(<<"symbol">>, <<"Символы"/utf8>>, f(expr(const,symbol))) ]}
    ].

%% ---------------------
%% make group item
%% ---------------------
g(_,_,[]) -> #{};
g(Code,Name,Funs) ->
    #{
        code => Code,
        name => Name,
        functions => Funs
    }.

%% ---------------------
%% make function item
%% ---------------------
f(List) ->
    lists:usort(
      lists:map(fun({Fname,Params}) ->
                          #{name => ?BU:to_binary(u(Fname)),
                            params => ?BU:to_binary(u(Params))}
                end, List)).

%% ---------------------
%% general funs
%% ---------------------
expr(common,math) ->
    [{"sin", "num"},
     {"cos", "num"},
     {"tan", "num"},
     {"asin", "num"},
     {"acos", "num"},
     {"atan", "num"},
     {"atan2", "num,num"},
     {"sinh", "num"},
     {"cosh", "num"},
     {"tanh", "num"},
     %{"asinh", "num"},
     %{"acosh", "num"},
     %{"atanh", "num"},
     {"exp", "num"},
     {"log", "num"},
     {"log2", "num"},
     {"log10", "num"},
     {"pow", "num,num"},
     {"sqrt", "num"},
     {"erf", "num"},
     {"erfc", "num"},
     % erlang
     {"max", "num,num"},
     {"min", "num,num"},
     {"trunc", "num"},
     {"round", "num"},
     {"abs", "num"},
     % oktell
     {"floor", "num"},
     {"ceil", "num"},
     {"lg", "num"},
     {"ln", "num"},
     {"log", "num,num"},
     {"sqr", "num"},
     % random
     {"random", ""},
     {"random", "int"},
     {"random", "int,int"}];

expr(common,string) ->
    [{"newid", ""},
     {"len", "str"},
     {"concat", "str,str"},
     {"equal", "str,str"},
     {"str", "str,str"},
     {"rstr", "str,str"},
     {"substr", "str,int"},
     {"substr", "str,int,int"},
     {"replace", "str,str,str"},
     {"lower", "str"},
     {"upper", "str"},
     {"remove", "str,int"},
     {"remove", "str,int,int"},
     {"indexof", "str,str"},
     {"indexof", "str,str,int"},
     {"trim", "str"},
     {"ltrim", "str"},
     {"rtrim", "str"},
     {"trimstart", "str"},
     {"trimend", "str"},
     {"left", "str,int"},
     {"right", "str,int"},
     {"reverse", "str"},
     {"regexreplace", "str,str,str"},
     {"regexreplaceg", "str,str,str"},
     {"modify_json", "str,str,str"}];

expr(common,datetime) ->
    [{"nowtick", ""},
     {"now", ""},
     {"nowutc", ""},
     {"nowgregsecond", ""},
     {"ticktolocal", "int"},
     {"ticktoutc", "int"},
     {"dateformat", "str, dt"},
     {"date", ""},
     {"date", "dt"},
     {"time", ""},
     {"time", "dt"},
     {"datetime", "str"},
     {"datetime", "int,int,int"},
     {"datetime", "int,int,int,int,int"},
     {"datetime", "int,int,int,int,int,int"},
     {"datetime", "int,int,int,int,int,int,int"},
     {"datediff", "atom,dt,dt"},
     {"dateadd", "atom,int,dt"},
     {"year", "dt"},
     {"month", "dt"},
     {"day", "dt"},
     {"hour", "dt"},
     {"minute", "dt"},
     {"second", "dt"},
     {"millisecond", "dt"},
     {"dayofyear", "dt"},
     {"dayofweek", "dt"},
     {"weekofyear", "dt"},
     {"isleapyear", "int"},
     {"datetimelocal", "dt"},
     {"datetimeutc", "dt"},
     {"localtoutc", "dt"},
     {"utctolocal", "dt"},
     {"valid_date", "dt"}];

expr(common,transform) ->
    [{"char", "int"},
     {"str", "int"},
     {"num", "str"},
     {"numval", "str"},
     {"dechex", "int"},
     {"hexdec", "str"},
     {"base64decode", "str"},
     {"base64encode", "str"},
     {"urldecode", "str"},
     {"urlencode", "str"},
     {"htmlencode", "str"},
     {"htmldecode", "str"},
     {"escape", "str"},
     {"unescape", "str"},
     {"translit", "str"},
     {"ifelse", "bool,any,any"}];

expr(common,hashcode) ->
    [{"md5", "str"},
     {"crc32", "any"},
     {"crc32", "num,any"},
     {"adler32", "any"},
     {"adler32", "num,any"},
     {"phash2", "any"}];

expr(common,path) ->
    [{"filename", "str"},
     {"fileext", "str"},
     {"filedir", "str"},
     {"makepath", "str,str"}];

expr(common,svcfunc) ->
    [{"startparam", "int"},
     {"getscriptref", ""},
     {"get_project_id",""},
     {"component","int(binary,atom),binary(map,proplist)"},
     {"component","int(binary,atom),binary(map,proplist),atom"},
     {"lock","any,int"},
     {"lock","any,atom,int,int"},
     {"unlock","any"}];

expr(common,access) ->
    [{"check_variable", "str"},
     {"get_variable_value", "str"},
     {"get_variable_value", "str, any"},
     {"get_variable_id", "str"},
     {"get_variable_orig", "str"},
     {"set_variable_value_if_undefined", "str, any"},
     {"var", "str"},
     {"storage_put", "str, any"},
     {"storage_put", "str, any, int"},
     {"storage_get", "str, any"},
     {"storage_del", "str"}];

expr(common,config) ->
    [{"site", ""},
     {"domain", ""},
     {"parentdomain", ""},
     {"isworktime", ""},
     {"isworktime", "int"}];

% --------------

expr(const,const) ->
    [{"e",""},
     {"pi",""},
     {"phi",""}];

expr(const,symbol) ->
    [{"tab", ""},
     {"endline", ""},
     {"quot", ""},
     {"dblquot", ""}].

%% ---------------------
%% return expression funs (for expression validator)
%% Functions that should be overriden by dummies
%% ---------------------
meta_expr() ->
 [{"startparam","int"},
  {"getscriptref",""},
  {"gs_chatsess","str,str"},
  {"gs_chatsess_common",""},
  {"get_project_id",""}].
