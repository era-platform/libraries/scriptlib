%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc Script-machine gen-server

-module(scriptlib_sm_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([
    start/1,
    start_link/1,
    stop/2,
    event/2,
    replace_script/2,
    change_owner/2,
    clear_owner/1
]).

-export([
    get_ownerpid/1,
    print/0
]).

-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

-define(TimerSec, {timersec}).
-define(KeyDelTempDir, <<"delete_tempdir">>).
-define(AsyncTerminateTimeout, 60000).

-define(StartComponentTypeDefault, 0).
-define(StartComponentTypePost, 1).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------
%% start (no link, stops by down message of owner and finish current work)
%% ---------------------------
-spec start(StartArgs::list() | map()) -> {error, Reason::term()} | {ok, Uuid::binary(), Pid::pid()}.
%% ---------------------------
start(StartArgs) when is_map(StartArgs) ->
    start(maps:to_list(StartArgs));
start(StartArgs) ->
    Uuid = ?BU:uuid_srvidx(),
    case gen_server:start(?MODULE, [{startts,os:timestamp()}, {id, Uuid} | lists:keydelete(id, 1, StartArgs)], [{hibernate_after,5000}]) of
        {ok, Pid} -> {ok, Uuid, Pid};
        Error -> Error
    end.

%% ---------------------------
%% start link (auto stop when owner down)
%% ---------------------------
-spec start_link(StartArgs::list() | map()) -> {error, Reason::term()} | {ok, Uuid::binary(), Pid::pid()}.
%% ---------------------------
start_link(StartArgs) when is_map(StartArgs) ->
    start_link(maps:to_list(StartArgs));
start_link(StartArgs) ->
    Uuid = ?BU:uuid_srvidx(),
    case gen_server:start_link(?MODULE, [{startts,os:timestamp()}, {id, Uuid} | lists:keydelete(id, 1, StartArgs)], [{hibernate_after,5000}]) of
        {ok, Pid} -> {ok, Uuid, Pid};
        Error -> Error
    end.

%% ---------------------------
%% external stop
%% ---------------------------
stop(Pid, Reason) when is_pid(Pid) ->
    %?OUT('$debug', "DEBUG. stop ~120p", [Reason]),
    gen_server:cast(Pid, {'stop',Reason}).

%% ---------------------------
%% external event
%% ---------------------------
event(Pid, Event) when is_pid(Pid) ->
    case erlang:is_process_alive(Pid) of
        false -> error;
        _ -> do_event(Pid, Event)
    end.

%% ---------------------------
%% Print all values
%% ---------------------------
print() ->
    gen_server:cast(?MODULE, {print}).

%% ---------------------------
%% Replace script at moment
%% ---------------------------
-spec replace_script(Pid:: pid(), NewScript:: {script, Script:: map()} | {code, Code:: binary()})
      -> ok | {error, Reason:: list()}.
%% ---------------------------
replace_script(Pid, {script, Script}=NewScript) when is_pid(Pid) and is_map(Script) ->
    gen_server:cast(Pid, {replace_script_id, NewScript});
replace_script(Pid, {code, Code}=NewScript) when is_pid(Pid) and is_binary(Code) ->
    gen_server:cast(Pid, {replace_script_id, NewScript});
replace_script(Pid, NewScript) ->
    {error, ?BU:str("~ts replace_script: wrong params ~n\t(Pid: ~500tp, Script: ~500tp)", [?APP, Pid, NewScript])}.

%%
change_owner(Pid, NewOwnerInfo) when is_pid(Pid) ->
    gen_server:cast(Pid, {'change_owner',NewOwnerInfo}).

%%
clear_owner(Pid) when is_pid(Pid) ->
    gen_server:cast(Pid, {'clear_owner'}).

%% ---------------------------
%% #306 return owner pid
%% ---------------------------
get_ownerpid(#state_scr{ownerpid=OwnerPid}) -> OwnerPid;
get_ownerpid(_) -> {error,invalid_state}.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    State = init_state(Opts),
    State1 = ?SCRU:update_dynamic_functions(State),
    contact_ownerpid(State1),
    register_cleanup(State1),
    log_sm(State1),
    start_sm(State1).

%% @private
init_state(Opts) when is_map(Opts) ->
    init_state(maps:to_list(Opts));
init_state(Opts) ->
    [Id,Type,Domain,Script,ScrCode,OwnerPid,LogLevel,TS] = ?BU:extract_required_props([id,type,domain,script,code,ownerpid,loglevel,startts], Opts),
    [PreVariables,UseOwnerDStrat,UseOwner,ResultToOwner] =
        ?BU:extract_optional_default([{variables,[]},
                                      {use_owner_down_strategy,false},
                                      {use_owner,false},
                                      {result_to_owner,false}], Opts),
    _OptsMinusScript = lists:keydelete(script,1,Opts),
    Self = self(),
    Ref = make_ref(),
    owner_down_strategy_processing(UseOwnerDStrat, {Self, Id, OwnerPid}), % RP-815
    #state_scr{type = Type,
               domain = Domain,
               scriptid = Id,
               script = Script,
               code = ScrCode,
               starttime = TS,
               objects = undefined,
               variables = init_variables(Script, PreVariables),
               meta = init_meta(Opts),
               limits = init_limits(Opts),
               selfpid = Self,
               loglevel = LogLevel,
               ownerpid = case UseOwner of true -> OwnerPid; false -> undefined end,
               monitor_ref = monitor_owner(UseOwner,OwnerPid),
               map = #{domain => Domain,
                       initial_var_values => ?BU:get_by_key(initial_var_values,Opts,[]),
                       cb_funs => init_callback_funs(Opts),
                       use_owner => UseOwner,
                       owner_pid => OwnerPid,
                       result_to_owner => ResultToOwner,
                       use_owner_down_strategy => UseOwnerDStrat},
               fun_events = ?BU:get_by_key(fun_events,Opts,undefined),
               ref = Ref,
               cref = make_ref(),
               timersec_funs_acc=[],
               timersec_ref=erlang:send_after(1000, Self, ?TimerSec)}.

%% @private #62
init_variables(_, []) -> [];
init_variables(Script, Variables) ->
    ScriptData = maps:get(scriptdata, Script, []),
    [ScriptVars] = ?BU:extract_optional_default([{<<"variables">>,[]}],ScriptData),
    F = fun(V, {In,Out}) ->
                Name = ?BU:get_by_key(<<"name">>,V),
                case lists:keytake(Name, #variable.name, In) of
                    {value, V1, L} -> {L,[V1|Out]};
                    _ -> {In,Out}
                end
        end,
    {_, Result} = lists:foldl(F, {Variables,[]}, ScriptVars),
    Result.

%% @private
init_meta(Opts) ->
    [Id,_Type,ScrCode,Script,OwnerPid] = ?BU:extract_required_props([id,type,code,script,ownerpid], Opts),
    Meta = ?BU:get_by_key(meta,Opts,#{}),
    ScriptRefFuns = [{{getscriptref,0},fun() -> Id end}],
    Scripts0 = maps:get(scripts,Meta,[]),
    Scripts = case maps:get(is_generated,Script,false) of
                  false -> Scripts0;
                  true -> fun(Code) when Code==ScrCode -> {ok,Script};
                             (Code) -> case Scripts0 of
                                           F when is_function(F,1) -> F(Code);
                                           L when is_list(L) -> lists:keyfind(Code,1,L)
                                       end end end,
    ScriptOpts = maps:get(script_opts,Meta,undefined),
    #meta{scripts = Scripts,
          script_opts = ScriptOpts,
          components = maps:get(components,Meta,[]),
          functions = lists:append(ScriptRefFuns, maps:get(functions,Meta,[])),
          scripts_prior = maps:get(scripts_prior,Meta,#{}),
          monitor = maps:get(monitor,Meta,#{}),
          fun_res = {OwnerPid,maps:get(fun_res,Meta,undefined)},
          callback_funs = maps:get(callback_funs,Meta,#{})}.

%% @private
init_limits(Opts) ->
    ScrLimits = ?BU:get_by_key('scrlimits',Opts,#{}),
    ?SCR_LIMIT:initialize_limits(ScrLimits).

%% @private
init_callback_funs(Opts) ->
    OwnerPid = ?BU:get_by_key('ownerpid', Opts),
    SendResultToOwner = ?BU:get_by_key('result_to_owner',Opts,false),
    do_init_callback_funs(SendResultToOwner,OwnerPid).
%% @private
do_init_callback_funs(SendResultToOwner,OwnerPid) ->
    case SendResultToOwner of
        true when is_pid(OwnerPid) -> #{on_mainbranch_stopped => [get_cb_fun_result_to_owner(mainbranch_stopped,OwnerPid)],
                                        on_machine_stopped => [get_cb_fun_result_to_owner(machine_stopped,OwnerPid)]};
        _ -> #{}
    end.

%% @private
get_cb_fun_result_to_owner(Type,OwnerPid) ->
    Self = self(),
    fun(Result) ->
        Msg = {scriptmachine, {Type, #{pid => Self, result => Result}}},
        OwnerPid ! Msg
    end.

%% @private
contact_ownerpid(State) ->
    #state_scr{scriptid=Id,ownerpid=OwnerPid,starttime=TS,map=Map}=State,
    case maps:get(result_to_owner,Map) of
        false -> ok;
        true ->
            Self = self(),
            % send self pid to owner
            Msg = {scriptmachine, {inited, #{id => Id,
                                             starttime => TS,
                                             pid => Self}}},
            case is_pid(OwnerPid) of
                true -> OwnerPid ! Msg;
                _ -> ok
            end end.

%% @private
register_cleanup(#state_scr{selfpid=SMPid}=_State) ->
    ?BLmonitor:append_fun(SMPid, ?KeyDelTempDir, fun() -> ?SCR_FILE:spawn_cleanup_temp_dir(SMPid) end).

%% @private
log_sm(State) ->
    #state_scr{scriptid=Id,code=ScrCode,ownerpid=OwnerPid,loglevel=LogLevel}=State,
    ?SCR_TRACE(State, "~ts ScriptSrv inited (code=~500tp, id=~500tp, owner=~500tp, loglevel=~500tp)", [?APP, ScrCode, Id, OwnerPid, LogLevel]).

%% @private
start_sm(State) ->
    #state_scr{ref=Ref}=State,
    Self = self(),
    case ?SCR_LIMIT:on_start(State) of
        {ok,State1} ->
            % async start
            gen_server:cast(Self, {start_sm, Ref}),
            {ok, State1};
        {error,_}=Err ->
            % RP-501 async start restrict
            gen_server:cast(Self, {start_restricted, Ref, Err}),
            {ok, State}
    end.

%% ------------------------------
%% Call
%% ------------------------------

%% -----
handle_call({}, _From, #state_scr{}=State) ->
    Reply = ok,
    {reply, Reply, State};

%% -----
handle_call({print}, _From, #state_scr{}=State) ->
    % ?OUT('$debug', "ScriptSrv state: ~120p", [State]),
    {reply, ok, State};

%% -----
handle_call({get_info}, From, #state_scr{}=State) ->
    handle_call({get_info,[type,domain,code,starttime]}, From, State);

handle_call({get_info, Fields}, _From, #state_scr{}=State) ->
    RecordFields = record_info(fields,state_scr),
    Reply = {ok,lists:foldl(fun(K,Acc) -> Acc#{K => ?BU:get_record_field(K,State,RecordFields)} end, #{}, Fields)},
    {reply, Reply, State};

%% other
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% start scriptmachine
handle_cast({'start_sm',Ref}, #state_scr{objects=undefined, ref=Ref}=State) ->
    ?SCRU:make_outer_event('script_start',[self()],State),
    ?SCRU:make_parent_event('sm_start', State),
    gen_server:cast(self(), {start, Ref}),
    {noreply, State};

%% start scriptmachine restricted
handle_cast({'start_restricted',Ref,{error,_}=Err}, #state_scr{objects=undefined, ref=Ref}=State) ->
    timer:sleep(100),
    res(Err,State#state_scr{post_branch=true});

%% start script
handle_cast({'start',Ref}, #state_scr{objects=undefined, ref=Ref, script=Script}=State) ->
    case ?SCRU:init_objects(Script) of
        {error,_}=Err -> finalize(Err, State#state_scr{stopreason=Err, post_branch=true});
        Objects ->
            State1 = State#state_scr{scriptstarttime = os:timestamp(),
                                     objects = Objects},
            State2 = ?SCRU:init_variables(State1),
            case ?SCR_COMP:find_component_start(State2) of
                false ->
                    Err = {error, {internal_error, <<"Script component start not found">>}},
                    finalize(Err, State2#state_scr{stopreason=Err});
                {ok, #component{module={error,_}=Err}} -> finalize(Err, State2#state_scr{stopreason=Err});
                {ok, #component{}=Comp} ->
                    State3 = State2#state_scr{active_component=Comp},
                    ?SCRU:make_parent_event('script_start', #{componentid => Comp#component.id,
                                                              componentname => Comp#component.name}, State3),
                    res(?SCR_COMP:component_init(State3), State3)
            end
    end;

%% events
%% unsubscribe monitoring owner
handle_cast({'event',{unmonitor, OwnerPid}}, #state_scr{ownerpid=OwnerPid}=State) ->
    {noreply, demonitor_owner(State)};

%% external stop
handle_cast({'stop',{stop_external,owner}}, #state_scr{post_branch=true}=State) ->
    ?SCR_TRACE(State, "Got owner's stop on post stage. Skipped", []),
    {noreply, demonitor_owner(State)};
handle_cast({'stop',Reason}, #state_scr{code=ScrCode,active_component=undefined}=State) ->
    %?OUT('$debug', "DEBUG. STOP EXTERNAL B"),
    Type = case Reason of
               owner_down -> owner_down;
               _ -> stop
           end,
    State1 = demonitor_owner(State),
    finalize(Type, State1#state_scr{stopreason={external,Reason,ScrCode,undefined}});
handle_cast({'stop',Reason}, #state_scr{code=ScrCode,active_component=#component{id=CompId}}=State) ->
    case res_terminate(State) of
        {ok, State1} ->
            Type = case Reason of
                       owner_down -> owner_down;
                       _ -> stop
                   end,
            finalize(Type, State1#state_scr{stopreason={external,Reason,ScrCode,CompId}});
        {async, State1} ->
            State2 = demonitor_owner(State1),
            {noreply, State2#state_scr{terminating={true,stop}}, ?AsyncTerminateTimeout}
    end;

%% replace script
handle_cast({replace_script_id, Script}, #state_scr{}=State) ->
    replace_script_private(Script, State);

%% change owner to scriptmachine
handle_cast({change_owner,NewOwnerInfo}, #state_scr{scriptid=Id,map=Map,ownerpid=OwnerPid}=State) ->
    OwnerPid = case NewOwnerInfo of
                   Pid when is_pid(Pid) -> Pid;
                   _ -> undefined
               end,
    % cb funs
    SendResultToOwner = maps:get(result_to_owner,Map),
    Map1 = Map#{cb_funs => do_init_callback_funs(SendResultToOwner,OwnerPid)},
    State1 = State#state_scr{map=Map1},
    % basic monitor
    State2 = demonitor_owner(State1),
    UseOwner = maps:get(use_owner,Map,false),
    State3 = case OwnerPid of
                 undefined -> State2;
                 _ -> State2#state_scr{ownerpid = OwnerPid,
                                       monitor_ref = monitor_owner(UseOwner, OwnerPid),
                                       map = Map1}
             end,
    % owner down strategy
    State4 = case maps:get(use_owner_down_strategy,Map,false) of
                 UseOwnerDStrat when is_pid(OwnerPid) ->
                     Self = self(),
                     PrevOwnerPid = maps:get(owner_pid,Map),
                     stop_owner_down_strategy_processing({Self,Id,PrevOwnerPid}),
                     owner_down_strategy_processing(UseOwnerDStrat,{Self,Id,OwnerPid}),
                     State3#state_scr{map=Map1#{owner_pid => OwnerPid}};
                 _ -> State3
             end,
    {noreply,State4};

%% clear owner from scriptmachine
handle_cast({clear_owner}, #state_scr{}=State) ->
    State1 = demonitor_owner(State),
    {noreply,State1};

%% cross messages
%% script interaction component
handle_cast({'event',{cross_message, _, _}=Ev}, #state_scr{active_component=#component{type=141}}=State) ->
    res(?SCR_COMP:component_handle_event(Ev,State), State);
handle_cast({'event',{cross_message, Sender, Message}}, #state_scr{cross_messages=Msgs}=State) ->
    {noreply, State#state_scr{cross_messages=lists:append([{Sender, Message}],Msgs)}};
handle_cast({'event',Ev}, #state_scr{active_component=undefined,queue=Q}=State) ->
    {noreply, State#state_scr{queue=[Ev|Q]}};
handle_cast({'event',Ev}, #state_scr{}=State) ->
    res(?SCR_COMP:component_handle_event(Ev,State), State);

%% handle next component or script change
handle_cast({after_component,Ref,CRef,Msg}, #state_scr{ref=Ref,cref=CRef}=State) ->
    after_component(Msg,State);

%% other
handle_cast(_Request, State) -> {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% events
handle_info({'event',_Ev}=E, State) ->
    handle_cast(E,State);

%% timersec functions
handle_info(?TimerSec, #state_scr{timersec_funs_acc=Fs}=State) ->
    Fs1 = lists:filtermap(fun({K,{F,Acc}}) ->
                                  case F(Acc) of
                                      undefined -> false;
                                      T -> {true,{K,{F,T}}}
                                  end;
                             (_) -> false
                          end, Fs),
    State1 = State#state_scr{timersec_funs_acc=Fs1,
                             timersec_ref=erlang:send_after(1000, self(), ?TimerSec)},
    {noreply, State1};

%% down of owner (proc is linked, and auto down instead of message)
handle_info({'DOWN', Ref, process, _Pid, _Result}, #state_scr{monitor_ref=Ref,code=ScrCode,active_component=undefined}=State) ->
    State1 = demonitor_owner(State),
    finalize(owner_down, State1#state_scr{stopreason={owner_down,ScrCode,undefined}});
handle_info({'DOWN', Ref, process, _Pid, _Result}, #state_scr{monitor_ref=Ref,code=ScrCode,active_component=#component{id=CompId}}=State) ->
    case res_terminate(State) of
        {ok, State1} ->
            State2 = demonitor_owner(State1),
            finalize(owner_down, State2#state_scr{stopreason={owner_down,ScrCode,CompId}});
        {async, State1} ->
            State2 = demonitor_owner(State1),
            {noreply, State2#state_scr{terminating={true,down}}, ?AsyncTerminateTimeout}
    end;

%% timeout
handle_info(timeout, #state_scr{code=ScrCode,active_component=#component{id=CompId}}=State) ->
    finalize(timeout, State#state_scr{stopreason={timeout,ScrCode,CompId}});
handle_info({async_terminating_timeout, Ref}, #state_scr{code=ScrCode,ref=Ref,active_component=#component{id=CompId}}=State) ->
    finalize(timeout, State#state_scr{stopreason={async_terminating_timeout,ScrCode,CompId}});

%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, #state_scr{terminating_timer=undefined}=_State) ->
    ok;
terminate(_Reason, #state_scr{terminating_timer=TimerRef}=_State) ->
    erlang:cancel_timer(TimerRef),
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------
do_event(_, ping) -> ok;
do_event(Pid, {stop, _}=Stop) -> gen_server:cast(Pid, Stop);
do_event(Pid, Event) -> ?SCR_COMP:event(Pid, Event).

%% ---------------------------
%% FSM processor after component result
%% ---------------------------
res(R,State) ->
    res_1(R,State).

%% @private
res_1({'error',_}=Err,State) -> finalize(Err, State#state_scr{stopreason=Err});
%%
res_1({'ok',State},_) -> {noreply, State};
%%
res_1(transfer_not_set=R,State) -> finalize(R, State#state_scr{stopreason=R});
%%
res_1(maxcount=R,State) -> finalize(R, State#state_scr{stopreason=R});
%%
res_1(maxduration=R,State) -> finalize(R, State#state_scr{stopreason=R});
%%
res_1({'stop',#state_scr{code=ScrCode,active_component=#component{id=CompId}}=State},_) ->
    {_, State1} = res_terminate(State#state_scr{stopreason=normal}),
    finalize(normal, State1#state_scr{stopreason={component,ScrCode,CompId}});
%%
res_1({'next',CompId,#state_scr{ref=Ref}=State},_) ->
    CRef = make_ref(),
    {_, State1} = res_terminate(State),
    gen_server:cast(self(), {after_component,Ref,CRef,{'next_component',CompId}}),
    {noreply, State1#state_scr{cref=CRef}};
%%
res_1({'script',ScrCode,#state_scr{ref=Ref}=State},_) ->
    CRef = make_ref(),
    {_, State1} = res_terminate(State),
    gen_server:cast(self(), {after_component,Ref,CRef,{'next_script',ScrCode}}),
    {noreply, State1#state_scr{cref=CRef}};
%%
res_1({'stack',#state_scr{ref=Ref}=State},_) ->
    CRef = make_ref(),
    {_, State1} = res_terminate(State),
    gen_server:cast(self(), {after_component,Ref,CRef,{'stack_script'}}),
    {noreply, State1#state_scr{cref=CRef}}.

%% ---------------------------
%% Terminates component
%% ---------------------------
-spec res_terminate(State:: #state_scr{}) -> {ok|async, State:: #state_scr{}}.
%% ---------------------------
res_terminate(State) ->
    case ?SCR_COMP:component_terminate(State) of
        {error,_} -> {ok,State};
        {ok,State1} -> {ok,State1#state_scr{active_component=undefined}};
        {async,State1} -> {async,State1}
    end.

%% ----------------------------------
%% RP-501 check limits and process component result in FSM
%% ----------------------------------
after_component(Msg, State) ->
    case ?SCR_LIMIT:after_component(State) of
        {stop,Reason,State1} -> res(Reason,State1);
        {ok,State1} -> after_component_end(Msg,State1)
    end.

%% @private
%% async stopping
after_component_end(_, #state_scr{terminating={true,Type}}=State) ->
    start_post_branch(Type, State);
%% next component
after_component_end({'next_component',CompId}, #state_scr{terminating=false}=State) ->
    case ?SCR_COMP:find_component_by_id(CompId,State) of
        false ->
            Err = {error, {internal_error, <<"Script component not found by id">>}},
            finalize(Err, State#state_scr{stopreason=Err});
        {ok, #component{module={error,_}=Err}} -> finalize(Err, State#state_scr{stopreason=Err});
        {ok, #component{}=Comp} -> maybe_run_component(Comp,State)
    end;
%% start embedded script
after_component_end({'next_script',ScrCode}, #state_scr{ref=Ref,loglevel=LogLevel,terminating=false}=State) ->
    %?LOG('$trace', "ScriptSrv sub (code=~120p)", [ScrCode]),
    ?SCR_TRACE(State, "ScriptSrv sub (code=~120p)", [ScrCode]),
    case ?SCRU:find_script(ScrCode, State) of
        {_,#{}=Script} ->
            % RP-1460
            LogLevel1 = case ?SCRU:find_script_opts(ScrCode, State) of
                            {ok,MapOpts} -> maps:get(<<"loglevel">>,MapOpts,LogLevel);
                            false -> LogLevel
                        end,
            State1 = State#state_scr{script=Script,
                                     code=ScrCode,
                                     loglevel=LogLevel1, % RP-1460
                                     objects=undefined},
            UpState = ?SCRU:update_dynamic_functions(State1),
            gen_server:cast(self(), {start, Ref}),
            {noreply, UpState};
        _ ->
            Err = {error, {not_found, <<"Script not found">>}},
            finalize(Err, State#state_scr{stopreason=Err})
    end;
%% return from embedded script
after_component_end({'stack_script'}, #state_scr{stack=[], code=ScrCode}=State) ->
    finalize(normal, State#state_scr{stopreason={stack,ScrCode,undefined}});
after_component_end({'stack_script'}, #state_scr{stack=[StackItem|Rest], limits=Limits}=State) ->
    [ScrCode,NextId,Time] = ?BU:maps_get([code,next,starttime], StackItem),
    %?LOG('$trace', "ScriptSrv stack (code=~120p)", [ScrCode]),
    ?SCR_TRACE(State, "ScriptSrv stack (code=~120p)", [ScrCode]),
    case ?SCRU:find_script(ScrCode, State) of
        {_,#{}=Script} ->
            case ?SCRU:init_objects(Script) of
                {error,_}=Err -> finalize(Err, State#state_scr{stopreason=Err});
                Objects ->
                    State1 = State#state_scr{script=Script,
                                             code=ScrCode,
                                             scriptstarttime=Time,
                                             objects=Objects,
                                             loglevel=maps:get(loglevel,StackItem), % RP-1460
                                             stack=Rest,
                                             limits=Limits#{countpassed:=maps:get(countpassed,StackItem)}}, % RP-501
                    State2 = ?SCRU:init_variables(State1),
                    UpState = ?SCRU:update_dynamic_functions(State2),
                    case ?SCR_COMP:find_component_by_id(NextId, UpState) of
                        false ->
                            Err = {error, {not_found, <<"Script not found">>}},
                            finalize(Err, UpState#state_scr{stopreason=Err});
                        {ok, #component{module={error,_}=Err}} -> finalize(Err, UpState#state_scr{stopreason=Err});
                        {ok, #component{}=Comp} ->
                            UpState1 = UpState#state_scr{active_component=Comp},
                            ?SCRU:make_parent_event('script_continue', #{componentid => Comp#component.id,
                                                                         componentname => Comp#component.name}, UpState1),
                            res(?SCR_COMP:component_init(UpState1), UpState1)
                    end
            end;
        _ ->
            Err = {error, {not_found, <<"Script not found">>}},
            finalize(Err, State#state_scr{stopreason=Err})
    end.

%% ----------------------------------------------------------------
%% @private
%% Check to run a component based on metadata
%% ----------------------------------------------------------------
-spec maybe_run_component(Comp::#component{},State::#state_scr{}) -> Result::term().
%% ----------------------------------------------------------------
maybe_run_component(#component{type=CT}=Comp,#state_scr{type=T}=State) ->
    case is_component_permitted(CT,T) of
        true ->
            State1 = State#state_scr{active_component=Comp},
            res(?SCR_COMP:component_init(State1), State1);
        false -> process_denied_component(Comp,State)
    end.

%% @private
is_component_permitted(CompId,_ScrType) when CompId > 50000 -> true; % Ghost components.
is_component_permitted(_CompId,_ScrType) -> true.

%% @private
process_denied_component(#component{type=Type,id=Id,module=M,name=Name},#state_scr{}=State) ->
    ?SCR_TRACE(State, "component T:~500tp, Id:~500tp, M:~500tp, N:'~ts' failure", [Type, Id, M, Name]),
    finalize({error,<<"not_allowed_by_metadata">>},State).

%% ------------------------------------------------------------
%% Terminates script-machine
%% ------------------------------------------------------------
-spec finalize(Reason:: normal | {error,_} | stop | owner_down | timeout | transfer_not_set | maxcount | maxduration, #state_scr{}) ->
                      {ok,#state_scr{}} | {stop,{shutdown,#state_scr{}}}.
%% ------------------------------------------------------------
finalize(Reason, #state_scr{monitor_ref=MonRef}=State) when MonRef/=undefined ->
    finalize(Reason, demonitor_owner(State));
%%
%%finalize(Reason, #state_scr{stopfuns=SF}=State) when SF/=undefined ->
%%    lists:foreach(fun(F) when is_function(F) -> try F() catch _:_ -> ok end end, lists:reverse(SF)),
%%    finalize(Reason, State#state_scr{stopfuns=undefined});
%%
finalize({error,_}, #state_scr{stopreason=Reason,post_branch=true}=State) ->
    State1 = cleanup(State),
    Result = [{stoptype, error},
              {stopreason, Reason}],
    exec_callback_funs('on_machine_stopped',Result,State1),
    ?SCR_ERROR(State1, "Stopping script. Reason:~500tp", [Reason]),
    {stop, {shutdown,Result}, State1};
%%
finalize(Type, #state_scr{stopreason=Reason,post_branch=true}=State) ->
    State1 = cleanup(State),
    Result = [{stoptype, Type},
              {stopreason, Reason},
              {variables, ?SCR_VAR:extract_local(State1)}],
    exec_callback_funs('on_machine_stopped',Result,State1),
    ?SCR_TRACE(State1, "Stopping script. Stop:~500tp, Reason:~500tp", [Type, Reason]),
    {stop, {shutdown,Result}, State1};
%%
finalize({Type,_}=T, #state_scr{stopreason=Reason}=State) ->
    ?SCR_TRACE(State, "Fin type:~500tp", [T]),
    State1 = demonitor_owner(State),
    NewRef = make_ref(),
    Result = [{stoptype, Type},
              {stopreason, Reason},
              {variables, ?SCR_VAR:extract_local(State)}],
    exec_callback_funs('on_mainbranch_stopped',Result,State),
    start_post_branch(Type, State1#state_scr{ref=NewRef});
%%
finalize(Type, #state_scr{stopreason=Reason}=State) ->
    ?SCR_TRACE(State, "Fin type:~500tp", [Type]),
    State1 = demonitor_owner(State),
    NewRef = make_ref(),
    Result = [{stoptype, Type},
              {stopreason, Reason},
              {variables, ?SCR_VAR:extract_local(State)}],
    exec_callback_funs('on_mainbranch_stopped',Result,State),
    start_post_branch(Type, State1#state_scr{ref=NewRef}).

%% @private
cleanup(#state_scr{selfpid=SMPid,timersec_ref=TimerRef,stopfuns=SF}=State) ->
    State1 = case SF of
                 undefined -> State;
                 _ when is_list(SF) ->
                     lists:foreach(fun(F) when is_function(F) -> try F() catch _:_ -> ok end end, lists:reverse(SF)),
                     State#state_scr{stopfuns=undefined}
             end,
    ?OUT('$trace', "cleanup",[]),
    erlang:cancel_timer(TimerRef),
    ?SCRU:make_outer_event('script_stop',[],State1),
    ?SCRU:make_parent_event('sm_stop', State1),
    % stop monitor
    ?BLmonitor:drop_fun(self(),?KeyDelTempDir),
    % delete files
    ?SCR_FILE:spawn_cleanup_temp_dir(SMPid),
    State1.

%% ------------------------------------------------------------
%% #61
%% finds start component of post-branch by selected StopReason and starts it
%% ------------------------------------------------------------
-spec start_post_branch(StopReason::atom(), State::#state_scr{}) -> {noreply, State1::#state_scr{}} | {stop, {shutdown,Result::list()}, State::#state_scr{}}.
%% ------------------------------------------------------------
start_post_branch(StopReason, #state_scr{}=State0) when is_atom(StopReason)->
    State = State0#state_scr{post_branch=true},
    case ?SCR_COMP:find_component_poststart(StopReason, State) of
        false -> finalize(StopReason, State);
        {ok, #component{module={error,_}=Err}} -> finalize(Err, State#state_scr{stopreason=Err});
        {ok, #component{}=Comp} ->
            State1 = State#state_scr{active_component=Comp, terminating=false},
            ?SCRU:make_parent_event('script_post_branch', #{componentid => Comp#component.id,
                                                            componentname => Comp#component.name}, State1),
            res(?SCR_COMP:component_init(State1), State1)
    end.

%% ------------------------------------------------------------
%% Replace script in meta silently (without refer+replaces).
%% ------------------------------------------------------------
-spec replace_script_private(NewScript:: {script, Script::map()} | {code, Code:: binary()}, State:: #state_scr{}) -> {noreply, UpdateState:: #state_scr{}}.
%% ------------------------------------------------------------
replace_script_private({script, Script}, #state_scr{meta=M,scriptid=_Id}=State) ->
    ?SCR_TRACE(State, "replace_script_private (id=~120p)", [_Id]),
    #meta{scripts=Scripts0}=M,
    UniqueCode = ?BU:to_binary(erlang:ref_to_list(make_ref())),
    Meta1 = M#meta{scripts=fun(Code) when Code==UniqueCode -> {ok,Script};
                              (Code) -> case Scripts0 of
                                            F when is_function(F,1) -> F(Code);
                                            L when is_list(L) -> lists:keyfind(Code,1,L)
                                        end end},
    UpdateState = State#state_scr{meta=Meta1},
    res({'script', UniqueCode, UpdateState}, UpdateState);
replace_script_private({code, Code}, #state_scr{}=State) ->
    res({'script',Code,State}, State).

%% ------------------------------------------------------------
%% Monitor owner
%% ------------------------------------------------------------
monitor_owner(false,_) -> undefined;
monitor_owner(_,OwnerPid) when is_pid(OwnerPid) ->
    case catch erlang:monitor(process,OwnerPid) of
        MonRef when is_reference(MonRef) -> MonRef;
        _ -> undefined
    end;
monitor_owner(_UseOwner,_OwnerInfo) -> undefined.

%% ---------------------------
%% Demonitor owner
%% ---------------------------
demonitor_owner(#state_scr{ownerpid=OwnerPid,monitor_ref=MonitorRef}=State) ->
    do_demonitor_owner({OwnerPid,MonitorRef},State).

%% @private
do_demonitor_owner({_,undefined},State) -> State;
do_demonitor_owner({OwnerPid,_},State) when not is_pid(OwnerPid) -> State;
do_demonitor_owner({_OwnerPid,MonitorRef},#state_scr{}=State) ->
    catch erlang:demonitor(MonitorRef),
    State#state_scr{ownerpid=undefined,
                    monitor_ref=undefined}.

%% ----------------------------------------------------------------
%% RP-815. Send event to launch component start post.
%% ----------------------------------------------------------------
owner_down_strategy_processing(false, _Params) -> ok;
owner_down_strategy_processing(_, {_, _, OwnerPid}) when not is_pid(OwnerPid) -> ok;
owner_down_strategy_processing(Strategy, {Self, Id, OwnerPid}) ->
    F = case Strategy of
            event -> fun() -> timer:sleep(1000), event(Self, {cross_message, {sender, {<<"script">>, Id}}, <<"owner_down">>}) end;
            poststart -> fun() -> timer:sleep(1000), event(Self, {'stop', 'owner_down'}) end
        end,
    ?BLmonitor:append_fun(OwnerPid, {use_owner_down_strategy,Self}, F).

%% ---------------------------
%% Stop owner down strategy
%% ---------------------------
stop_owner_down_strategy_processing({_Self, _, OwnerPid}) when not is_pid(OwnerPid) -> ok;
stop_owner_down_strategy_processing({Self, _Id, OwnerPid}) ->
    ?BLmonitor:drop_fun(OwnerPid, {use_owner_down_strategy,Self}).

%% ----------------------------------------------------------------
-spec exec_callback_funs(CallbackName::atom(),Data::term(),State::#state_scr{}) -> ok.
%% ----------------------------------------------------------------
exec_callback_funs(CallbackName,Data,#state_scr{map=M}) when CallbackName==on_machine_stopped; CallbackName==on_mainbranch_stopped ->
    case ?BU:parse_map([cb_funs,CallbackName],M) of
        not_found -> ok;
        L when is_list(L) -> lists:foreach(fun(F) -> F(Data) end,L);
        _ -> ok
    end.

%% -----------------------------------------------------------------
