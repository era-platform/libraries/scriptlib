%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc Script trace log writer

-module(scriptlib_sm_tracer).
-author(['George Makarov <georgemkrv@gmail.com>','Peter Bukashin <tbotc@yandex.ru>']).

-export([log_crash/3, log_crash/2,
         log_error/3, log_error/2,
         log_warning/3, log_warning/2,
         log_info/3, log_info/2,
         log_trace/3, log_trace/2,
         log_debug/3, log_debug/2,
         write/3, write/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------
log_crash(#state_scr{loglevel=Lvl}, _, _) when Lvl < 1 -> ok;
log_crash({Lvl, _, _, _, _}, _, _) when Lvl < 1 -> ok;
log_crash(StateOrTag, Fmt, Args) ->
    write_log(StateOrTag, '$crash', Fmt, Args).

log_crash(#state_scr{loglevel=Lvl}, _) when Lvl < 1 -> ok;
log_crash({Lvl, _, _, _, _}, _) when Lvl < 1 -> ok;
log_crash(StateOrTag, Text) ->
    write_log(StateOrTag, '$crash', Text).

%% ------------------
log_error(#state_scr{loglevel=Lvl}, _, _) when Lvl < 2 -> ok;
log_error({Lvl, _, _, _, _}, _, _) when Lvl < 2 -> ok;
log_error(StateOrTag, Fmt, Args) ->
    write_log(StateOrTag, '$error', Fmt, Args).

log_error(#state_scr{loglevel=Lvl}, _) when Lvl < 2 -> ok;
log_error({Lvl, _, _, _, _}, _) when Lvl < 2 -> ok;
log_error(StateOrTag, Text) ->
    write_log(StateOrTag, '$error', Text).

%% ------------------
log_warning(#state_scr{loglevel=Lvl}, _, _) when Lvl < 3 -> ok;
log_warning({Lvl, _, _, _, _}, _, _) when Lvl < 3 -> ok;
log_warning(StateOrTag, Fmt, Args) ->
    write_log(StateOrTag, '$warning', Fmt, Args).

log_warning(#state_scr{loglevel=Lvl}, _) when Lvl < 3 -> ok;
log_warning({Lvl, _, _, _, _}, _) when Lvl < 3 -> ok;
log_warning(StateOrTag, Text) ->
    write_log(StateOrTag, '$warning', Text).

%% ------------------
log_info(#state_scr{loglevel=Lvl}, _B, _C) when Lvl < 4 -> ok;
log_info({Lvl, _, _, _, _}, _, _) when Lvl < 4 -> ok;
log_info(StateOrTag, Fmt, Args) ->
    write_log(StateOrTag, '$trace', Fmt, Args).

log_info(#state_scr{loglevel=Lvl}, _) when Lvl < 4 -> ok;
log_info({Lvl, _, _, _, _}, _) when Lvl < 4 -> ok;
log_info(StateOrTag, Text) ->
    write_log(StateOrTag, '$trace', Text).

%% ------------------
log_trace(#state_scr{loglevel=Lvl}, _B, _C) when Lvl < 5 -> ok;
log_trace({Lvl, _, _, _, _}, _, _) when Lvl < 5 -> ok;
log_trace(StateOrTag, Fmt, Args) ->
    write_log(StateOrTag, '$trace', Fmt, Args).

log_trace(#state_scr{loglevel=Lvl}, _) when Lvl < 5 -> ok;
log_trace({Lvl, _, _, _, _}, _) when Lvl < 5 -> ok;
log_trace(StateOrTag, Text) ->
    write_log(StateOrTag, '$trace', Text).

%% ------------------
log_debug(#state_scr{loglevel=Lvl}, _B, _C) when Lvl < 6 -> ok;
log_debug({Lvl, _, _, _, _}, _, _) when Lvl < 6 -> ok;
log_debug(StateOrTag, Fmt, Args) ->
    write_log(StateOrTag, '$trace', Fmt, Args).

log_debug(#state_scr{loglevel=Lvl}, _) when Lvl < 6 -> ok;
log_debug({Lvl, _, _, _, _}, _) when Lvl < 6 -> ok;
log_debug(StateOrTag, Text) ->
    write_log(StateOrTag, '$trace', Text).

%% ------------------
write(StateOrTag, Fmt, Args) ->
    write_log(StateOrTag, '$trace', Fmt, Args).

write(StateOrTag, Text) ->
    write_log(StateOrTag, '$trace', Text).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------
-spec write_log(State :: state_scr() | tuple(), Type :: 'crash' | 'error' | 'warning' | 'info' | 'trace' | 'debug', Fmt :: string(), Args :: list()) -> ok.
%% ------------------
write_log(#state_scr{domain=Domain, code=Code, type=ScrType, scriptid=Id}=_State, Type, Fmt, Args) ->
    ?BLlog:write({script, make_name(ScrType, Domain)}, {make_tag(Type, Code, Id)++Fmt,Args});
write_log({_, Domain, Code, ScrType, Id}, Type, Fmt, Args) ->
    ?BLlog:write({script, make_name(ScrType, Domain)}, {make_tag(Type, Code, Id)++Fmt,Args}).

write_log(#state_scr{domain=Domain, code=Code, type=ScrType, scriptid=Id}=_State, Type, Text) ->
    ?BLlog:write({script, make_name(ScrType, Domain)}, make_tag(Type, Code, Id)++Text);
write_log({_, Domain, Code, ScrType, Id}, Type, Text) ->
    ?BLlog:write({script, make_name(ScrType, Domain)}, make_tag(Type, Code, Id)++Text).

%% @private
make_name(ScrType, Domain) ->
    ?BU:to_atom_new(?BU:to_list(ScrType)++"@"++?BU:to_list(Domain)).

%% @private
make_tag(Type, Code, Id) ->
    lists:flatten(io_lib:format("~ts:~ts:~ts: ", [?BU:to_list(Type), ?BU:to_list(Code), ?BU:to_list(Id)])).