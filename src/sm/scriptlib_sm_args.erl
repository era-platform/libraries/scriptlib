%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc Script arguments getter routines

-module(scriptlib_sm_args).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_value/3,
         get_value/4,

         get_int/4,
         get_float/4,
         get_string/3,
         get_string/4,
         get_string_with_encoding/4]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------------
%% get (from argument, default if something wrong)
%% ---------------------------------------------
-spec get_value(Arg::list()|term(), Default::term(), State::state_scr()) -> Value::argvalue() | term().
%% ---------------------------------------------
get_value([{_,_}|_]=Arg, Default, State)  ->
    case get(Arg, State) of
        undefined -> Default;
        {ok,undefined} -> Default;
        {ok,Value} -> Value
     end;
get_value(Arg,_,_) when is_binary(Arg) -> Arg;
get_value(Arg,_,_) when is_integer(Arg) -> Arg;
get_value(_, Default, _) -> Default.

%% ---------------------------------------------
%% get (from component, default if something wrong)
%% ---------------------------------------------
-spec get_value(PropertyKey::binary(), CompData::list(), Default::term(), State::state_scr()) -> Value::argvalue() | term().
%% ---------------------------------------------
get_value(PropertyKey, CompData, Default, State) when is_binary(PropertyKey), is_list(CompData) ->
    case lists:keyfind(PropertyKey, 1, CompData) of
        {_,Arg} -> get_value(Arg, Default, State);
        false -> Default
    end.

%% ---------------------------------------------
%% forcely convert to integer
%% ---------------------------------------------
-spec get_int(PropertyKey::binary(), CompData::list(), Default::term(), State::state_scr()) -> Value::integer() | term().
%% ---------------------------------------------
get_int(PropertyKey, CompData, Default, State) ->
    FInt = fun(S) -> try erlang:trunc(?BU:to_float(S)) catch error:_ -> Default end end,
    case get_value(PropertyKey, CompData, undefined, State) of
        N when is_integer(N) -> N;
        N when is_float(N) -> erlang:trunc(N);
        S when is_binary(S) -> FInt(S);
        {S,_Enc} when is_binary(S) -> FInt(S); % @encstring
        undefined -> Default
    end.

%% ---------------------------------------------
%% forcely convert to float
%% ---------------------------------------------
-spec get_float(PropertyKey::binary(), CompData::list(), Default::term(), State::state_scr()) -> Value::float() | term().
%% ---------------------------------------------
get_float(PropertyKey, CompData, Default, State) ->
    FFloat = fun(S) -> try ?BU:to_float(S) catch error:_ -> Default end end,
    case get_value(PropertyKey, CompData, undefined, State) of
        N when is_integer(N) -> N*1.0;
        N when is_float(N) -> N;
        S when is_binary(S) -> FFloat(S);
        {S,_Enc} when is_binary(S) -> FFloat(S); % @encstring
        undefined -> Default
    end.

%% ---------------------------------------------
%% forcely convert to string (utf-8 encoded as default)
%% ---------------------------------------------
-spec get_string(Arg::list()|term(), Default::term(), State::state_scr()) -> Value::binary() | term().
%% ---------------------------------------------
get_string(Arg, Default, State) ->
    case get_value(Arg, undefined, State) of
        N when is_number(N) -> ?BU:to_binary(N);
        S when is_binary(S) -> S;
        {S,_Enc}=Value when is_binary(S), is_binary(_Enc) -> {SUtf8,_}=?SCRU:to_utf8(Value), SUtf8; % @encstring
        undefined -> Default
    end.

%% ---------------------------------------------
%% forcely convert to string (utf-8 encoded as default)
%% ---------------------------------------------
-spec get_string(PropertyKey::binary(), CompData::list(), Default::term(), State::state_scr()) -> Value::binary() | term().
%% ---------------------------------------------
get_string(PropertyKey, CompData, Default, State) ->
    case get_value(PropertyKey, CompData, undefined, State) of
        N when is_number(N) -> ?BU:to_binary(N);
        S when is_binary(S) -> S;
        {S,_Enc}=Value when is_binary(S), is_binary(_Enc) -> {SUtf8,_}=?SCRU:to_utf8(Value), SUtf8; % @encstring
        undefined -> Default
    end.

%% ---------------------------------------------
%% forcely convert to string,
%% leave encoding info, use utf-8 as default encoding.
%% ---------------------------------------------
-spec get_string_with_encoding(PropertyKey::binary(), CompData::list(), Default::term(), State::state_scr()) -> Value::encstring() | term().
%% ---------------------------------------------
get_string_with_encoding(PropertyKey, CompData, Default, State) ->
    case get_value(PropertyKey, CompData, undefined, State) of
        N when is_number(N) -> ?SCRU:to_utf8(?BU:to_binary(N));
        S when is_binary(S) -> ?SCRU:to_utf8(S);
        {S,Enc} when is_binary(S) -> {S,Enc};
        undefined -> Default
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------------------------
%% get (from argument)
%% ---------------------------------------------
-spec get(Arg::list()|term(), State::state_scr()) -> undefined | not_implemented | {ok,Value::argvalue()}.
%% ---------------------------------------------
get(Arg, State) when is_list(Arg) ->
    case lists:keyfind(<<"argType">>,1,Arg) of
        {_,ArgType} ->
            case ?SCRU:map_argtype(ArgType) of
                'constant' ->
                    {_,V} = lists:keyfind(<<"value">>,1,Arg),
                    {_,T} = lists:keyfind(<<"varType">>,1,Arg),
                    ?SCRU:convert(V,T);
                'variable' ->
                    case lists:keyfind(<<"variable">>,1,Arg) of
                        false -> undefined;
                        {_,V} ->
                            lists:keyfind(<<"variable">>,1,Arg),
                            ?SCR_VAR:get(V, State) % @encstring
                    end;
                'expression' ->
                    case lists:keyfind(<<"expression">>,1,Arg) of
                        false -> undefined;
                        {_,E} -> get_expression(E,State)
                    end;
                'template' ->
                    {_,T} = lists:keyfind(<<"template">>,1,Arg),
                    case scriptlib_sm_metadata_api:template_to_expression(undefined,T,undefined) of
                        {ok,E} -> get_expression(E,State);
                        {error,R} ->
                            ?SCR_ERROR(State, "ERROR template (~120p) ~120p", [T,R]),
                            undefined
                    end end end;
get(_, _) -> undefined.

%% @private
get_expression(Expr,State) ->
    Meta = (State#state_scr.meta)#meta.functions,
    try {ok,?SCR_EXPR:apply(Expr,Meta,State)}
    catch
        error:R:ST ->
            ?SCR_ERROR(State, "get_expression error ~120tp~n\tExpression: ~120tp~n\tStack: ~120tp", [R,Expr,ST]),
            undefined;
          exit:R:ST ->
              ?SCR_ERROR(State, "get_expression exit ~120tp~n\tExpression: ~120tp~n\tStack: ~120tp", [R,Expr,ST]),
              undefined
    end.
