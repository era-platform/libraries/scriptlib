%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc

-module(scriptlib_sm_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    init_objects/1,
    init_variables/1
]).

-export([
    find_script/2,
    find_script_opts/2
]).

-export([
    make_outer_event/3,
    make_parent_event/2, make_parent_event/3
]).

-export([
    default_value/1,
    map_vartype/1,
    map_argtype/1,
    convert/2,
    try_convert_to_num/1,
    try_convert_to_datetime/1,
    to_utf8/1
]).

-export([parse_scriptmachine_result/1]).

-export([
    init_store/1,
    store_data/2,
    get_stored/2
]).

-export([update_dynamic_functions/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% API functions.
%% Script initialization
%% ====================================================================

%% -----------------------------------
%% build objects hashtable (by id)
%% -----------------------------------
init_objects(#{}=Script) ->
    ScriptData = maps:get(scriptdata, Script, []),
    case lists:keyfind(<<"objects">>,1,ScriptData) of
        false -> {error, {invalid, <<"Script error. Objects not found">>}};
        {_,O} ->
            lists:foldl(fun(CompData,Acc) ->
                                case ?BU:extract_optional_props([<<"oId">>,<<"oType">>], CompData) of
                                    [undefined,_] -> Acc;
                                    [_,undefined] -> Acc;
                                    [Id,_Type] ->
                                        %?OUT('$debug', "Id:~500tp, CompData:\n~500tp,\nAcc:\n~500tp",[Id,CompData,Acc]),
                                        maps:put(?BU:to_int(Id),CompData,Acc)
                                end end, maps:new(), O)
    end.

%% -----------------------------------
%% build/update variables list
%% -----------------------------------
init_variables(#state_scr{script=Script, variables=Vars, map=Map}=State) ->
    % expand list by script's new variables
    ScriptData = maps:get('scriptdata', Script, []),
    ScriptVars = ?BU:get_by_key(<<"variables">>,ScriptData,[]),
    Vars1 = lists:foldl(fun(V, Acc) ->
                                Var = #variable{id = ?BU:get_by_key(<<"id">>,V),
                                                name = ?BU:get_by_key(<<"name">>,V),
                                                type = map_vartype(?BU:get_by_key(<<"type">>,V)),
                                                location = case ?BU:get_by_key(<<"location">>,V) of
                                                               0 -> 'local';
                                                               _ ->
                                                                   % TODO: non local variable types
                                                                   'local'
                                                           end,
                                                value = undefined},
                                case lists:keytake(Var#variable.name, #variable.name, Acc) of
                                    {value, {_,V1}, L} -> lists:keystore(Var#variable.name, #variable.name, L, V1#variable{id=Var#variable.id});
                                    {value, V1, L} -> lists:keystore(Var#variable.name, #variable.name, L, V1#variable{id=Var#variable.id}); %@abramov 18.11.2016 (replace script)
                                    false -> [Var|Acc]
                                end end, Vars, ScriptVars),
    % update local variables values by initial params of script (remove applied from initial list to exclude from futher assign)
    {Vars2,InitVals2} = case maps:get(initial_var_values,Map,[]) of
                            [] -> {Vars1,[]};
                            InitVals ->
                                InitVals1 = case is_map(InitVals) of true -> maps:to_list(InitVals); false -> InitVals end,
                                lists:foldr(fun(#variable{location='local',name=VName}=V, {VAcc,IAcc}) ->
                                                    case lists:keytake(VName,1,IAcc) of
                                                        false -> {[V|VAcc],IAcc};
                                                        {value,{_,Value},IAcc1} -> {[V#variable{value = Value}|VAcc],IAcc1}
                                                    end;
                                               (V,{VAcc,IAcc}) -> {[V|VAcc],IAcc}
                                            end, {[],InitVals1}, Vars1)
                        end,
    % return new state
    UpState = State#state_scr{variables=Vars2,
                              map=Map#{initial_var_values => InitVals2}},
    init_variables_default(UpState).

%% @private
init_variables_default(#state_scr{variables=Vars, script=Script}=State) ->
    Opts = maps:get('opts', Script, #{}),
    DefVar = case maps:get(<<"variables">>, Opts, #{}) of X when is_map(X) -> X; _ -> #{} end,
    F = fun(#variable{value=undefined, name=Name, type=Type}=V) ->
                case maps:get(Name, DefVar, undefined) of
                    undefined -> V;
                    ValueBin -> {ok, Value}=convert(ValueBin,Type), V#variable{value=Value}
                end;
           (V) -> V
        end,
    UpVars = lists:map(F, Vars),
    State#state_scr{variables=UpVars}.

%% ====================================================================
%% API functions.
%% Script search
%% ====================================================================

%% -------------------------------------
-spec find_script(ScrCode::binary(), State::state_scr()) -> false | {error,Reason::term()} | {ScrCode::binary(), Script::map()}.
%% -------------------------------------
find_script(ScrCode, #state_scr{domain=Domain,meta=#meta{scripts=S}}=_State) ->
    Res = case S of
              _S when is_list(_S) -> case lists:keyfind(ScrCode,1,S) of {_,Scr} -> {ok,Scr}; false -> false end;
              _S when is_function(_S,1) -> case S(ScrCode) of R when is_map(R) -> {ok, R}; O -> O end;
              _ -> {error,{invalid,<<"Invalid metadata. Script not found">>}}
          end,
    case Res of
        {ok,_}=Ok -> Ok;
        _ ->
            case ?CFG:get_script(Domain,ScrCode) of
                undefined -> {error,{invalid,<<"Invalid metadata. Script not found">>}};
                {ok,_}=Ok -> Ok;
                false -> false
            end end.

%% -------------------------------------
-spec find_script_opts(ScrCode::binary(), State::state_scr()) -> false | {ok, ScriptOpts::map()}.
%% -------------------------------------
find_script_opts(ScrCode, #state_scr{meta=#meta{script_opts=S}}=_State) ->
    case S of
        _S when is_function(_S,1) -> S(ScrCode);
        _ -> false
    end.

%% ====================================================================
%% API functions.
%% Generate events
%% ====================================================================

%% -------------------------------------
%% Sends event out to monitor function.
%% (ex. to inform about start of next component, start script, stop script)
%% -------------------------------------
-spec make_outer_event(EventType::atom(), Args::list(), State::state_scr()) -> undefined | {ok,T::term()}.
%% -------------------------------------
make_outer_event(EventType, Args, #state_scr{meta=#meta{monitor=M}}=_State) ->
    Len = length(Args),
    case maps:get(EventType, M, undefined) of
        undefined -> undefined;
        Fun when is_function(Fun,Len) -> {ok,erlang:apply(Fun, Args)}
    end.

%% -------------------------------------
%% Sends event out to monitor function.
%% (ex. to inform about start of next component, start script, stop script)
%% -------------------------------------
-spec make_parent_event(EventType::atom(), State::state_scr()) -> ok.
%% -------------------------------------
make_parent_event(_, #state_scr{fun_events=undefined}) -> ok;
make_parent_event(sm_start, #state_scr{fun_events=FE}) -> FE({sm_start, #{}});
make_parent_event(sm_stop, #state_scr{fun_events=FE}) -> FE({sm_stop, #{}}).

%% -------------------------------------
-spec make_parent_event(EventType::atom(), MapOpts::map(), State::state_scr()) -> ok.
%% -------------------------------------
make_parent_event(_, _, #state_scr{fun_events=undefined}) -> ok;
make_parent_event(T, Opts, #state_scr{script=Script, fun_events=FE})
  when T==script_start; T==script_continue; T==script_post_branch ->
    FE({T, Opts#{code => maps:get(code,Script,<<"">>),
                 name => maps:get(name,Script,<<"">>)}});
make_parent_event(T, Opts, #state_scr{fun_events=FE}) -> FE({T, Opts}).

%% ====================================================================
%% API functions.
%% Mapping routines
%% ====================================================================

%% ----------------
default_value('numeric') -> 0;
default_value('string') -> <<>>;
default_value('datetime') -> <<"2000-01-01 00:00:00">>;
default_value(_) -> <<"undefined">>.

%% ----------------
map_vartype(0) -> 'any';
map_vartype(1) -> 'numeric';
map_vartype(2) -> 'string';
map_vartype(4) -> 'datetime'.

%% ----------------
map_argtype(1) -> 'constant';
map_argtype(2) -> 'variable';
map_argtype(3) -> 'expression'; % temporary before scripteditor
map_argtype(4) -> 'function'; % no metadata, no usage
map_argtype(8) -> 'expression';
map_argtype(32) -> 'template'.

%% ----------------
convert(Value, Type) when is_integer(Type) -> convert(Value, map_vartype(Type));
convert(Value, 'any') -> {ok, Value};
convert(Value, 'string') -> {ok, ?BU:to_binary(Value)};
convert(Value, 'numeric') -> {ok, try_convert_to_num(Value)};
convert(Value, 'datetime') -> {ok, try_convert_to_datetime(Value)}.

%% convert to number
try_convert_to_num(Arg) ->
    try ?BU:to_int(Arg)
    catch error:_ ->
            try ?BU:to_float(Arg)
            catch error:_ -> undefined
            end
    end.

%% convert to datetime
try_convert_to_datetime(Arg) ->
    try ?SCR_EXPRU:parse_datetime(Arg) of
        {{0,0,0},_,_}=Dt -> ?SCR_EXPRU:to_time(Dt);
        Dt -> ?SCR_EXPRU:to_datetime(Dt)
    catch error:_ -> undefined
    end.

%% ----------------
%% @encstring
%% empty
to_utf8(<<>>) -> {<<>>,<<"utf-8">>};
to_utf8({<<>>,_}) -> {<<>>,<<"utf-8">>};
%% no change
to_utf8({S,<<"utf-8">>}) -> {S,<<"utf-8">>};
%% representation
%% to_utf8({S,<<"binary">>}) -> {S,<<"utf-8">>};
to_utf8(S) when is_binary(S) -> {S,<<"utf-8">>};
%% convert
to_utf8({S,Enc}) when is_binary(S), is_binary(Enc) ->
    case ?BU:iconvert(Enc, <<"utf-8">>, S) of
        {ok, S1} -> {S1, <<"utf-8">>};
        {error, _ReasonAtom} -> {S, Enc} % @@ XXX conversion unsuccessful
    end.

%% ====================================================================
%% API functions.
%% Other routines
%% ====================================================================

%% ------------------------------------
%% parse script machine result
%% ------------------------------------
-spec parse_scriptmachine_result(Result::list()|tuple()) -> {ok,Map::map()} | {error,Reason::term()}.
%% ------------------------------------
parse_scriptmachine_result({_,Result}) -> parse_scriptmachine_result(Result);
parse_scriptmachine_result(Result) when is_list(Result) ->
    [StopType,StopReason,Vars] = ?BU:extract_optional_default([{stoptype,error},{stopreason,<<"undefined">>},{variables,undefined}],Result),
    M = #{stoptype => StopType,
          stopreason => StopReason},
    case StopType of
        normal ->
            Variables = parse_vars_results(Vars),
            {ok,M#{variables=>Variables}};
        _ -> {ok,M}
    end;
parse_scriptmachine_result(_) ->
    {error,<<"invalid_result">>}.

%% @private
parse_vars_results(Vars) ->
    F = fun({_Id,Name,{Val,_Encod}}) ->
                case ?SCRU:to_utf8(Val) of
                    {NVal,<<"utf-8">>} -> {true,#{name=>Name,value=>NVal}};
                    {NVal,Enc} -> {true,#{name=>Name,value=>NVal,encoding=>Enc}}
                end;
           ({_Id,Name,undefined}) -> {true,#{name=>Name,value=>null}};
           ({_Id,Name,V}) -> {true,#{name=>Name,value=>V}};
           (_) -> false
        end,
    lists:filtermap(F,Vars).

%% ==========================================
%% Store of arbitrary data
%% ==========================================

init_store(#state_scr{store=undefined}=State) -> State#state_scr{store=maps:new()};
init_store(State) -> State.

store_data({_,undefined}, #state_scr{store=undefined}=State) -> State;
store_data({Key,undefined}, #state_scr{store=Store}=State) ->
    State#state_scr{store=maps:remove(Key, Store)};
store_data({Key,Value}, #state_scr{}=State) ->
    #state_scr{store=Store}=State1 = init_store(State),
    State1#state_scr{store=maps:put(Key,Value,Store)}.

get_stored(_, #state_scr{store=undefined}=_State) -> false;
get_stored(Key, #state_scr{store=Store}=_State) ->
    case maps:find(Key, Store) of
        error -> false;
        {_,Value} -> {Key,Value}
    end.

%% ==========================================
%% Setup/update dynamic state-dependant functions
%%  (get data from actual state)
%% ==========================================
update_dynamic_functions(State) ->
    State.