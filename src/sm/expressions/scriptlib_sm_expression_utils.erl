%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 08.03.2021
%%% @doc

-module(scriptlib_sm_expression_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

-define(E, erlang).
-define(BS, bitstring).

%% ====================================================================
%% API functions
%% ====================================================================

% string in list
% convert to string or die
s(Arg) when is_list(Arg) ->
    case ?BU:to_unicode_list(Arg) of
        L when is_list(L) -> L; % RP-608
        _ -> Arg % #392
    end;
s(Arg) when is_binary(Arg) ->
    case ?BU:to_unicode_list(Arg) of
        L when is_list(L) -> L;
        _ -> ?BU:to_list(Arg) % #392
    end;
s(Arg) when is_atom(Arg) -> ?BU:to_list(Arg);
s(Arg) when is_number(Arg) -> ?BU:to_list(?BU:to_binary(Arg));
s(_) -> exit(<<"invalid argument type">>).

% string in binary
b(Arg) when is_binary(Arg) -> Arg;
b(Arg) when is_list(Arg) -> ?BU:to_binary(Arg);
b(Arg) when is_atom(Arg) -> ?BU:to_binary(Arg);
b(Arg) when is_number(Arg) -> ?BU:to_binary(Arg);
b(_Arg) -> ?OUT('$warning', "Arg: ~120p", [_Arg]), exit(<<"invalid argument type">>).

% number (integer or float)
% convert to number or die
n([]) -> 0; % #394
n(<<>>) -> 0; % #394
n(Arg) ->
    case try_convert_to_num(Arg) of
        undefined -> exit(<<"invalid argument type">>);
        Num -> Num
    end.

% integer
i(Arg) -> ?E:trunc(n(Arg)).

%% ----------------------------------------------------

floor(X) when X>=0 -> ?E:trunc(X);
floor(X) when X<0 -> case ?E:trunc(X) of X -> X; Y -> Y-1 end.

ceil(X) when X>=0 -> case ?E:trunc(X) of X -> X; Y -> Y+1 end;
ceil(X) when X<0 -> ?E:trunc(X).

%% ----------------------------------------------------

%%% Parse datetime in simple mode, timezone loss

parse_datetime(Arg) ->
    case ?BU:parse_datetime(Arg) of
        {{1970,1,1}, {0,0,0}, 0} when is_integer(Arg) -> parse_datetime(?SCRU:default_value('datetime'));
        {{1970,1,1}, {0,0,0}, 0} -> try parse_datetime(?BU:to_int(Arg)) catch _:_ -> parse_datetime(?SCRU:default_value('datetime')) end;
        Res -> Res
    end.

%% Parse datetime in timezone mode (convert and return utc time)
parse_datetime_utc(V) -> ?BU:parse_datetime_utc(V).

%% Build datetime binary string

% build date string (as it is without timezone)
to_date({{Y,M,D},{_H,_Mi,_S},_Ms}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0B", [Y,M,D]).

% build time string (as it is without timezone)
to_time({{_Y,_M,_D},{H,Mi,S},_Ms}) ->
    ?BU:strbin("~2..0B:~2..0B:~2..0B", [H,Mi,S]).

% build datetime string (as it is without timezone)
to_datetime({{Y,M,D},{H,Mi,S},Ms}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B.~3..0B", [Y,M,D,H,Mi,S,Ms]).

% build datetime string (as it is in local timezone)
to_datetime_local({{Y,M,D},{H,Mi,S},Ms}) ->
    TS = os:timestamp(),
    {XD,{XH,ZM,_}} = calendar:time_difference(calendar:now_to_universal_time(TS), calendar:now_to_local_time(TS)),
    Z = case XD>=0 of true -> "+"; false -> "-" end,
    ZH = XH - XD*24,
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B.~2..0B~s~2..0B:~2..0B", [Y,M,D,H,Mi,S,Ms div 10,Z,ZH,ZM]).

% build datetime string (as it is in utc timezone)
to_datetime_utc({{Y,M,D},{H,Mi,S},Ms}) ->
    ?BU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B.~2..0B+00:00", [Y,M,D,H,Mi,S,Ms div 10]).

% build datetime string (as it is by fmt, 'zzz' means local timezone)
to_datetime(Fmt, {{Y,M,D},{H,Mi,S},Ms}) ->
    % @todo
    L = [{"yyyy", fun() -> "~4..0B" end, fun() -> [Y rem 10000] end},
         {"yy", fun() -> "~2..0B" end, fun() -> [Y rem 100] end},
         {"MM", fun() -> "~2..0B" end, fun() -> [M] end},
         {"dd", fun() -> "~2..0B" end, fun() -> [D] end},
         {"d", fun() -> "~p" end, fun() -> [D] end},
         {"h", fun() -> "~p" end, fun() -> [H] end},
         {"HH", fun() -> "~2..0B" end, fun() -> [H] end},
         {"MMM", fun() -> "~2..0B" end, fun() -> [Mi] end},
         {"mm", fun() -> "~2..0B" end, fun() -> [Mi] end},
         {"ss", fun() -> "~2..0B" end, fun() -> [S] end},
         {"ffff", fun() -> "~3..0B" end, fun() -> [Ms*10] end},
         {"fff", fun() -> "~3..0B" end, fun() -> [Ms] end},
         {"ff", fun() -> "~2..0B" end, fun() -> [Ms div 10] end},
         {"f", fun() -> "~1..0B" end, fun() -> [Ms div 100] end},
         {"zzz", fun() -> "~s~2..0B:~2..0B" end,
                  fun() -> TS = os:timestamp(),
                         {XD,{XH,ZM,_}} = calendar:time_difference(calendar:now_to_universal_time(TS), calendar:now_to_local_time(TS)),
                         Z = case XD>=0 of true -> "+"; false -> "-" end,
                         ZH = XH - XD*24,
                         [Z,ZH,ZM] end}],
    b(lists:foldl(fun({K,F1,F2}, Acc) -> re:replace(Acc,K,?BU:str(F1(),F2()),[{return,list},global]) end, s(Fmt), L)).

%% state to log
shrink_state(#state_scr{domain=Domain,loglevel=SLL,type=SType,code=SCode,scriptid=SID,selfpid=SMPid}=_State) ->
    #state_scr{domain=Domain,loglevel=SLL,type=SType,code=SCode,scriptid=SID,selfpid=SMPid}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private
try_convert_to_num(Arg) when is_number(Arg) -> Arg;
try_convert_to_num(Arg) ->
    try ?BU:to_int(Arg)
    catch error:_ ->
              try ?BU:to_float(Arg)
              catch error:_ -> undefined
              end
    end.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% init function, load environment
start_test_() ->
    include_environment(),
    [].

%% @private attach env project
include_environment() ->
    {ok,CWD} = file:get_cwd(),
    RPath = filename:dirname(filename:dirname(CWD)),
    Path = filename:join([RPath, "rostell_env", "ebin"]),
    code:add_patha(Path).

%% -------------
s_test_() ->
    {"s test",
     [?_assertEqual("s",s("s")),
      ?_assertEqual("asdf",s("asdf")),
      ?_assertEqual("фыва",s("фыва")),
      ?_assertEqual("ПРИВ прив",s([208,159,208,160,208,152,208,146,32,208,191,209,128,208,184,208,178])),
      ?_assertEqual("фыва",s(<<"фыва"/utf8>>)),
      ?_assertEqual("null",s(null)),
      ?_assertEqual("5",s(5)),
      ?_assertEqual("5.3",s(5.3)),
      ?_assertEqual({error,[0,1055],<<159,208,2>>}, ?BU:to_unicode_list([0,208,159,159,208,2])),
      ?_assertEqual([0,208,159,159,208,2], s([0,208,159,159,208,2])),
      ?_assertEqual([0,208,159,159,208,2], s(<<0,208,159,159,208,2>>))]}.

%% -------------
b_test_() ->
    {"b test",
     [?_assertEqual(<<"asdf">>,b(<<"asdf">>)),
      ?_assertEqual(<<"фыва"/utf8>>,b(<<"фыва"/utf8>>)),
      ?_assertEqual(<<"s">>,b("s")),
      ?_assertEqual(<<"фыва"/utf8>>,b("фыва")),
      ?_assertEqual(<<0,208,159,159,208,2>>,b([0,208,159,159,208,2])),
      ?_assertEqual(<<"asdf">>,b(asdf)),
      ?_assertEqual(<<"25">>,b(25)),
      ?_assertEqual(<<"25.3">>,b(25.3))]}.

%% -------------
n_test_() ->
    {"n test",
     [?_assertEqual(10,n(10)),
      ?_assertEqual(10,n("10")),
      ?_assertEqual(10,n(<<"10">>)),
      ?_assertEqual(10.2,n(10.2)),
      ?_assertEqual(10.2,n("10.2")),
      ?_assertEqual(10.2,n(<<"10.2">>)),
      ?_assertExit(<<"invalid argument type">>,n("asdf"))]}.

%% -------------
i_test_() ->
    {"i test",
     [?_assertEqual(10,i(10)),
      ?_assertEqual(10,i(10.2)),
      ?_assertEqual(10,i("10.2")),
      ?_assertEqual(-2,i(<<"-2.55">>)),
      ?_assertExit(<<"invalid argument type">>,i("asdf"))]}.

%% -------------
try_convert_to_num_test_() ->
    F = fun(X) -> try_convert_to_num(X) end,
    {"try_convert_to_num",
     [?_assertEqual(3, F(3)),
      ?_assertEqual(-1, F(-1)),
      ?_assertEqual(0, F(0)),
      ?_assertEqual(3.2, F(3.2)),
      ?_assertEqual(-1.5, F(-1.5)),
      ?_assertEqual(5, F(<<"5">>)),
      ?_assertEqual(5.8, F(<<"5.8">>)),
      ?_assertEqual(7.2, F("7.2")),
      ?_assertEqual(undefined, F(<<"5 2 3">>)),
      ?_assertEqual(undefined, F(<<"asdf">>))]}.

-endif.
