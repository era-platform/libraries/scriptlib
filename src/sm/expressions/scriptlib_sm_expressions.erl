%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc Expressions for scripts.
%%%        Variables could be used as
%%%                [Name],
%%%                get_variable_id_by_name(Name) -> Id::binary() or exit,
%%%                get_variable_id(Name|Id) -> Id::binary()
%%%                get_variable_value_by_id(Id::binary()) -> term() or exit
%%%                get_variable_value(Name|Id) -> term() or exit
%%%                get_variable_orig(Name|Id) -> term() or exit
%%%                check_variable(Name|Id) -> true|false|undefined
%%%                get_variable_value(Name|Id,Default) -> term()
%%%                set_variable_value_if_undefined(Name|Id,Value) -> term()

-module(scriptlib_sm_expressions).
-author('Peter Bukashin <tbotc@yandex.ru>').

%-compile(export_all).
-export([apply/3]).
-export([get_script_state/0,
         apply_script_state/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

-define(STOREKEY(Pid), {script_expression,Pid}).

-define(EOUT(Fmt,Args), ok).
%%-define(EOUT(Fmt,Args), ?OUT('$debug',Fmt,Args)).

%% ====================================================================
%% API functions
%% ====================================================================

apply(Expression, Meta, State) when is_list(Expression) ->
    ?MODULE:apply(?BU:to_binary(Expression), Meta, State);
apply(Expression, Meta, #state_scr{variables=Vars}=State) when is_binary(Expression) ->
    ?EOUT("Exp = ~120tp", [Expression]),
    % updated expression
    E1 = lists:foldl(fun(#variable{name=VarName,id=VarId},Acc) ->
                            VarX = <<"[",VarName/bitstring,"]">>,
                            VarFun = <<"get_variable_value_by_id(<<\"",VarId/bitstring,"\">>)">>,
                            binary:replace(Acc, VarX, VarFun, [global])
                     end, Expression, Vars),
    case string:trim(E1) of <<>> -> exit(<<"empty expression">>); _ -> ok end,
    % update meta
    Meta1 = prepare_var_getters(State) ++ Meta,
    Meta2 = prepare_state_modifiers(State) ++ Meta1,
    apply_wait_result(E1, Meta2, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% called from async expression calculator process, returns state of scriptmachine
%% used by functions
-spec get_script_state() -> {ok,State::#state_scr{}} | {error,R::atom()}.
get_script_state() ->
    Self = self(),
    case ?BLstore:find_t(?STOREKEY(Self)) of
        false -> {error,not_found};
        {_,SPid} ->
            RefX = make_ref(),
            SPid ! {get_state,Self,RefX},
            receive {state,RefX,State} -> {ok,State}
            after 1000 -> {error,timeout}
            end end.

%% called from async expression calculator process, returns fun over state result
%% used by functions
apply_script_state(F) when is_function(F,1) ->
    Self = self(),
    case ?BLstore:find_t(?STOREKEY(Self)) of
        false -> {error,not_found};
        {_,SPid} ->
            RefX = make_ref(),
            SPid ! {apply_state,Self,RefX,F},
            receive {res,RefX,Res} -> {ok,Res}
            after 1000 -> {error,timeout}
            end end.

%% ====================================================================
%% Private functions
%% ====================================================================

%% @private
apply_wait_result(E1, Meta, State) ->
    {Self,Ref} = {self(),make_ref()},
    StateTag = ?SCR_EXPRU:shrink_state(State),
    {Pid,ExpMRef} = spawn_monitor(fun() -> receive {store_done,Ref} -> ok after 5000 -> ok end,
                                           try Res = apply_internal(E1, Meta),
                                               Self ! {ok,Ref,Res}
                                           catch
                                               throw:abort -> ok;
                                               E:R:StackTrace -> ?SCR_CRASH(StateTag, " Script process crashed: ~n\t~200tp", [{E,R,StackTrace}])
                                           end end),
    % #306
    ?BLstore:store_t(?STOREKEY(Pid),Self,600000),
    Pid ! {store_done,Ref},
    R = wait_acc_result({Ref,Pid,ExpMRef},State),
    ?BLstore:delete_t(?STOREKEY(Pid)),
    R.

% @private
wait_acc_result({Ref,Pid,ExpMRef}=P,#state_scr{monitor_ref=ScrMRef}=State) ->
    Fabort = fun(E) -> Pid ! abort, self() ! E, <<>> end,
    receive
        {'DOWN',ExpMRef,process,Pid,Reason} -> throw(Reason);
        {ok,Ref,Result} -> Result;
        {get_state,Pid,RefX} ->
            Pid ! {state,RefX,State},
            wait_acc_result(P,State);
        {apply_state,Pid,RefX,F} when is_function(F,1) ->
            Pid ! {res,RefX,F(State)},
            wait_acc_result(P,State);
%%         % @todo get expression values do not change state in components
%%         {set_state,Pid,#state_scr{}=StateX} ->
%%             wait_acc_result(P,StateX)
        % ---------
        {'DOWN',ScrMRef,process,_,_}=E -> Fabort(E);
        {'$gen_cast',{stop,_}}=E -> Fabort(E)
    end.

% -----------------
% @private
apply_internal(Expression, Meta) when is_binary(Expression) ->
    apply_internal(?BU:to_list(Expression), Meta);
apply_internal(Expression, Meta) when is_list(Expression) ->
    ?EOUT("Meta = ~120tp", [Meta]),
    Flocal = make_local_fun(Meta),
    Fnonlocal = make_nonlocal_fun(Meta),
    case apply_internal(Expression, {Flocal,Fnonlocal}, erl_eval:new_bindings()) of
        {value,Value,_Bindings1} when is_binary(Value); is_atom(Value); is_list(Value) -> ?SCRU:to_utf8(?BU:to_binary(Value)); % @encstring
        {value,{Value},_Bindings1} when is_binary(Value) -> ?SCRU:to_utf8(?BU:to_binary(Value)); % @encstring
        {value,Value,_Bindings1} -> Value;
        _ -> exit(<<"expression error">>)
    end.

% @private
apply_internal(E1, {Flocal,Fnonlocal}, Bindings) when is_list(E1) ->
    ?EOUT("E1 = ~120tp, ~n\t~ts", [E1,E1]),
    E2 = lists:reverse(lists:dropwhile(fun(" ") -> true; ($\t) -> true; ($.) -> true; (_) -> false end, lists:reverse(E1))) ++ ".",
    ?EOUT("E2 = ~120tp, ~n\t~ts", [E2,E2]),
    {ok,S1,_} = erl_scan:string(E2),
    ?EOUT("S1 = ~120tp", [S1]),
    {ok,S2} = erl_parse:parse_exprs(S1),
    ?EOUT("S2 = ~120tp", [S2]),
    %{value,_Value,_Bindings1} = erl_eval:exprs(S2,Bindings,{eval,Flocal},{value,Fnonlocal}).
    catches(S2,{Flocal,Fnonlocal},Bindings).

%% @private
%% catch build-in function
-define(CatchFun1, 'catch').
-define(CatchFun2, 'except').
catches(S2,{Flocal,Fnonlocal},Bindings) ->
    % return true if expression has built-in 'catch' function inside
    Fc = fun Fc({'call',_N1,{atom,_N2,FN},[_Arg,_Def]}) when _N1==_N2 andalso (FN==?CatchFun1 orelse FN==?CatchFun2) -> true;
             Fc({'call',_N,_Fun,Args}) -> lists:any(fun(Arg) -> Fc(Arg) end, Args);
             Fc({'block',_N,Args}) -> lists:any(fun(Arg) -> Fc(Arg) end, Args);
             Fc({'catch',_N,Arg}) -> Fc(Arg);
             Fc({'try',_N,Args,_,Clauses,_}) -> lists:any(fun(Arg) -> Fc(Arg) end, Args ++ Clauses);
             Fc({'clause',_N,_,_,Args}) -> lists:any(fun(Arg) -> Fc(Arg) end, Args);
             Fc({'cons',_N,Arg,Rest}) -> Fc(Arg) orelse Fc(Rest);
             Fc({'op',_N,_Op,Arg1,Arg2}) -> Fc(Arg1) orelse Fc(Arg2);
             Fc({'op',_N,_Op,Arg}) -> Fc(Arg);
             Fc({'bin_element',_N,Arg,_,_}) -> Fc(Arg);
             Fc(_) -> false
        end,
    % return result value argument by sub-expression result
    Fv = fun({value,V,_}) ->
                 {ok,E1,_} = erl_scan:string(?BU:str("~500tp.",[V])),
                 {ok,[E2]} = erl_parse:parse_exprs(E1),
                 E2
         end,
    % compute sub-expression
    Fe = fun(Expr,BindingsE) ->
                 erl_eval:exprs([Expr],BindingsE,{eval,Flocal},{value,Fnonlocal})
         end,
    % return new expression tree (where catch built-in function mapped into result)
    F = fun F({'call',_N1,{atom,_N2,FN},[Arg,Def]},X) when _N1==_N2 andalso (FN==?CatchFun1 orelse FN==?CatchFun2) ->
                 Fx = fun(Arg0) ->
                              case Fc(Arg0) of
                                  true -> F(Arg0,X);
                                  false -> Arg0
                              end end,
                 ArgX = Fx(Arg),
                 DefX = Fx(Def),
                 case catch Fv(Fe(ArgX,X)) of
                     {'EXIT',_} -> Fv(Fe(DefX,X));
                     T -> T
                 end;
            F({'call',_N,Fun,Args},X) -> {'call',_N,Fun,lists:map(fun(Arg) -> F(Arg,X) end, Args)};
            F({'block',_N,Args},X) -> {'block',_N,lists:map(fun(Arg) -> F(Arg,X) end, Args)};
            F({'catch',_N,Arg},X) -> {'catch',_N,F(Arg,X)};
            F({'try',_N,Args,P1,Clauses,P2},X) -> {'try',_N,lists:map(fun(Arg) -> F(Arg,X) end, Args),P1,lists:map(fun(Arg) -> F(Arg,X) end, Clauses),P2};
            F({'clause',_N,P1,P2,Args},X) -> {'clause',_N,P1,P2,lists:map(fun(Arg) -> F(Arg,X) end, Args)};
            F({'cons',_N,Arg,Rest},X) -> {'cons',_N,F(Arg,X),F(Rest,X)};
            F({'op',_N,Op,Arg1,Arg2},X) -> {op,_N,Op,F(Arg1,X),F(Arg2,X)};
            F({'op',_N,Op,Arg},X) -> {op,1,Op,F(Arg,X)};
            F({'bin_element',_N,Arg,P1,P2},X) -> {'bin_element',_N,F(Arg,X),P1,P2};
            F(T,_X) -> T
        end,
    %
    lists:foldl(fun(Arg,{value,_Value,X}) ->
                        case Fc(Arg) of
                            false -> Fe(Arg,X);
                            true -> Fe(F(Arg,X),X)
                        end end, {value,undefined,Bindings}, S2).

%% ====================================================================
%% Sub internal functions
%% ====================================================================

%% @private
%% prepare meta functions to get variable values
prepare_var_getters(#state_scr{variables=Vars}=State) ->
    % Funs for vars
    % ----------
    % return id by name or exists if not found
    FVarIdByName = fun V(VarName) when is_list(VarName) -> V(?BU:to_binary(VarName));
                       V(VarName) when is_binary(VarName) ->
                              case lists:foldl(fun(#variable{name=N, id=Id}, undefined) when N==VarName -> Id;
                                                (_,Acc) -> Acc
                                             end, undefined, Vars) of
                                undefined -> exit(<<"var: internal error, variable not found">>);
                                VarId -> VarId
                            end end,
    % return id by name or id
    FVarId = fun(VarX) ->
                     case catch FVarIdByName(VarX) of
                         {'EXIT',_} -> ?BU:to_binary(VarX);
                         Id -> Id
                     end end,
    % ----------
    % return val by id or exists if not found or value is undefined
    FVarValById = fun(VarId) ->
                        case ?SCR_VAR:get_value(VarId, undefined, State) of
                            undefined -> exit(<<"var: internal error, variable not found or undefined">>);
                            {S,_Enc}=Value when is_binary(S), is_binary(_Enc) -> {SUtf8,_}=?SCRU:to_utf8(Value), SUtf8; % @encstring
                            Value -> Value
                        end end,
    % return val by id or name or exists if not found or values is undefined
    FVarVal = fun(VarX) ->
                      VarId = FVarId(VarX),
                      FVarValById(VarId)
              end,
    % return val by id or name, if value is undefined then return Default.
    FVarValDef = fun(VarX,Default) ->
                      VarId = FVarId(VarX),
                      case ?SCR_VAR:get_value(VarId, undefined, State) of
                          undefined -> Default;
                          {S,_Enc}=Value when is_binary(S), is_binary(_Enc) -> {SUtf8,_}=?SCRU:to_utf8(Value), SUtf8; % @encstring
                          Value -> Value
                      end end,
    % return val with encoding by id or exists if not found or value is undefined
    FVarEnc = fun(VarX) ->
                     VarId = FVarId(VarX),
                     case ?SCR_VAR:get_value(VarId, undefined, State) of
                         undefined -> exit(<<"var: internal error, variable not found or undefined">>);
                         {S,_Enc} when is_binary(S), is_binary(_Enc) -> S; % @encstring
                         Value -> Value
                     end end,
    % ----------
    % return true if value is defined, false if value is undefined, undefined if variable not found
    FVarCheck = fun(VarX) ->
                        VarId = FVarId(VarX),
                         case ?SCR_VAR:get_value(VarId, undefined, State) of
                             undefined ->
                                lists:foldl(fun(#variable{id=V}, <<"undefined">>) when V==VarId -> <<"false">>;
                                               (_,Acc) -> Acc
                                            end, <<"undefined">>, Vars);
                             _ -> <<"true">>
                         end end,
    % ----------
    % return variable value if defined, else return DefaultValue and setup DefaultValue to variable
    FVarValSetup = fun(VarX,DefValue) ->
                        VarId = FVarId(VarX),
                        case ?SCR_VAR:get_value(VarId, undefined, State) of
                            undefined -> ?SCR_VAR:set_value(VarId,case DefValue of _ when is_list(DefValue) -> ?BU:to_binary(DefValue); _ -> DefValue end,State), DefValue;
                            {S,_Enc}=Value when is_binary(S), is_binary(_Enc) -> {SUtf8,_}=?SCRU:to_utf8(Value), SUtf8; % @encstring
                            Value -> Value
                        end end,
    % ----------
    [{{get_variable_id_by_name,1},FVarIdByName},
     {{get_variable_id,1},FVarId},
     {{get_variable_value_by_id,1},FVarValById},
     {{get_variable_value,1},FVarVal},
     {{get_variable_orig,1},FVarEnc},
     {{check_variable,1},FVarCheck},
     {{get_variable_value,2},FVarValDef},
     {{set_variable_value_if_undefined,2},FVarValSetup} ].

%% @private
%% prepare meta functions to use script state for read and write
prepare_state_modifiers(#state_scr{}=_State) -> [].

%% --------------------------------------
%% local fun handler
%% handles calls to script vars, other script functions
%% if script function should override system function - then it should be handled in nonlocals
make_local_fun(Meta) ->
    %?OUT('$debug',"make_local_fun"),
    fun E(get_variable_value_by_id, Args, Bindings) -> get_variable_value_by_id(Args, Bindings, Meta, E);
        E(get_variable_value, Args, Bindings) -> var(Args, Bindings, Meta, E);
        E(get_variable_orig, Args, Bindings) -> get_variable_orig(Args, Bindings, Meta, E);
        E(var, Args, Bindings) -> var(Args, Bindings, Meta, E);
        E(eval, Args, Bindings) -> eval(Args, Bindings, Meta, E);
        E(FunName, Args, Bindings) -> exec_fun(FunName, Args, Bindings, Meta, E)
    end.

%% nonlocal fun handler
%% blocks calls to file, code, and some other system modules... Also could be blocked to r_* modules to avoid system calls
make_nonlocal_fun(_Meta) ->
    %?OUT('$debug',"make_nonlocal_fun"),
    fun ({erlang,Fun}, Args) ->
             %?OUT('$debug',"fun nonlocal: erlang:~500tp", [Fun]),
             ArgLen = length(Args),
             case ?BU:function_exported(?SCR_EXPRF, Fun, ArgLen) of
                 true -> erlang:apply(?SCR_EXPRF, Fun, Args);
                 false -> erlang:apply(erlang, Fun, Args)
             end;
        ({xunit,Fun}, Args) ->
             case Fun of
                  absolutepath ->
                     [RPath] = Args,
                     {ok,State} = get_script_state(),
                     case ?SCR_FILE:expand_and_check_path(?BU:to_binary(RPath), read, State) of
                         {error,_} -> <<>>;
                         {ok,Path} -> ?SCR_FILE:get_absolute_path(Path)
                     end;
                 _ -> exit(<<"xunit:", (?BU:to_binary(Fun))/binary, ": unknown xunit function">>)
              end;
        ({Mod,Fun}, Args) ->
              %?OUT('$debug',"Audit fun nonlocal: ~500tp:~500tp", [Mod,Fun]),
              case ?BU:to_binary(Mod) of
                 BMod ->
                     case is_denied(BMod,?BU:to_binary(Fun)) of
                         true -> exit(<<BMod/bitstring, ": access denied">>);
                         false ->
                             ArgLen = length(Args),
                             case ?BU:function_exported(Mod,Fun,ArgLen) of
                                 true ->
                                     erlang:apply(Mod,Fun,Args);
                                 false ->
                                     BMod = ?BU:to_binary(Mod),
                                     BFun = ?BU:to_binary(Fun),
                                     BLen = ?BU:to_binary(ArgLen),
                                     exit(<<BMod/bitstring, ":", BFun/bitstring, "/", BLen/bitstring, " not found">>)
                             end end end end.

%% @private
is_denied(_, _) -> false.
%%is_denied(<<"os">>, _) -> true;
%%is_denied(<<"file">>, _) -> true;
%%is_denied(<<"code">>, _) -> true;
%%is_denied(<<"init">>, _) -> true;
%%is_denied(<<"rpc">>, _) -> true;
%%is_denied(<<"erlang">>, <<"apply">>) -> true;
%%is_denied(<<"erlang">>, <<"spawn",_/bitstring>>) -> true;
%%is_denied(_,_) -> false.

%% ====================================================================
%% Privates
%% ====================================================================

%% @private
%% get variable value by direct
get_variable_value_by_id(Args,Bindings, Meta, EF) ->
    {Args1,Bindings1} = erl_eval:expr_list(Args,Bindings,{eval,EF}),
    case Args1 of
        [VarId] when is_binary(VarId) -> get_variable(VarId, Bindings1, Meta);
        _ -> exit(<<"var: invalid argument type">>)
    end.

%% @private
%% get variable value used as function of name or id, for ex. var("Variable Name").
var(Args, Bindings, Meta, EF) ->
    {Args1,Bindings1} = erl_eval:expr_list(Args,Bindings,{eval,EF}),
    case Args1 of
        [VarX] when is_list(VarX); is_binary(VarX) -> % ;is_atom(VarName)
            {_,F} = lists:keyfind({get_variable_id,1},1,Meta),
            get_variable(F(?BU:to_binary(VarX)), Bindings1, Meta);
         _ -> exit(<<"var: invalid argument type">>)
    end.

%% @private
%% evaluates expression
eval(Args, Bindings, Meta, EF) ->
    {Args1,Bindings1} = erl_eval:expr_list(Args,Bindings,{eval,EF}),
    case Args1 of
        [Expr] when is_list(Expr); is_binary(Expr) -> apply_internal(Expr, Meta, Bindings1);
        _ -> exit(<<"eval: invalid argument type">>)
    end.

%% @private
%% other functions
exec_fun(FunName, Args, Bindings, Meta, EF) ->
    {Args1,Bindings1} = erl_eval:expr_list(Args,Bindings,{eval,EF},{value,make_nonlocal_fun(Meta)}),
    ArgLen = length(Args1),
    % ?OUT('$debug',"exec fun ~500tp, ~500tp", [FunName, Args]),
    case ?BU:function_exported(?SCR_EXPRF, FunName, ArgLen) of
        true ->
            Value = erlang:apply(?SCR_EXPRF, FunName, Args1),
            {value,Value,Bindings1};
        false ->
            BFunName = ?BU:to_binary(FunName),
            BLen = ?BU:to_binary(ArgLen),
            case ?CFG:expression_function(FunName,ArgLen) of
                {ok,Fun} when is_function(Fun,ArgLen) ->
                    Value = erlang:apply(Fun,Args1),
                    % ?LOG('$debug', "Fun ~ts~500tp -> ~500tp.",[FunName,Args,Value]),
                    {value,Value,Bindings1};
                _ ->
                    case lists:keyfind({FunName,ArgLen},1,Meta) of
                        {_,Fun} when is_function(Fun,ArgLen) ->
                            Value = erlang:apply(Fun,Args1),
                            % ?LOG('$debug', "Fun ~ts~500tp -> ~500tp.",[FunName,Args,Value]),
                            {value,Value,Bindings1};
                        false ->
                            exit(<<BFunName/bitstring, "/", BLen/bitstring, ": function not found">>)
                    end end end.

%% @private
%% attends to get variable (strings converted to utf-8 automatically)
get_variable(VarId, Bindings, Meta) ->
    case erl_eval:binding(VarId, Bindings) of
        {value,Value} -> {value,Value,Bindings};
        unbound ->
            case lists:keyfind({get_variable_value_by_id,1},1,Meta) of
                {_,F} when is_function(F) ->
                    Value = F(VarId),
                    Bindings1 = erl_eval:add_binding(VarId,Value,Bindings),
                    {value,Value,Bindings1};
                false ->
                    exit(<<"var: internal error, function not found">>)
            end end.

%% @encstring
%% attends to get variable (strings leaved on original encoding)
get_variable_orig(Args,Bindings, Meta, EF) ->
    {Args1,Bindings1} = erl_eval:expr_list(Args,Bindings,{eval,EF}),
    case Args1 of
        [VarX] when is_binary(VarX); is_list(VarX) -> get_variable_orig_1(VarX, Bindings1, Meta);
        _ -> exit(<<"var: invalid argument type">>)
    end.

%% attends to get variable (strings leaved on original encoding)
get_variable_orig_1(VarX, Bindings, Meta) ->
    BindKey = {orig_enc,VarX},
    case erl_eval:binding(BindKey, Bindings) of
        {value,Value} -> {value,Value,Bindings};
        unbound ->
            case lists:keyfind({get_variable_orig_encoded,1},1,Meta) of
                {_,F} when is_function(F) ->
                    Value = F(VarX),
                    Bindings1 = erl_eval:add_binding(BindKey,Value,Bindings),
                    {value,Value,Bindings1};
                false ->
                    exit(<<"var: internal error, function not found">>)
            end
    end.
