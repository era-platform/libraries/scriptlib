%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc

-module(scriptlib_sm_expression_funs).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all,
          {no_auto_import,[size/1,length/1]}]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

-define(E, erlang).
-define(M, ?MODULE).
-define(ERREXPR, <<"error">>).
-define(ERREMPTYD, <<"domain_not_defined">>).
-define(ERRNOTAVAILD, <<"domain_not_available">>).
-define(ERRGETSCRST, <<"get_script_state_error">>).

-import(?SCR_EXPRU, [s/1, b/1, n/1, i/1]).

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------
%% type transform
%% ------------------------

to_binary(X) -> ?BU:to_binary(X).
to_int(X) -> ?BU:to_int(X).
to_integer(X) -> ?BU:to_int(X).
to_float(X) -> ?BU:to_float(X).
to_bool(X) -> ?BU:to_bool(X).
to_guid(X) -> ?BU:to_guid(X).
to_list(X) -> ?BU:to_unicode_list(X).
to_string(X) -> b(?BU:to_list(X)).

size(X) -> erlang:size(b(X)). % #392
length(X) -> erlang:length(s(X)). % #392

%% ------------------------
%% operations
%% ------------------------
'+'(A,B) when is_binary(A); is_binary(B); is_list(A); is_list(B); is_atom(A); is_atom(B) ->
    apply_to_numbers(A,B,
                     fun(A1,B1) -> A1 + B1 end,
                     fun(_A1,_B1) ->
                             A2 = ?BU:to_binary(A),
                             B2 = ?BU:to_binary(B),
                             <<A2/bitstring,B2/bitstring>>
                     end);
'+'(A,B) -> A + B.

'++'(A,B) ->
    A2 = ?BU:to_binary(A),
    B2 = ?BU:to_binary(B),
    <<A2/bitstring,B2/bitstring>>.

'-'(A,B) when is_binary(A); is_binary(B); is_list(A); is_list(B) ->
    apply_to_numbers(A,B,fun(A1,B1) -> A1-B1 end, fun(_,_) -> exit(<<"invalid argument for operator '-'">>) end);
'-'(A,B) -> A - B.

'*'(A,B) when is_binary(A); is_binary(B); is_list(A); is_list(B) ->
    apply_to_numbers(A,B,fun(A1,B1) -> A1*B1 end, fun(_,_) -> exit(<<"invalid argument for operator '*'">>) end);
'*'(A,B) -> A * B.

'/'(A,B) when is_binary(A); is_binary(B); is_list(A); is_list(B) ->
    apply_to_numbers(A,B,fun(A1,B1) -> A1/B1 end, fun(_,_) -> exit(<<"invalid argument for operator '/'">>) end);
'/'(A,B) -> A / B.

'div'(A,B) when is_binary(A); is_binary(B); is_list(A); is_list(B) ->
    apply_to_numbers(A,B,fun(A1,B1) -> A1 div B1 end, fun(_,_) -> exit(<<"invalid argument for operator 'div'">>) end);
'div'(A,B) -> A div B.

'rem'(A,B) when is_binary(A); is_binary(B); is_list(A); is_list(B) ->
    apply_to_numbers(A,B,fun(A1,B1) -> A1 rem B1 end, fun(_,_) -> exit(<<"invalid argument for operator 'rem'">>) end);
'rem'(A,B) -> A rem B.

% #398
'=='(A,B) when is_binary(A), is_binary(B) -> A == B;
'=='(A,B) when is_list(A), is_list(B) -> A == B;
'=='(A,B) when (is_binary(A) orelse is_list(A)) andalso (is_binary(B) orelse is_list(B)) -> ?BU:to_binary(A) == ?BU:to_binary(B);
'=='(A,B) -> A == B.

'/='(A,B) when is_binary(A), is_binary(B) -> A /= B;
'/='(A,B) when is_list(A), is_list(B) -> A /= B;
'/='(A,B) when (is_binary(A) orelse is_list(A)) andalso (is_binary(B) orelse is_list(B)) -> ?BU:to_binary(A) /= ?BU:to_binary(B);
'/='(A,B) -> A /= B.

'>'(A,B) when is_binary(A), is_binary(B) -> A > B;
'>'(A,B) when is_list(A), is_list(B) -> A > B;
'>'(A,B) when (is_binary(A) orelse is_list(A)) andalso (is_binary(B) orelse is_list(B)) -> ?BU:to_binary(A) > ?BU:to_binary(B);
'>'(A,B) -> A > B.

'>='(A,B) when is_binary(A), is_binary(B) -> A >= B;
'>='(A,B) when is_list(A), is_list(B) -> A >= B;
'>='(A,B) when (is_binary(A) orelse is_list(A)) andalso (is_binary(B) orelse is_list(B)) -> ?BU:to_binary(A) >= ?BU:to_binary(B);
'>='(A,B) -> A >= B.

'<'(A,B) when is_binary(A), is_binary(B) -> A < B;
'<'(A,B) when is_list(A), is_list(B) -> A < B;
'<'(A,B) when (is_binary(A) orelse is_list(A)) andalso (is_binary(B) orelse is_list(B)) -> ?BU:to_binary(A) < ?BU:to_binary(B);
'<'(A,B) -> A < B.

'=<'(A,B) when is_binary(A), is_binary(B) -> A =< B;
'=<'(A,B) when is_list(A), is_list(B) -> A =< B;
'=<'(A,B) when (is_binary(A) orelse is_list(A)) andalso (is_binary(B) orelse is_list(B)) -> ?BU:to_binary(A) =< ?BU:to_binary(B);
'=<'(A,B) -> A =< B.

% @private
apply_to_numbers(A,B,Fnum,Fbin) ->
    A1 = number_or_binary(A),
    B1 = number_or_binary(B),
    case is_binary(A1) orelse is_binary(B1) of
        false -> Fnum(A1,B1);
        true -> Fbin(A1,B1)
    end.

% @private
number_or_binary(X) ->
    X1 = ?BU:to_binary(X),
    try erlang:binary_to_float(X1)
    catch error:_ ->
              try erlang:binary_to_integer(X1)
              catch error:_ -> X1
              end end.

%% ------------------------
%% transforms (oktell)
%% ------------------------

char(X) -> b([i(X)]).
str(X) -> ?BU:to_binary(X).
num(X) -> n(X).
numval(X) when is_number(X) -> X;
numval(X) -> lists:foldl(fun(_, []) -> 0;
                            (_, {ok,N}) -> N;
                            (_, Acc) -> case ?SCR_EXPRU:try_convert_to_num(Acc) of undefined -> lists:droplast(Acc); N -> {ok,N} end
                           end, s(X), lists:seq(0,length(s(X)))).

dechex(X) -> b(integer_to_list(i(X), 16)).
hexdec(X) -> b(list_to_integer(s(X), 16)).
%strhex(X) -> lists:foldl(fun(C,Acc) -> C1 = list_to_binary(integer_to_list(C,16)), <<Acc/bitstring,C1/bitstring>> end, <<>>, s(X)).
%hexstr(X) -> ok.
%guidtoint(X) -> ok.

%eval(X) -> ?SCR_EXPR:apply(s(X), []).
ifelse(Bool, A, B) -> case ?BU:to_bool(Bool) of true -> A; false -> B end.
md5(X) -> ?BU:md5(s(X)).

%translit(X) -> ok.

base64decode(X) -> base64:decode(b(X)).
base64encode(X) -> base64:encode(b(X)).
%base64decodeutf8(X) -> ok.
%base64encodeutf8(X) -> ok.

urldecode(X) -> b(?BU:urldecode(s(X))).%b(http_uri:decode(s(X))).
urlencode(X) -> b(?BU:urlencode(s(X))).%b(http_uri:encode(s(X))).
%urldecodeutf8(X) -> ok.
%urlencodeutf8(X) -> ok.

htmldecode(X) -> ?BLhtml:decode(b(X)).
htmlencode(X) -> ?BLhtml:encode(b(X)).

escape(X) ->
    [$"|SR]=S = s(jsx:encode(b(X))),
    case lists:suffix([$"],S) and lists:prefix([$"],S) of
        true -> b(lists:droplast(SR));
        false -> b(S)
    end.
unescape(X) ->
    S = s(X),
    case lists:suffix([$"],S) and lists:prefix([$"],S) of
        true -> b(jsx:decode(b(S)));
        false -> b(jsx:decode(b([$"] ++ S ++ [$"])))
    end.

%% ------------------------
%% string constants (oktell)
%% ------------------------

tab() -> <<"\t">>.
endline() -> <<"\r\n">>.
quot() -> <<"'">>.
dblquot() -> <<"\"">>.

%% ------------------------
%% math
%% ------------------------

sin(X) -> math:sin(n(X)).
cos(X) -> math:cos(n(X)).
tan(X) -> math:tan(n(X)).
asin(X) -> math:asin(n(X)).
acos(X) -> math:acos(n(X)).
atan(X) -> math:atan(n(X)).
atan2(Y,X) -> math:atan2(n(Y),n(X)).
sinh(X) -> math:sinh(n(X)).
cosh(X) -> math:cosh(n(X)).
tanh(X) -> math:tanh(n(X)).
%asinh(X) -> math:asinh(n(X)).
%acosh(X) -> math:acosh(n(X)).
%atanh(X) -> math:atanh(n(X)).
exp(X) -> math:exp(n(X)).
log(X) -> math:log(n(X)).
log2(X) -> math:log2(n(X)).
log10(X) -> math:log10(n(X)).
pow(X,Y) -> math:pow(n(X),n(Y)).
sqrt(X) -> math:sqrt(n(X)).
erf(X) -> math:erf(n(X)).
erfc(X) -> math:erfc(n(X)).

%% erlang functions. math
max(A,B) -> erlang:max(n(A),n(B)).
min(A,B) -> erlang:min(n(A),n(B)).
trunc(A) -> erlang:trunc(n(A)).
round(A) -> erlang:round(n(A)).
abs(A) -> erlang:abs(n(A)).

%% math from oktell
floor(X) -> ?SCR_EXPRU:floor(n(X)).
ceil(X) -> ?SCR_EXPRU:ceil(n(X)).
lg(X) -> math:log10(n(X)).
ln(X) -> math:log(n(X)).
log(X,Y) -> math:log(n(X))/math:log(n(Y)).
sqr(X) -> n(X)*n(X).

%% math constants (oktell)
e() -> math:exp(1).
pi() -> math:pi().
phi() -> (1 + math:sqrt(5)) / 2.

%% math random
random() -> ?BU:random(100000000)/100000000.
random(To) -> ?BU:random(i(To)).
random(From,To) -> ?BU:random(i(From),i(To)).

%% -------------------------------
%% erlang functions. hashcodes
%% -------------------------------

crc32(A) -> erlang:crc32(?BU:to_binary(A)).
crc32(Old, A) -> erlang:crc32(n(Old), ?BU:to_binary(A)).
adler32(A) -> erlang:adler32(?BU:to_binary(A)).
adler32(Old, A) -> erlang:adler32(n(Old), ?BU:to_binary(A)).
phash2(A) -> erlang:phash2(A).

%% -------------------------------
%% string functions (oktell)
%% -------------------------------

newid() -> ?BU:newid().
len(A) -> length(s(A)).
concat(A,B) -> b(string:concat(s(A),s(B))).
equal(A,B) -> string:equal(s(A),s(B)).
str(A,Sub) -> string:str(s(A),s(Sub)).
rstr(A,Sub) -> string:rstr(s(A),s(Sub)).
substr(A,From) -> substr(A,From,100000000).
substr(A,From,Len) ->
    {A1,F1,L1} = {s(A),i(From),i(Len)},
    R = case length(A1) of
            L when L>F1, F1>=1 ->
                {_,A2}=lists:split(F1-1,A1),
                case length(A2)>L1 of
                    true -> {A3,_}=lists:split(L1,A2), A3;
                    false -> A2
                end;
            _ -> ""
        end,
    b(R).
replace(A,Pat,Rep) -> binary:replace(b(A),b(Pat),b(Rep),[global]).
lower(A) -> b(string:lowercase(s(A))).
upper(A) -> b(string:uppercase(s(A))).
remove(A,From) -> ?M:remove(A,From,100000000).
remove(A,From,Len) ->
    {A1,F1,L1} = {s(A),i(From),i(Len)},
    R = case length(A1) of
            L when L>F1, F1>=1 ->
                {A2,X}=lists:split(F1-1,A1),
                case length(X)>L1 of
                    true -> {_Y,A3}=lists:split(L1,X), A2++A3;
                    false -> A2
                end;
            _ -> A1
        end,
    b(R).

indexof(A,B) -> ?M:str(A,B).
indexof(A,B,From) ->
    case ?M:str(?M:substr(A,From),B) of
        0 -> 0;
        I -> I+i(From)-1
    end.

trim(A) -> ?M:rtrim(?M:ltrim(A)).
ltrim(A) -> lists:dropwhile(fun(X) when X==$\t;X==$\r;X==$\n;X==32 -> true; (_) -> false end, s(A)).
rtrim(A) -> lists:reverse(lists:dropwhile(fun(X) when X==$\t;X==$\r;X==$\n;X==32 -> true; (_) -> false end, lists:reverse(s(A)))).
trimstart(A) -> ltrim(A).
trimend(A) -> rtrim(A).

left(A,Len) -> string:left(s(A),i(Len)).
right(A,Len) -> string:right(s(A),i(Len)).

reverse(A) -> string:reverse(s(A)).

% #298 unicode
regexreplace(A,S1,S2) ->
    b(re:replace(s(A),s(S1),s(S2),[{return,binary},unicode])).
regexreplaceg(A,S1,S2) ->
    b(re:replace(s(A),s(S1),s(S2),[{return,binary},global,unicode])).

translit(Str) -> ?BU:str_translit(b(Str)).

%% ----------------------------
%% operations
%% ----------------------------
% +, -, *, /, rem, div, bnot, band, bor, bxor, bsl, bsr

%% ----------------------------
%% datetime (oktell)
%% ----------------------------
nowtick() -> ?BU:timestamp().
now() -> ?SCR_EXPRU:to_datetime_local(?BU:localdatetime_ms()).
nowutc() -> ?SCR_EXPRU:to_datetime_utc(?BU:utcdatetime_ms()).
nowgregsecond() -> ?BU:current_gregsecond().

ticktolocal(TSx) ->
    TS = i(TSx),
    XMs = TS rem 1000,
    XS = TS div 1000 rem 1000000,
    XD = TS div 1000000000,
    {D,T} = calendar:now_to_local_time({XD,XS,XMs * 1000}),
    ?SCR_EXPRU:to_datetime_local({D,T,XMs}).
ticktoutc(TSx) ->
    TS = i(TSx),
    XMs = TS rem 1000,
    XS = TS div 1000 rem 1000000,
    XD = TS div 1000000000,
    {D,T} = calendar:now_to_universal_time({XD,XS,XMs * 1000}),
    ?SCR_EXPRU:to_datetime_utc({D,T,XMs}).

dateformat(Fmt, X) -> ?SCR_EXPRU:to_datetime(s(Fmt),?SCR_EXPRU:parse_datetime(b(X))).

date() -> ?SCR_EXPRU:to_date(?BU:localdatetime_ms()).
date(X) -> {D,_T,_Ms} = ?SCR_EXPRU:parse_datetime(b(X)), ?SCR_EXPRU:to_date({D,{0,0,0},0}).
time() -> ?SCR_EXPRU:to_time(?BU:localdatetime_ms()).
time(X) -> {_D,T,Ms} = ?SCR_EXPRU:parse_datetime(b(X)), ?SCR_EXPRU:to_time({{1970,1,1},T,Ms}).

datetime(X) -> ?SCR_EXPRU:to_datetime(?SCR_EXPRU:parse_datetime(b(X))).
datetime(Y,M,D) -> ?SCR_EXPRU:to_date({{i(Y),i(M),i(D)},{0,0,0},0}).
datetime(Y,M,D,H,Mi) -> ?SCR_EXPRU:to_datetime({{i(Y),i(M),i(D)},{i(H),i(Mi),0},0}).
datetime(Y,M,D,H,Mi,S) -> ?SCR_EXPRU:to_datetime({{i(Y),i(M),i(D)},{i(H),i(Mi),i(S)},0}).
datetime(Y,M,D,H,Mi,S,Ms) -> ?SCR_EXPRU:to_datetime({{i(Y),i(M),i(D)},{i(H),i(Mi),i(S)},i(Ms)}).

datediff(Part, A1, A2) ->
    PartStr = ?BU:to_list(Part),
    {D1,T1,Ms1} = ?SCR_EXPRU:parse_datetime_utc(b(A1)),
    {D2,T2,Ms2} = ?SCR_EXPRU:parse_datetime_utc(b(A2)),
    {Days,{H,Mi,S}} = calendar:time_difference({D1,T1},{D2,T2}),
    case PartStr of
        "yy" -> Days div 365;
        "yyyy" -> Days div 365;
        "q" -> Days div 91;
        "qq" -> Days div 91;
        "m" -> Days div 30;
        "mm" -> Days div 30;
        "ww" -> Days div 7;
        "wk" -> Days div 7;
        "d" -> Days;
        "dd" -> Days;
        "h" -> Days*24 + H;
        "hh" -> Days*24 + H;
        "mi" -> Days*1440 + H*60 + Mi;
        "n" -> Days*1440 + H*60 + Mi;
        "s" -> Days*86400 + H*3600 + Mi*60 + S;
        "ss" -> Days*86400 + H*3600 + Mi*60 + S;
        "ms" -> (Days*86400 + H*3600 + Mi*60 + S)*1000 + Ms2-Ms1
     end.

dateadd(Part, X, Dt) when is_integer(X) ->
    PartStr = ?BU:to_list(Part),
    Dt1 = b(Dt),
    {D1,T1,Ms1} = ?SCR_EXPRU:parse_datetime_utc(Dt1),
    GS = ?BU:gregsecond({D1,T1}),
    GS1 = case PartStr of
              "yy" -> GS + X * 365 * 86400;
              "yyyy" -> GS + X * 365 * 86400;
              "q" -> GS + X * 91 * 86400;
              "qq" -> GS + X * 91 * 86400;
              "m" -> GS + X * 30 * 86400;
              "mm" -> GS + X * 30 * 86400;
              "ww" -> GS + X * 7 * 86400;
              "wk" -> GS + X * 7 * 86400;
              "d" -> GS + X * 86400;
              "dd" -> GS + X * 86400;
              "h" -> GS + X * 3600;
              "hh" -> GS + X * 3600;
              "mi" -> GS + X * 60;
              "n" -> GS + X * 60;
              "s" -> GS + X;
              "ss" -> GS + X;
              "ms" -> GS + X div 1000
          end,
    Ms2 = case PartStr of "ms" -> Ms1 + X rem 1000; _ -> Ms1 end,
    {D2,T2} = calendar:gregorian_seconds_to_datetime(GS1),
    ?SCR_EXPRU:to_datetime_utc({D2,T2,Ms2}).

year(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {{Y,_,_},_,_} -> Y; undefined -> exit(<<"Invalid datetime">>) end.
month(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {{_,M,_},_,_} -> M; undefined -> exit(<<"Invalid datetime">>) end.
day(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {{_,_,D1},_,_} -> D1; undefined -> exit(<<"Invalid datetime">>) end.
hour(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {_,{H,_,_},_} -> H; undefined -> exit(<<"Invalid datetime">>) end.
minute(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {_,{_,Mi,_},_} -> Mi; undefined -> exit(<<"Invalid datetime">>) end.
second(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {_,{_,_,S},_} -> S; undefined -> exit(<<"Invalid datetime">>) end.
millisecond(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {_,_,Ms} -> Ms; undefined -> exit(<<"Invalid datetime">>) end.

dayofyear(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {{Y,_,_}=D1,T1,_} -> {Days,_} = calendar:time_difference({{Y,1,1},{0,0,0}},{D1,T1}), Days + 1; undefined -> exit(<<"Invalid datetime">>) end.
dayofweek(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {Date,_,_} -> calendar:day_of_the_week(Date); undefined -> exit(<<"Invalid datetime">>) end.
weekofyear(D) -> case ?SCR_EXPRU:parse_datetime(b(D)) of {Date,_,_} -> {_Y,WK}=calendar:iso_week_number(Date), WK; undefined -> exit(<<"Invalid datetime">>) end.
isleapyear(Y) -> calendar:is_leap_year(i(Y)).

% forcely set datetime format to utc
datetimeutc(D) ->
    case ?SCR_EXPRU:parse_datetime_utc(b(D)) of
        undefined -> exit(<<"Invalid datetime">>);
        Dt -> ?SCR_EXPRU:to_datetime_utc(Dt)
    end.

% consider D as localtime (as it is without timezone) and convert to utc datetime (adding hours)
localtoutc(D) ->
    case ?SCR_EXPRU:parse_datetime(b(D)) of
        {Date,Time,Ms} ->
            case calendar:local_time_to_universal_time_dst({Date,Time}) of
                [] -> ?SCR_EXPRU:to_datetime_utc({Date,Time,Ms});
                [{Date1,Time1}] -> ?SCR_EXPRU:to_datetime_utc({Date1,Time1,Ms});
                [{Date1,Time1},_] -> ?SCR_EXPRU:to_datetime_utc({Date1,Time1,Ms})
            end;
        undefined -> exit(<<"Invalid datetime">>)
    end.

% forcely set datetime format to local with timezone
datetimelocal(D) ->
    case ?SCR_EXPRU:parse_datetime_utc(b(D)) of
        {Date,Time,Ms} ->
            {Date1,Time1} = calendar:universal_time_to_local_time({Date,Time}),
            ?SCR_EXPRU:to_datetime_local({Date1,Time1,Ms});
        undefined -> exit(<<"Invalid datetime">>)
    end.

% consider D as utc (as it is with 0 timezone) and convert to datetime with local timezone (adding hours)
utctolocal(D) ->
    case ?SCR_EXPRU:parse_datetime(b(D)) of
        {Date,Time,Ms} ->
            {Date1,Time1} = calendar:universal_time_to_local_time({Date,Time}),
            ?SCR_EXPRU:to_datetime_local({Date1,Time1,Ms});
        undefined -> exit(<<"Invalid datetime">>)
    end.

valid_date(D) ->
    try ?SCR_EXPRU:parse_datetime(b(D)) of
        {Date,{H,Mi,S},Ms} ->
            case calendar:valid_date(Date) of
                true when H>=0, H=<23, Mi>=0, Mi=<59, S>=0, S=<59, Ms>=0, Ms=<999 -> true;
                _ -> false
            end;
        undefined -> false
    catch
        error:_ -> false;
        throw:_ -> false;
        exit:_ -> false
    end.

%% ------------------------
%% JSON data
%% ------------------------
modify_json(Query, Data, OperationData) -> ?JSON:exec(Query, Data, OperationData).
modify_json(Query, Data, OperationData, Default) -> ?JSON:exec(Query, Data, OperationData, Default).

site() -> ?CFG:site().

domain() ->
    case ?SCR_EXPR:get_script_state() of
        {error,_} -> log_err(?ERRGETSCRST), ?ERREXPR;
        {ok,#state_scr{domain=undefined}=_ScrState} -> log_err(?ERREMPTYD), ?ERREXPR;
        {ok,#state_scr{domain=Domain}} -> b(Domain)
    end.

%% ---
%% @private
log_err(Err) -> ?LOG('$error', "Script Expression. Error! Reason:~120p",[Err]).

%% ------------------------
%% file ops
%% ------------------------
filename(RPath) -> b(filename:basename(s(RPath))).
fileext(RPath) -> b(filename:extension(s(RPath))).
filedir(RPath) -> b(filename:dirname(s(RPath))).
makepath(RPathBase, RFilePath) -> b(filename:join(s(RPathBase), s(RFilePath))).
%% scripts has no lists!
%% splitpath(RPath) -> lists:foldl(fun(S, Acc) -> lists:append([b(S)], Acc) end, [], filename:split(s(RPath))).
%% joinpath(PathList) -> b(filename:join(lists:foldl(fun(B, Acc) -> lists:append([s(B)], Acc) end, [], PathList))).

%% ------------------
%% storage on site
%% ------------------
storage_put(Key,Value) -> storage_put(Key,Value,2*86400).
storage_put(Key,Value,Timeout) ->
    skip_check_expression(
        fun() ->
            Key1 = <<"script_storage_",(b(Key))/binary>>,
            case ?SCR_EXPR_STORAGE:put(Key1,Value,i(Timeout))  of
                error -> <<"error">>;
                ok -> <<"ok">>
            end end).

storage_get(Key,Default) ->
    skip_check_expression(
        fun() ->
            Key1 = <<"script_storage_",(b(Key))/binary>>,
            case ?SCR_EXPR_STORAGE:get(Key1,Default) of
                error -> <<"error">>;
                false -> Default;
                {_K,Value} -> Value
            end end).

storage_del(Key) ->
    skip_check_expression(
        fun() ->
            Key1 = <<"script_storage_",(b(Key))/binary>>,
            case ?SCR_EXPR_STORAGE:delete(Key1) of
                error -> <<"error">>;
                ok -> <<"ok">>
            end end).

%% -----------------
%% components interpretator
%% -----------------
component(Type,DataOpts) ->
    skip_check_expression(
        fun(StateTag) ->
            F = ?SCR_EXPR_COMP:get_component_fun2(StateTag),
            F(Type,DataOpts)
        end).

component(Type,DataOpts,ReturnFormat) ->
    skip_check_expression(
        fun(StateTag) ->
            F = ?SCR_EXPR_COMP:get_component_fun3(StateTag),
            F(Type,DataOpts,ReturnFormat)
        end).

lock(Key,WaitMs) ->
    lock(Key,2,WaitMs,0).

lock(Key,WaitMs,Mode,AutoUnlockMs) ->
    skip_check_expression(
        fun(StateTag) ->
            F = ?SCR_EXPR_COMP:get_component_fun3(StateTag),
            MapOpts = #{<<"key">> => b(Key),
                        <<"action">> => 1, % lock
                        <<"mode">> => case ?BU:to_binary(Mode) of <<"1">> -> 1; <<"read">> -> 1; _ -> 2 end,
                        <<"timeoutMs">> => i(WaitMs),
                        <<"autoUnlockTimeoutMs">> => i(AutoUnlockMs)},
            case F('mutex',MapOpts,map) of
                Res when is_map(Res) ->
                    case maps:get(<<"transferKey">>,Res) of
                        <<"transfer">> -> <<"ok">>;
                        <<"transferTimeout">> -> <<"timeout">>;
                        _ -> <<"error">>
                    end;
                _ -> <<"error">>
            end end).

unlock(Key) ->
    skip_check_expression(
        fun(StateTag) ->
            F = ?SCR_EXPR_COMP:get_component_fun3(StateTag),
            MapOpts = #{<<"key">> => b(Key),
                <<"action">> => 2},
            case F('mutex',MapOpts,map) of
                Res when is_map(Res) ->
                    case maps:get(<<"transferKey">>,Res) of
                        <<"transfer">> -> <<"ok">>;
                        _ -> <<"error">>
                    end;
                _ -> <<"error">>
            end end).

%% ====================================================================
%% Internal functions
%% ====================================================================

skip_check_expression(F) when is_function(F,1); is_function(F,0) ->
    case ?SCR_EXPR:apply_script_state(fun(#state_scr{}=State) -> ?SCR_EXPRU:shrink_state(State) end) of
        {ok,#state_scr{selfpid=undefined}} -> <<>>;
        {ok,StateTag} when is_function(F,1)-> F(StateTag);
        {ok,_} when is_function(F,0)-> F()
    end.

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

%% init function, load environment
start_test_() ->
    include_environment(),
    [].

%% @private attach env project
include_environment() ->
    {ok,CWD} = file:get_cwd(),
    RPath = filename:dirname(filename:dirname(CWD)),
    Path = filename:join([RPath, "rostell_env", "ebin"]),
    code:add_patha(Path).

%% -----------------------
indexof_test_() ->
    {"indexof test",
     [?_assertEqual(2,indexof("QWE","WE")),
      ?_assertEqual(2,indexof("QWE","WE",1)),
      ?_assertEqual(2,indexof("QWE","WE",2)),
      ?_assertEqual(0,indexof("QWE","WE",3)),
      ?_assertEqual(0,indexof("QWE","WE",10)),
      ?_assertEqual(0,indexof("QWE","фы",2)),
      ?_assertEqual(5,indexof("йцукфыва","фы")),
      ?_assertEqual(5,indexof("йцукфыва","фы",3)),
      ?_assertEqual(5,indexof("йцукфыва","фы",5)),
      ?_assertEqual(0,indexof("йцукфыва","фы",6)),
      ?_assertEqual(5,indexof("йцукфыва",<<"фы"/utf8>>,2))]}.

%% -----------------------
len_test_() ->
    {"len test",
     [?_assertEqual(0,len("")),
      ?_assertEqual(4,len("asdf")),
      ?_assertEqual(4,len([0,1,2,3])),
      ?_assertEqual(4,len("фыва")),
      ?_assertEqual(4,len(<<"asdf">>)),
      ?_assertEqual(4,len(<<"фыва"/utf8>>)),
      ?_assertEqual(2,len(<<208,159,208,149>>))]}.

%% -----------------------
upper_test_() ->
    {"upper test",
     [?_assertEqual(<<"ASDF">>,upper("asdf")),
      ?_assertEqual(<<"ASDF">>,upper("ASDF")),
      ?_assertEqual(<<"ФЫВА"/utf8>>,upper("фыва")),
      ?_assertEqual(<<"ФЫВА"/utf8>>,upper(<<"фыва"/utf8>>)),
      ?_assertEqual(<<"ФЫВА"/utf8>>,upper(<<"ФЫВА"/utf8>>)),
      ?_assertEqual(<<"345">>,upper(<<"345">>)),
      ?_assertEqual(<<0,1,2,3>>,upper(<<0,1,2,3>>)),
      ?_assertEqual(<<0,1,2,3>>,upper([0,1,2,3])),
      ?_assertEqual(<<0,208,159,208,2>>,upper(<<0,208,159,208,2>>))]}.

%% -----------------------
lower_test_() ->
    {"lower test",
     [?_assertEqual(<<"asdf">>,lower("asdf")),
      ?_assertEqual(<<"asdf">>,lower("ASDF")),
      ?_assertEqual(<<"фыва"/utf8>>,lower("фыва")),
      ?_assertEqual(<<"фыва"/utf8>>,lower(<<"фыва"/utf8>>)),
      ?_assertEqual(<<"фыва"/utf8>>,lower(<<"ФЫВА"/utf8>>)),
      ?_assertEqual(<<"345">>,lower(<<"345">>)),
      ?_assertEqual(<<0,1,2,3>>,lower(<<0,1,2,3>>)),
      ?_assertEqual(<<0,1,2,3>>,lower([0,1,2,3])),
      ?_assertEqual(<<0,240,159,240,2>>,lower(<<0,208,159,208,2>>))]}. % ?

%% -----------------------
str_test_() ->
    {"str test",
     [?_assertEqual(2,str("asdf","sd")),
      ?_assertEqual(0,str("asdf","g")),
      ?_assertEqual(2,str("asdfasdf","sd")),
      ?_assertEqual(0,str("asdfasdf","фыва")),
      ?_assertEqual(3,str("фывафыва","ва")),
      ?_assertEqual(2,str("asdfasdf",<<"sd"/utf8>>)),
      ?_assertEqual(0,str("asdfasdf",<<"фыва"/utf8>>)),
      ?_assertEqual(3,str(<<"фывафыва"/utf8>>,<<"ва"/utf8>>))]}.

%% -----------------------
rstr_test_() ->
    {"rstr test",
     [?_assertEqual(2,rstr("asdf","sd")),
      ?_assertEqual(0,rstr("asdf","g")),
      ?_assertEqual(6,rstr("asdfasdf","sd")),
      ?_assertEqual(0,rstr("asdfasdf","фыва")),
      ?_assertEqual(7,rstr("фывафыва","ва")),
      ?_assertEqual(6,rstr("asdfasdf",<<"sd"/utf8>>)),
      ?_assertEqual(0,rstr("asdfasdf",<<"фыва"/utf8>>)),
      ?_assertEqual(7,rstr(<<"фывафыва"/utf8>>,<<"ва"/utf8>>))]}.

-endif.
