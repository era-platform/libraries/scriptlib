%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.08.2021
%%% @doc Internal script component execution.

-module(scriptlib_sm_expression_component).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_component_fun2/1,
         get_component_fun3/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

-define(VarPrefix, "exprtemp##_").
-define(TransferVarId, <<"00000001-0000-0000-0000-000000000000">>).
-define(TransferVarName, <<"transferKey">>).

%% ====================================================================
%% Public functions
%% ====================================================================

get_component_fun2(#state_scr{}=State) ->
    StateTag = ?SCR_EXPRU:shrink_state(State),
    fun(Type,DataOpts) ->
        exec_component(Type,DataOpts,auto,StateTag)
    end.

get_component_fun3(#state_scr{}=State) ->
    StateTag = ?SCR_EXPRU:shrink_state(State),
    fun(Type,DataOpts,ReturnFormat) ->
            exec_component(Type,DataOpts,ReturnFormat,StateTag)
    end.

%% @private
exec_component(Type,DataOpts,ReturnFormat,#state_scr{}=StateTag) ->
    case parse_type(Type) of
        false ->
            ?SCR_ERROR(StateTag, " Expression subscript process unknown component type: ~120tp", [Type]),
            <<"error:101">>;
        {ok,TypeId,Module} ->
            try case start_script(TypeId,Module,DataOpts,StateTag) of
                    {error,Reason} ->
                        ?SCR_ERROR(StateTag, " Expression subscript process start error: ~120tp", [Reason]),
                        <<"error:103">>;
                    {ok,Pid} ->
                        MonRef = erlang:monitor(process,Pid),
                        F = fun F() ->
                                    receive
                                        abort -> ?SCRSRV:stop(Pid,{stop_external,owner}), throw(abort);
                                        {'DOWN',MonRef,process,Pid,_} -> <<"error:104">>;
                                        {scriptmachine,{main_stopped,Res}}=T when is_map(Res) ->
                                            Result = maps:get(result,Res),
                                            case ?BU:get_by_key(stoptype,Result) of
                                                normal ->
                                                    Result1 = lists:map(fun({_VId,Name,{Value,_}}) -> {Name,Value};
                                                                           ({_VId,Name,Value}) -> {Name,Value}
                                                                        end, ?BU:get_by_key(variables,Result)),
                                                    case ?BU:to_binary(ReturnFormat) of
                                                        <<"auto">> -> case DataOpts of
                                                                          _ when is_list(DataOpts) -> Result1;
                                                                          _ when is_map(DataOpts) -> maps:from_list(Result1);
                                                                          _ when is_binary(DataOpts) -> jsx:encode(Result1);
                                                                          _ -> jsx:encode(Result1)
                                                                      end;
                                                        <<"map">> -> maps:from_list(Result1);
                                                        <<"json">> -> jsx:encode(Result1);
                                                        <<"binary">> -> jsx:encode(Result1);
                                                        <<"proplist">> -> Result1;
                                                        <<"list">> -> Result1;
                                                        _ -> jsx:encode(Result1)
                                                    end;
                                                error ->
                                                    ?SCR_ERROR(StateTag, " Expression subscript process stop error: ~120tp", [?BU:get_by_key(stopreason,Result)]),
                                                    <<"error:105">>;
                                                T ->
                                                    ?SCR_ERROR(StateTag, " Expression subscript process stop unknown type: ~120tp, reason: ~120tp", [T,?BU:get_by_key(stopreason,Result)]),
                                                    <<"error:106">>
                                            end;
                                        _ -> F()
                                    end end,
                        F()
                end
            catch
                throw:abort ->
                    throw(abort);
                E:R:StackTrace ->
                    ?SCR_CRASH(StateTag, " Expression subscript process crashed: ~n\t~200tp", [{E,R,StackTrace}]),
                    <<"error:102">>
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
start_script(Type,Module,DataOpts,#state_scr{domain=Domain,loglevel=SMLL}=StateTag) ->
    {ComponentInfo,Variables} = build_component_info(Type,Module,DataOpts,StateTag),
    {ok,ScriptItem} = get_script_item(ComponentInfo,Variables,StateTag),
    Code = maps:get(code,ScriptItem),
    Opts1 = [{type,svc},
             {domain,Domain},
             {script,ScriptItem},
             {code,Code},
             {meta,#{components => [],
                     scripts => [],
                     domain => Domain,
                     functions => [] }},
             {ownerpid, self()},
             {use_owner, true},
             {result_to_owner, true},
             {variables,[]},
             {loglevel, SMLL}],
    case ?SCRSRV:start(Opts1) of
        {ok, _Uuid, Pid} when is_pid(Pid) -> {ok,Pid};
        _ -> {error,{internal_error,?BU:strbin("Internal error. Cannot start script",[])}}
    end.

%% @private
parse_type(Type) when is_binary(Type); is_atom(Type) ->
    BType = ?BU:to_binary(Type),
    Components = maps:merge(?SCR_COMPONENTS_DEFAULT:default_components(), ?CFG:components()),
    case maps:get(BType,Components,undefined) of
        undefined -> false;
        Module -> {ok,BType,Module}
    end.

%% @private
build_component_info(Type,Module,DataOpts,#state_scr{selfpid=SMPid}=_StateTag) ->
    Metadata = Module:metadata(svc),
    Data = lists:ukeymerge(1, [{<<"oType">>,Type},
                               {<<"oId">>,2},
                               {<<"name">>,<<>>},
                               {pid,SMPid}], normalize_dataopts(DataOpts)),
    Vars = [
            [{<<"id">>,?TransferVarId},
             {<<"name">>,?TransferVarName},
             {<<"type">>,2},
             {<<"location">>,0}]
           ],
    FCompAssign = fun(Id,Value) ->
                        [{<<"oType">>,106},
                         {<<"oId">>,Id},
                         {<<"name">>,?TransferVarName},
                         {<<"mode">>,0},
                         {<<"variable">>,?TransferVarId},
                         {<<"value">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,?BU:to_binary(Value)}]},
                         {<<"transfer">>,3}]
                  end,
    {Data1,Vars1,AssignComps} =
        lists:foldl(fun({Key,POpts},{Data0,Vars0,AssignComps0}=Acc) ->
                            case maps:get(type,POpts) of
                                <<"variable">> ->
                                    Var = [{<<"id">>,VId = <<"00000002-0000-0000-0000-000000000000">>},
                                           {<<"name">>,?BU:to_binary(Key)},
                                           {<<"type">>,2},
                                           {<<"location">>,0}],
                                    {lists:keystore(Key,1,Data0,{Key,VId}),[Var|Vars0],AssignComps0};
                                <<"transfer">> ->
                                    NextId = erlang:phash2(Key),
                                    {lists:keystore(Key,1,Data0,{Key,NextId}),Vars0,[FCompAssign(NextId,Key)|AssignComps0]};
                                _ -> Acc
                            end end, {Data,Vars,[]}, Metadata),
    {[Data1|AssignComps],Vars1}.

%% @private
normalize_dataopts(DataOpts) ->
    DataX0 = case DataOpts of
                 _ when is_map(DataOpts) -> ?BU:map_to_json(DataOpts);
                 _ when is_list(DataOpts) -> DataOpts;
                 _ when is_binary(DataOpts) -> jsx:decode(DataOpts)
             end,
    lists:map(fun({K,V}) when is_list(V) ->
                    case lists:all(fun(I) -> is_integer(I) end,V) of
                        true -> {?BU:to_binary(K), case ?BU:to_binary(V) of {error,_,_} -> V; B -> B end};
                        false -> {?BU:to_binary(K), V}
                    end;
                 ({K,V}) when is_atom(V) -> {?BU:to_binary(K), ?BU:to_binary(V)};
                 ({K,V}) -> {?BU:to_binary(K), V}
              end, DataX0).

%% @private
get_script_item(ComponentsData,Variables,#state_scr{loglevel=SMLL}=_StateTag) ->
    ScriptItem = #{checksum => 0,
                   code => <<"expression_sub_component">>,
                   id => <<"d04dd310-0176-385d-5d4d-7cd30a921f58">>,
                   lwt => {{2021,8,3},{20,5,0},0},
                   name => <<"expression_sub_component">>,
                   opts => #{<<"comment">> => <<>>,<<"loglevel">> => SMLL,<<"title">> => <<>>},
                   projectid => <<"00000000-0000-0000-0000-000000000000">>,
                   scriptdata => [{<<"objects">>,[[{<<"oId">>,1},
                                                   {<<"oType">>,101},
                                                   {<<"type">>,0},
                                                   {<<"defineVars">>,1},
                                                   {<<"implementedTime">>,60000},
                                                   {<<"transfer">>,2},
                                                   {<<"name">>,<<"Start">>}]]
                                                 ++ ComponentsData ++
                                                 [[{<<"oId">>,3},
                                                   {<<"oType">>,102},
                                                   {<<"callStack">>,0},
                                                   {<<"name">>,<<"Stop">>}]]},
                                  {<<"variables">>,Variables},
                                  {<<"scriptVersion">>,170105}],
                   t => svcscript},
    {ok,ScriptItem}.

%% -----------------------------------------------------