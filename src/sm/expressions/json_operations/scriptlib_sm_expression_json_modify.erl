%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc

-module(scriptlib_sm_expression_json_modify).

-export([exec/3, exec/4]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

-define(JU, scriptlib_sm_expression_json_utils).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------
%% exec/4
%% --------------
exec(Query, Data, OperationData) ->
    exec(Query, Data, OperationData, <<"modify_json error1">>).

%% --------------
%% exec/5
%% --------------
exec(Query, Data, OperationData, Default) ->
    do_modify(Query, Data, OperationData, Default).


%% ====================================================================
%% Internal functions
%% ====================================================================

do_modify(Query, Data, OperationData, Default) ->
    try jsx:decode(?BU:to_binary(Data)) of
        DecodedData -> do_modify_1(Query, DecodedData, OperationData, Default)
    catch
        E:Error ->
            ?LOG('$error', "~500tp -- modify_json: jsx:decode ERROR: ~500tp", [?MODULE, {E,Error}]),
            Default
    end.

%% -------
do_modify_1(Query, Data, OperationData, Default) when is_list(OperationData); is_binary(OperationData); is_atom(OperationData) ->
    try jsx:decode(?BU:to_binary(OperationData),[return_maps]) of
        DecodedOpData -> do_modify_2(Query, Data, DecodedOpData, Default)
    catch
        E:Error ->
            ?LOG('$error', "~500tp -- modify_json: jsx:decode params ERROR: ~500tp", [?MODULE, {E,Error}]),
            Default
    end;
do_modify_1(Query, Data, OperationData, Default) when is_map(OperationData) ->
    do_modify_2(Query, Data, OperationData, Default).

%% -------
do_modify_2(Query, DecodedData, OperationData, Default) ->
    QueryBin = ?BU:to_binary(Query),
    M = maps:fold(fun(K,V,Acc) -> Acc#{?BU:to_binary(K)=>V} end, #{}, OperationData),
    TVal = erlang:make_ref(),
    [Act] = ?BU:maps_get_default([<<"action">>], M, [TVal]),
    case Act/=TVal of
        true ->
            case ?JU:modify_object(QueryBin, DecodedData, M) of
                {error,_} -> Default;
                Result0 ->
                    case catch jsx:encode(Result0) of
                        {'EXIT',_} -> Default;
                        Result -> Result
                    end
            end;
        false -> Default
    end.
