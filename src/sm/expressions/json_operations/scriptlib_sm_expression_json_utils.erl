%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc
%%% operations for Data type = list:
%%% * prepend(Data::list(),Value::json_term())
%%% * append(Data::list(),Value::json_term())
%%% * insert(Data::list(),index::unsignedint,Value::json_term())
%%% * replace(Data::list(),index::unsignedint,Value::json_term())
%%% * delete(Data::list(),index::unsignedint)
%%%
%%% operations for Data type = dict:
%%% * keyinsert(Data::dict(),key::string(),Value::json_term())
%%% * keyreplace(Data::dict(),key::string,Value::json_term())
%%% * keystore(Data::dict(),key::string(),Value::json_term())
%%% * keydelete(Data::dict(),key::string)

-module(scriptlib_sm_expression_json_utils).

-export([find_object/2,
         modify_object/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

find_object(Query, Data) ->
    do_only_find_object(Query, Data).

modify_object(Query, Data, OperationData) ->
    try do_modify_object(Query, Data, OperationData) of
        {error,Err} ->
            ?LOG('$error', "~500tp -- do_modify_object Search ERROR: ~500tp",[?MODULE,Err]),
            Err;
        Result -> Result
    catch
        _:_Err ->
            ?LOG('$error', "~500tp -- do_modify_object modify ERROR: ~500tp",[?MODULE,_Err]),
            {error,<<"internal error">>}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------
%% MODIFY
%% ----------------------------
%% @private
do_modify_object(Query, Data, OperationData) ->
    ModifyFun = get_modify_fun(OperationData),
    do_modify_object_1(Query, Data, ModifyFun).

%% ---
do_modify_object_1(Query, Data, ModifyFun) ->
    case do_modify_object_2(Query, Data, ModifyFun) of
        {error,_}=Err -> Err;
        {{Res,F}=_Result, []} when is_function(F) -> Res;
        {Result, FunReconstructList} ->
            lists:foldl(fun(FunReconstruct,Acc) -> FunReconstruct(Acc) end, Result, FunReconstructList)
    end.

%% ---
do_modify_object_2(Query, Data, ModifyFun) ->
    case do_find_object(Query, Data) of
        {error,_}=Err -> Err;
        {ResData,FunReconstructList} ->
            Result = ModifyFun(ResData),
            {Result, FunReconstructList}
    end.

%% @private
get_modify_fun(OperationData) when is_map(OperationData) ->
    Act = ?BU:to_binary(maps:get(<<"action">>,OperationData)),
    do_get_modify_fun(Act,OperationData).

%% Operations for JSON LIST
do_get_modify_fun(Act, OperationData)
  when Act== <<"append">>;Act== <<"prepend">>;Act== <<"insert">>;Act== <<"replace">>;Act== <<"replace_auto">> ->
    NewValue0 = maps:get(<<"value">>,OperationData),
    NewValue = prepare_value_type(NewValue0),
    fun(OldVal) when Act== <<"replace_auto">> ->
            IsDict = case OldVal of [] -> false; [I|_] -> is_tuple(I); _ -> false end,
            case is_list(OldVal) andalso IsDict==false of
                true -> lists:map(fun(_AccVal) -> NewValue end,OldVal);
                false -> throw({error,<<"invalid target value type for action ",(Act)/binary>>})
            end;
       (OldVal) ->
            IsDict = case OldVal of [] -> false; [I|_] -> is_tuple(I); _ -> false end,
            case is_list(OldVal) andalso IsDict==false of
                true when Act== <<"append">> ->
                    NRes = OldVal++[NewValue],
                    {NRes,fun(PosList) -> Last = lists:last(PosList), PosList++[Last+1] end};
                true when Act== <<"prepend">> ->
                    NRes = [NewValue|OldVal],
                    {NRes, fun(PosList) -> Hd=hd(PosList),[Hd|[X+1 || X <- PosList]] end};
                true ->
                    Index = ?BU:to_int(maps:get(<<"index">>,OperationData)),
                    CheckIndexRBorder = case Act of <<"insert">> -> Index=<erlang:length(OldVal); <<"replace">> -> Index<erlang:length(OldVal) end,
                    case Index>=0 andalso CheckIndexRBorder of
                        true when Act== <<"insert">> ->
                            {Left,Right} = lists:split(Index, OldVal),
                            lists:append(lists:append(Left,[NewValue]),Right);
                        true when Act== <<"replace">> ->
                            {Left,[_|R1]=_Right} = lists:split(Index, OldVal),
                            lists:append(Left,[NewValue|R1]);
                        true -> throw({error,<<"invalid action ",(Act)/binary>>});
                        false -> throw({error,<<"invalid index (more or less len) for action ",(Act)/binary>>})
                    end;
                false -> throw({error,<<"invalid target value type for action ",(Act)/binary>>})
            end
    end;
do_get_modify_fun(Act, OperationData) when Act== <<"delete">> ->
    fun(OldVal) ->
            IsDict = case OldVal of [] -> false; [I|_] -> is_tuple(I); _ -> false end,
            case is_list(OldVal) andalso IsDict==false of
                true ->
                    Index = ?BU:to_int(maps:get(<<"index">>,OperationData)),
                    case Index>=0 andalso Index=<erlang:length(OldVal)  of
                        true when Act== <<"delete">> ->
                            {Left,[_|R1]=_Right} = lists:split(Index, OldVal),
                            lists:append(Left,R1);
                        true -> throw({error,<<"invalid action ",(Act)/binary>>});
                        false -> throw({error,<<"invalid index (more or less len) for action ",(Act)/binary>>})
                    end;
                false -> throw({error,<<"invalid target value type for action ",(Act)/binary>>})
            end
    end;
%% Operations for JSON DICT
do_get_modify_fun(Act, OperationData)
  when Act== <<"keystore">>;Act== <<"keyreplace">>;Act== <<"keyinsert">> ->
    Key = ?BU:to_binary(maps:get(<<"key">>,OperationData)),
    NewValue0 = maps:get(<<"value">>,OperationData),
    NewValue = prepare_value_type(NewValue0),
    fun(OldValRaw) ->
            OldVal =
                case OldValRaw of
                    [{}] -> [];
                    [{_,_}|_] -> OldValRaw;
                    _ -> throw({error,<<"invalid target value type for action ",(Act)/binary>>})
                end,
            case Act of
                <<"keystore">> ->
                    lists:keystore(Key,1,OldVal,{Key,NewValue});
                <<"keyreplace">> ->
                    lists:keyreplace(Key,1,OldVal,{Key,NewValue});
                <<"keyinsert">> ->
                    case lists:keymember(Key,1,OldVal) of
                        true -> throw({error,<<"invalid operation key already exists for action ",(Act)/binary>>});
                        false -> [{Key,NewValue}|OldVal]
                    end
            end
    end;
do_get_modify_fun(Act, OperationData) when Act== <<"keydelete">> ->
    Key = ?BU:to_binary(maps:get(<<"key">>,OperationData)),
    fun([I|_]=OldVal) when is_tuple(I) ->
            case Act of
                <<"keydelete">> ->
                    lists:keydelete(Key,1,OldVal)
            end;
       (_) -> throw({error,<<"invalid target value type for action ",(Act)/binary>>})
    end;
%% Not declared operations
do_get_modify_fun(_Act, _OperationData) -> throw({error,<<"invalid action ",(_Act)/binary>>}).


%% ---
prepare_value_type(Value) when is_map(Value) ->
    maps:to_list(Value);
prepare_value_type(Value) when is_list(Value) ->
    ?BU:to_binary(Value);
prepare_value_type(Value) when is_binary(Value) ->
    try jsx:decode(Value) of
        DecodedValue -> DecodedValue
    catch
        E:Error ->
            ?LOG('$error', "~500tp -- modify_json: jsx:decode value ERROR: ~500tp", [?MODULE, {E,Error}]),
            throw({error,<<"value must be json_term">>})
    end.

%% ----------------------------
%% FIND
%% ----------------------------
%% @private
do_only_find_object(Query, Data) ->
    case do_find_object(Query, Data) of
        {error,_}=Err -> Err;
        {Result,_FunReconstructList} -> Result
    end.

%% ---
do_find_object(Query, Data) ->
    Parts = binary:split(Query, <<"/">>, [global]),
    F = fun(_,{error,_}=Error) -> Error;
           (<<>>,{_Obj,_FRecs}=Acc)-> Acc;
           (El,{[I|_]=Obj,FRecs}) ->
                IsDict = is_tuple(I),
                case IsDict of
                    true -> handle_do_find_object(find_object_key(El,Obj),FRecs);
                    false -> handle_do_find_object(find_object_index(El,Obj),FRecs)
                end;
           (Key, {_Obj,_FRecs}) -> %% when Object is not list
                Msg1 = <<"wrong param before: '">>, Msg2 = <<"'">>,
                {error, << Msg1/binary, Key/binary, Msg2/binary >>}
        end,
    lists:foldl(F, {Data,[]}, Parts).

% @private
handle_do_find_object({error,_}=Err,_FRecs) -> Err;
handle_do_find_object({Result,FReconstruct},FRecs) -> {Result,[FReconstruct|FRecs]}.

% @private
% if data is JSON Dict
find_object_key(Key, Data) when is_list(Data) ->
    Msg1 = <<"key: '">>, Msg2 = <<"' not found">>,
    Error = {error, << Msg1/binary, Key/binary, Msg2/binary >>},
    Replace = fun(X) -> binary:replace(X, [<<"\"">>,<<"\\">>], <<"">>, [global]) end,
    case binary:match(Key,<<"(">>) of
        nomatch ->
            PKey = Replace(Key),
            case lists:keysearch(PKey, 1, Data) of
                false -> Error;
                {value, {_,Value}} ->
                    FReconstr0 = fun({NewV,_FunPos}) -> lists:keyreplace(PKey, 1, Data, {PKey, NewV});
                                    (NewV) -> lists:keyreplace(PKey, 1, Data, {PKey, NewV})
                                 end,
                    {Value, FReconstr0} %% ceceron
            end;
        {Index,_} ->
            Key1 = binary:part(Key, 0, Index),
            PKey1 = Replace(Key1),
            case lists:keysearch(PKey1, 1, Data) of
                false -> Error;
                {value, {_,Value}} ->
                    FReconstr1 = fun(NewV0) -> lists:keyreplace(PKey1, 1, Data, {PKey1,NewV0}) end, %% ceceron
                    Param = binary:part(Key, Index+1, byte_size(Key)-Index-2),
                    {Res, FReconstr2} = do_find_object_filtered(Param,Value),
                    {Res, fun(NV) -> NV2=FReconstr2(NV), FReconstr1(NV2) end}
            end
    end;
find_object_key(Key, _Data) ->
    Msg1 = <<"wrong param before: '">>, Msg2 = <<"'">>,
    {error, << Msg1/binary, Key/binary, Msg2/binary >>}.

% @private
% if data is JSON List
find_object_index(Key, Data) when is_list(Data) ->
    Msg1 = <<"wrong index: ">>,
    Error = {error, << Msg1/binary, Key/binary >>},
    case catch binary:first(Key)==$( andalso binary:last(Key)==$) of
        {'EXIT',_} -> Error;
        true ->
            Size = byte_size(Key)-2,
            <<$(,RKey:Size/binary,$)>> =Key,
            {Res, FReconstr2} = do_find_object_filtered(RKey, Data),
            {Res, fun(NV) -> FReconstr2(NV) end};
        false ->
            try
                Index = erlang:binary_to_integer(Key),
                F = fun(_, {ok, _, _}=R) -> R;
                       (Obj, Count) -> case Count of Index -> {ok, Obj, Count+1}; _ -> Count + 1 end end,
                case lists:foldl(F, 0, Data) of
                    {ok, Res, Pos} ->
                        FReconstr3 = fun(NewValData) ->
                                             NewVal = case NewValData of
                                                          {NewValX,_FPos} -> NewValX;
                                                          NewValX -> NewValX end,
                                             {Left,Right} = lists:split(Pos, Data),
                                             lists:append(lists:append(lists:droplast(Left),[NewVal]),Right)
                                     end,
                        {Res, FReconstr3};
                    _ -> Error
                end
            catch
                _:_ -> Error
            end
    end;
find_object_index(_A, _B) -> {error, <<"index for object">>}.

% @private
do_find_object_filtered(Param,Value) ->
    Replace = fun(X) -> binary:replace(X, [<<"\"">>,<<"\\">>], <<"">>, [global]) end,
    [Key2,Value2] = binary:split(Param, <<":">>),
    PKey2 = Replace(Key2),
    PValue2 = Replace(Value2),
    F = fun(Obj, {R,PosList,Pos}) ->
                case lists:keysearch(PKey2, 1, Obj) of
                    {value, {_,JValue}} when not is_list(JValue) ->
                        case ?BU:to_binary(JValue) of
                            PValue2 -> {lists:append(R, [Obj]),PosList++[Pos],Pos+1};
                            _ -> {R,PosList,Pos+1}
                        end;
                    _ -> {R,PosList,Pos+1}
                end
        end,
    {Res,PosList,_} = lists:foldl(F, {[],[],1}, Value),
    FReconstr2 = fun(NewValParams) ->
                         {PosListX,NewValList} =
                             case NewValParams of
                                 {NewValList0,FModifyPosList} -> {FModifyPosList(PosList),NewValList0};
                                 NewValList0 -> {PosList,NewValList0}
                             end,
                         {Res1,_}=
                             lists:foldl(fun(Pos,{Acc,[NewVal|T]}) ->
                                                 AccLen1 = erlang:length(Acc)+1,
                                                 case Pos of
                                                     AccLen1 -> {Acc++[NewVal],T};
                                                     -1 -> {[NewVal|Acc],T};
                                                     _ ->
                                                         {Left,Right} = lists:split(Pos, Acc),
                                                         SubRes = lists:append(lists:append(lists:droplast(Left),[NewVal]),Right),
                                                         {SubRes,T}
                                                 end
                                         end, {Value, NewValList}, PosListX),
                         Res1
                 end,
    {Res, FReconstr2}.
