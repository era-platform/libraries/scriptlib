%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc

-module(scriptlib_sm_components).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    find_component_start/1,
    find_component_poststart/2,
    find_component_by_id/2
]).

-export([
    component_init/1,
    component_handle_event/2,
    component_terminate/1
]).

-export([
    get/3, get/2,
    get_file/4, get_file/3
]).

-export([
    next/2
]).

-export([
    event_after/3, event_after/2,
    event/2
]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ==========================================
%% Find component, find module, build
%% ==========================================

%% --------------------------------
%% find first component
%% --------------------------------
-spec find_component_start(State::#state_scr{}) -> false | {ok, Component::#component{}}.
%% --------------------------------
find_component_start(#state_scr{objects=O}=State) ->
    maps:fold(fun(_K,V,false) ->
                    case lists:keyfind(<<"oType">>,1,V) of
                        {_,T} when T == <<"start">> ->
                            Comp = build_component(V,State),
                            log_component(Comp,State),
                            {ok,Comp};
                        _ -> false
                    end;
                 (_,_,Acc) -> Acc
              end, false, O);
find_component_start(State) ->
    ?OUT('$warning', "DEBUG1. State: ~120p", [State]),
    ?OUT('$warning', "DEBUG2. StateX: ~120p", [#state_scr{}]),
    false.

%% --------------------------------
%% find first component of post branch
%% --------------------------------
find_component_poststart(StopReason, #state_scr{objects=O}=State) when is_atom(StopReason) ->
    Res = maps:fold(fun(_,_,{ok,Comp}) -> {ok,Comp};
                       (_K,V,Acc) ->
                            case lists:keyfind(<<"oType">>,1,V) of
                                {_,T} when T == <<"poststart">> ->
                                    Comp = build_component(V,State),
                                    CheckState = State#state_scr{active_component=Comp},
                                    case check_transition(StopReason, CheckState) of
                                        false -> Acc;
                                        true ->
                                            log_component(Comp,State),
                                            {ok,Comp};
                                        default when Acc==false ->
                                            {default,Comp};
                                        _ -> Acc
                                    end;
                                _ -> false
                            end end, false, O),
    case Res of
        {ok,Comp} -> {ok,Comp};
        {default,Comp} -> {ok,Comp};
        false -> false
    end;
find_component_poststart(StopReason, State) ->
    ?OUT('$warning', "DEBUG1. StopReason: ~120tp, State: ~120p", [StopReason, State]),
    ?OUT('$warning', "DEBUG2. StateX: ~120p", [#state_scr{}]),
    false.

%% @private
check_transition(_SomeReason, #state_scr{active_component=#component{module={error,_}}}) -> false;
check_transition(SomeReason, #state_scr{active_component=#component{module=Module}}=State) ->
    case ?BU:function_exported(Module,check_transition,2) of
        false -> false;
        true ->
            case erlang:apply(Module,check_transition,[SomeReason,State]) of
                true -> true;
                _ -> false
            end end;
check_transition(_,_) -> false.

%% --------------------------------
%% find next component by id
%% --------------------------------
find_component_by_id(Id, #state_scr{objects=O}=State) ->
    case maps:get(Id, O, error) of
        error -> false;
        CompData ->
            Comp = build_component(CompData,State),
            log_component(Comp,State),
            {ok, Comp}
    end.

%% --------------------------------
%% @private
log_component(Comp,State) ->
    #component{type=Type,id=Id,module=Module,name=Name} = Comp,
    ?SCR_TRACE(State, "T:~500tp, Id:~500tp, M:~500tp, N:'~ts'", [Type, Id, Module, Name]).

%% --------------------------------
%% @private
%% Mandatory keys for CompData : <<"oId">>,<<"oType">>,<<"name">>
%% --------------------------------
-spec build_component(CompData::list(), State::#state_scr{}) -> Component::#component{}.
%% --------------------------------
build_component(CompData,State) ->
    [Id,Type] = ?BU:extract_required_props([<<"oId">>,<<"oType">>], CompData),
    Name = ?BU:to_binary(?BU:get_by_key(<<"name">>,CompData,Id)),
    #component{id = Id,
               type = Type,
               module = find_component_module(Type,Id,Name,State),
               data = CompData,
               name = Name,
               state = undefined}.

%% @private
find_component_module(Type,Id,Name,#state_scr{meta=#meta{components=_C}}=State)
  when not is_list(_C) orelse _C==[] ->
    find_component_module_static(Type, Id, Name, State);
find_component_module(Type,Id,Name,#state_scr{meta=#meta{components=MetaComp}}=State) ->
    case lists:keyfind(Type, 1, MetaComp) of
        false -> find_component_module_static(Type, Id, Name, State);
        {_,Module} -> check_module({Type, Id, Name, Module})
    end.

%% @private
find_component_module_static(Type, Id, Name, State) ->
    case maps:get(Type, ?CFG:components(), undefined) of
        undefined -> find_component_module_default(Type, Id, Name, State);
        {_,Module} -> check_module({Type, Id, Name, Module})
    end.

%% @private
find_component_module_default(Type, Id, Name, _State) ->
    case maps:get(Type, ?SCR_COMPONENTS_DEFAULT:default_components(), undefined) of
        undefined -> {error, {invalid, ?BU:strbin("Script error. Component type unknown: ~120tp", [Type])}};
        Module when is_atom(Module) -> check_module({Type, Id, Name, Module})
    end.

%% @private
check_module({Type, _, _, Module}) when is_atom(Module) ->
    case code:is_loaded(Module) of
        false ->
            case code:ensure_loaded(Module) of
                {error,_}=Err ->
                    ?OUT('$error', "Check module: ~500tp", [Err]),
                    {error, {invalid, ?BU:strbin("Script error. Component type unknown: ~120tp in ~tp", [Type,Module])}};
                _ -> Module
            end;
        _ -> Module
    end;
check_module({Type, _, _, Module}) ->
    Err = {error, {invalid, ?BU:strbin("Script error. Component type ~120tp module invalid: ~120tp", [Type,Module])}},
    ?OUT('$error', "Check module: ~500tp", [Err]),
    Err.

%% ==========================================
%% Work
%% ==========================================

%% --------------------------------
component_init(#state_scr{active_component=#component{type=Type,module=Module}}=State) -> %% , trace_counter=C
    {FIn,FOut} = get_state_modifiers(Module),
    case ?BU:function_exported(Module,init,1) of
        false -> {error, {invalid, ?BU:strbin("Service error. Component module invalid: ~120tp. Callback 'init' not found",[Module])}};
        true ->
            ?SCRU:make_outer_event('component_start',[Type],State),
            State1 = before_init(Type,State),
            try erlang:apply(Module,init,[FIn(State1)]) of
                {error,_}=Err -> Err;
                {ok,SubState1} -> component_apply_events(after_init(Type,FOut(SubState1, State1)));
                T -> T
            catch
                E:R:StackTrace ->
                    ?SCR_CRASH(State1, "Script process got exception: ~n\tStack: ~200tp", [{E,R,StackTrace}]),
                    {error,R}
            end end.

%% @private
before_init(CompType,#state_scr{meta=#meta{callback_funs=CB}}=State) ->
    case maps:get('fun_before_component',CB,undefined) of
        FunBefore when is_function(FunBefore,2) -> #state_scr{}=FunBefore(CompType,State);
        _ -> State
    end.

%% @private
after_init(CompType,#state_scr{meta=#meta{callback_funs=CB}}=State) ->
    case maps:get('fun_after_init_component',CB,undefined) of
        FunAfterInit when is_function(FunAfterInit,2) -> #state_scr{}=FunAfterInit(CompType,State);
        _ -> State
    end.

%% @private
component_apply_events(#state_scr{queue=[]}=State) -> {ok,State};
component_apply_events(#state_scr{queue=Q}=State) ->
    #state_scr{active_component=#component{module=Module}}=State,
    case ?BU:function_exported(Module,handle_event,2) of
        false -> {ok,State};
        true ->
            Fapply = fun F([],StateX) -> {ok,StateX};
                         F([Ev|Rest],StateX) ->
                            case component_handle_event(Ev,StateX) of
                                {ok,StateX1} -> F(Rest,StateX1);
                                {keep,#state_scr{queue=QX}=StateX1} -> F(Rest,StateX1#state_scr{queue=[Ev|QX]});
                                {next,Id,#state_scr{queue=QX}=StateX1} -> {next,Id,StateX1#state_scr{queue=lists:reverse(Rest)++QX}};
                                {error,_}=Err -> Err
                            end end,
            Fapply(lists:reverse(Q),State#state_scr{queue=[]})
    end.

%% --------------------------------
component_handle_event(Event, #state_scr{active_component=#component{module=Module}}=State) ->
    {FIn,FOut} = get_state_modifiers(Module),
    case ?BU:function_exported(Module,handle_event,2) of
        false -> {error, {<<"Service error. Component module invalid">>}};
        true ->
            try erlang:apply(Module,handle_event,[Event, FIn(State)]) of
                {error,_}=Err -> Err;
                {ok,SubState1} ->
                    {ok,FOut(SubState1,State)};
                {keep,SubState1} ->
                    #state_scr{queue=Q}=State1=FOut(SubState1,State),
                    {ok,State1#state_scr{queue=[Event|Q]}}
            catch
                E:R:StackTrace ->
                    ?SCR_CRASH(State, "Script process got exception: ~n\tStack: ~200tp", [{E,R,StackTrace}]),
                    {error,R}
            end end.

%% --------------------------------
-spec component_terminate(State) -> {ok, State} | {error, Reason::binary()} | {async, State} when State :: #state_scr{}.
%% --------------------------------
component_terminate(#state_scr{active_component=undefined}=State) -> {ok,State};
component_terminate(#state_scr{active_component=#component{type=Type,module=Module}}=State) ->
    {FIn,FOut} = get_state_modifiers(Module),
    State1 = after_component(Type,State),
    case ?BU:function_exported(Module,terminate,1) of
        false -> {ok,State1};
        true ->
            try erlang:apply(Module,terminate,[FIn(State1)]) of
                {error,_}=Err -> Err;
                {ok,SubState1} -> {ok,FOut(SubState1,State1)};
                {async,SubState1} -> {async,FOut(SubState1,State1)}
            catch
                E:R:StackTrace ->
                    ?SCR_CRASH(State1, "Script process got exception: ~n\tStack: ~200tp", [{E,R,StackTrace}]),
                    {error,R}
            end end.

%% @private
after_component(CompType,#state_scr{meta=#meta{callback_funs=CB}}=State) ->
    case maps:get('fun_after_component',CB,2) of
        FunAfter when is_function(FunAfter,2) -> #state_scr{}=FunAfter(CompType,State);
        _ -> State
    end.

%% --------
get_state_modifiers(_Module) ->
    ParamMode = 'native',
    case ParamMode of
        'native' -> {fun(StateF) -> StateF end, fun(StateF,_) -> StateF end};
        'ex' -> {fun make_substate/1, fun store_substate/2}
    end.

%% @private
make_substate(State) ->
    #state_scr{active_component=#component{data=Data,state=CompState},
        variables=Vars,
        domain=Domain,
        type=Type,
        code=Code,
        scriptid=ScriptId,
        loglevel=LogLevel,
        ownerpid=OwnerPid,
        post_branch=PostBranch}=State,
    #{
        data => Data,
        state => CompState,
        variables => Vars,
        domain => Domain,
        script_type => Type,
        script_code => Code,
        sm_id => ScriptId,
        log_level => LogLevel,
        ownerpid => OwnerPid,
        post_branch => PostBranch,
        fun_next => fun() -> ok end, % TODO
        fun_stop => fun() -> ok end, % TODO
        fun_event => fun() -> ok end, % TODO
        fun_event_after => fun() -> ok end % TODO
    }.

%% @private
store_substate(SubState, #state_scr{active_component=AC}=State) ->
    CompState1= maps:get(state,SubState),
    Variables1 = maps:get(variables,SubState),
    State#state_scr{variables=Variables1,active_component=AC#component{state=CompState1}}.

%% ==========================================
%% Get value
%% ==========================================

%% extract property value from component (default if not found)
get(PropertyKey, CompData, Default) when is_binary(PropertyKey) ->
    case lists:keyfind(PropertyKey, 1, CompData) of
        {_,V} -> V;
        false -> Default
    end.

%% extract property value from component (throw if not found)
get(PropertyKey, CompData) when is_binary(PropertyKey) ->
    case lists:keyfind(PropertyKey, 1, CompData) of
        {_,V} -> V;
        false ->
            [Id,Type] = ?BU:extract_required_props([<<"oId">>,<<"oType">>], CompData),
            BId = ?BU:to_binary(Id),
            BType = ?BU:to_binary(Type),
            BProp = ?BU:to_binary(PropertyKey),
            throw({error,{invalid, <<"Script error. Component property error, id='", BId/bitstring, "', type='", BType/bitstring, "', property='", BProp/bitstring, "'">>}})
    end.

%% --------------------------------
%% Get file from property
%% --------------------------------
-spec get_file(PropertyKey::binary(), CompData::list(), Default::term(), State::state_scr()) -> Path::binary()|term().
%% --------------------------------
get_file(PropertyKey, CompData, Default, State) when is_binary(PropertyKey), is_list(CompData) ->
    Value = get(PropertyKey, CompData, Default),
    get_file(Value, Default, State).

%%
get_file(Default, Default, _) -> Default;
get_file(Value, _Default, _State) when is_binary(Value) -> Value; % for api
get_file(Value, Default, State) when is_list(Value) ->
    case lists:keyfind(<<"selectMode">>,1,Value) of
        false -> Default;
        {_,0} ->
            case lists:keyfind(<<"localFile">>,1,Value) of
                false -> Default;
                {_,T} when is_binary(T) ->
                    filename:join([":SYNC_SCRIPT_STATIC", T]) % ?ENVNAMING:get_subpath_script_static(Domain,ScriptType,ScriptId)
            end;
        {_,1} ->
            Folder = case ?SCR_ARG:get_string(<<"folder">>, Value, undefined, State) of
                         undefined -> <<>>;
                         <<>> -> <<>>;
                         T1 when is_binary(T1) -> <<T1/bitstring, "/">>
                     end,
            File = case ?SCR_ARG:get_string(<<"file">>, Value, undefined, State) of
                       undefined -> <<>>;
                       T2 when is_binary(T2) -> T2
                   end,
            <<Folder/binary, File/binary>>
    end;
get_file(_, Default, _) -> Default.

%% ==========================================
%% Next component
%% ==========================================

%% goto next component
next(Id, State) when is_integer(Id) -> {next, Id, State};
%%
next(PropertyKey, State) when is_binary(PropertyKey) -> next([PropertyKey], State);
next([], State) -> ?SCR_WARN(State, "Transfer not set!", []), transfer_not_set; % {error, {invalid, <<"Script error. Transfer not set">>}};
next([Transfer|Rest], State) ->
    #state_scr{active_component=#component{data=CompData}}=State,
    case lists:keyfind(Transfer,1,CompData) of
        {_,Id} when is_integer(Id) -> next(Id, State);
        false -> next(Rest, State);
        _ -> next(Rest, State)
    end.

%% ==========================================
%% Event to scriptmachine
%% ==========================================

event_after(Timeout, Pid, Message) when is_pid(Pid), is_integer(Timeout) ->
    erlang:send_after(Timeout, Pid, {'event', Message}).
event_after(Timeout, Message) when is_integer(Timeout) ->
    erlang:send_after(Timeout, self(), {'event', Message}).

event(Pid, Message) when is_pid(Pid) ->
    Pid ! {'event', Message}.
