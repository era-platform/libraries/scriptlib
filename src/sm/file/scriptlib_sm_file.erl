%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 03.08.2021
%%% @doc

-module(scriptlib_sm_file).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    get_script_temp_directory/1,
    spawn_cleanup_temp_dir/1,
    cleanup_temp_dir/1
]).

-export([
    expand_and_check_path/3,
    get_absolute_path/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("script.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------
%% path to script temp directory for script cleaner
%% --------------------------------
-spec get_script_temp_directory(Pid::pid()) -> LocalTempPath::string().
%% --------------------------------
get_script_temp_directory(Pid) when is_pid(Pid) ->
    filename:join([?CFG:temp_path(), pid_to_dirname(Pid)]).

%% --------------------------------
%% spawned temp directory cleaner/ tries to delete 100 times
%% --------------------------------
-spec spawn_cleanup_temp_dir(Pid::pid()) -> pid().
%% --------------------------------
spawn_cleanup_temp_dir(Pid) ->
    spawn(fun() -> timer:sleep(5000), cleanup_temp_dir(Pid) end).

%% --------------------------------
%% temp directory cleaner
%% --------------------------------
-spec cleanup_temp_dir(Pid::pid()) -> ok.
%% --------------------------------
cleanup_temp_dir(Pid) ->
    TempPath = get_script_temp_directory(Pid),
    ?BU:directory_delete(TempPath).

%% --------------------------------
%% converting pid to suitable filename
%% --------------------------------
pid_to_dirname(Pid) ->
    Str = pid_to_list(Pid),
    lists:sublist(Str, 2, length(Str)-2).

%% ====================================================================
%% API functions.
%% Expand and check path
%% ====================================================================

%% --------------------------------------------------------
-spec expand_and_check_path(ScrPath::binary(), OP :: read | write, State::state_scr())
      -> {ok,ExpPath::binary()} | {error, Reason::term()}.
%% --------------------------------------------------------
expand_and_check_path(Path, _Operation, #state_scr{}=_State) ->
    {ok,Path}.

%% --------------------------------------------------------
get_absolute_path(Path) -> Path.

%% ====================================================================
%% Internal functions
%% ====================================================================