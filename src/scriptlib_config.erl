%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.08.2021
%%% @doc Application options access

-module(scriptlib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    site/0,
    temp_path/0,
    icons_path/0,
    script_type/0,
    meta_module/0
]).

-export([
    components/0,
    expression_functions/0,
    expression_function/2,
    icon/1,
    get_dynamic_list/3,
    get_script/2,
    start_async_script/3,
    storage_put/3,
    storage_get/2,
    storage_delete/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%%
log_destination(Default) ->
    ?BU:get_env(?APP, 'log_destination', Default).

%% current_site
site() ->
    ?BU:get_env(?APP, 'site', <<"main_site">>).

%%
temp_path() ->
    case ?BU:get_env(?APP, 'temp_path', undefined) of
        Value when is_list(Value) -> Value;
        Value when is_binary(Value) -> ?BU:to_list(Value);
        _ -> ?BU:str("/tmp/scripts/~ts",[node()])
    end.

%%
icons_path() ->
    case ?BU:get_env(?APP, 'icons_path', undefined) of
        Value when is_list(Value) -> Value;
        Value when is_binary(Value) -> ?BU:to_list(Value);
        _ ->
            PrivDir = code:priv_dir(?APP),
            filename:join([PrivDir,"icons"])
    end.

%%
script_type() ->
    ?BU:get_env(?APP, 'script_type', undefined).

%%
meta_module() ->
    ?BU:get_env(?APP, 'meta_module', undefined).

%% ====================================================================
%% API functions
%% Build in meta-module functions
%% ====================================================================

%%
components() ->
    case meta(components,[],#{}) of
        Map when is_map(Map) -> Map;
        _ -> #{}
    end.

%%
expression_functions() ->
    case meta(expression_functions,[],[]) of
        List when is_list(List) -> List;
        _ -> []
    end.

%%
expression_function(FunName,Arity) ->
    case meta(expression_function,[FunName,Arity],undefined) of
        Fun when is_function(Fun,Arity) -> Fun;
        _ -> undefined
    end.

%%
icon(Key) ->
    case meta(icon,[Key],undefined) of
        V when is_binary(V) -> V;
        _ -> undefined
    end.

%%
get_dynamic_list(Domain,ScriptType,Key) ->
    %?BU:get_env(?APP, 'get_dynamic_list_function', undefined).
    case meta(get_dynamic_list,[Domain,ScriptType,Key],undefined) of
        undefined -> undefined;
        V -> V
    end.

%%
get_script(Domain,Code) ->
    case meta(get_script,[Domain,Code],undefined) of
        V when is_map(V) -> V;
        _ -> undefined
    end.

%%
start_async_script(Domain,Code,Variables) ->
    case meta(start_async_script,[Domain,Code,Variables],undefined) of
        V when is_map(V) -> V;
        _ -> undefined
    end.

%%
storage_put(Key,Value,TTLms) ->
    %?BU:get_env(?APP, 'put_storage_function', undefined).
    case meta(storage_put,[Key,Value,TTLms],error) of
        undefined -> error;
        V -> V
    end.

%%
storage_get(Key,Default) ->
    %?BU:get_env(?APP, 'get_storage_function', undefined).
    case meta(storage_get,[Key,Default],error) of
        undefined -> error;
        V -> V
    end.

%%
storage_delete(Key) ->
    %?BU:get_env(?APP, 'delete_storage_function', undefined).
    case meta(storage_delete,[Key],error) of
        undefined -> error;
        V -> V
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
meta(FunName,Args,Default) ->
    case meta_module() of
        undefined -> Default;
        MetaModule ->
            case ?BU:function_exported(MetaModule,FunName,length(Args)) of
                false -> Default;
                true -> erlang:apply(MetaModule,FunName,Args)
            end end.