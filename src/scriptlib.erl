%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.08.2021
%%% @doc pglib application (connector and connection pool).
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'script','scriptlib'}
%%%      temp_path
%%%          temporary data with auto delete after script finished is located there in sub folder named as pid
%%%          Default: "/tmp/scripts/NODE/
%%%      icons_path
%%%          used to search icons for components
%%%          Default: "PRIV/icons"
%%%      script_type
%%%          defines used script type (external knowledge). Used to filter options of components when building metadata.
%%%          Default: undefined (means no filters used when building metadata)
%%%      meta_module
%%%          External module desicription of ext functions
%%%          Default: undefined (means no external functions used)
%%%          Exported functions:
%%%             components() -> Map::map() when Map contains Key::binary() -> Module::atom(). Ex. #{<<"start">> => plugin_module_start, <<"stop">> => plugin_module_stop}. (If not exported then no external components)
%%%             expression_functions() -> [Section::map()] when Section contains code::binary(),name::binary(),groups::[Group::map()], Group contains code::binary(),name::binary(),functions::[Fun::map()], Fun contains name::binary(),params::binary().
%%%             expression_function(FunName,Arity) -> function() | undefined. Default undefined.
%%%             icon(Key) -> Base64::binary().
%%%             meta_expr() -> [{FunName::binary() | string(), ArgTypes::binary() | string()}]. (Expression functions that should be mocked when expression counter works).
%%%             get_dynamic_list(Domain,ScriptType,Key) -> []. (Returns dynamic list by key (for properties of non-built-in components). If not exported then no external dynamic lists).
%%%             get_script(Domain,Code) -> {ok,ScriptMap::map()} | false | {error,Reason::term()} | undefined. Default undefined.
%%%             start_async_script(Domain,Code,Variables) -> {ok,Info::map()} | {error,Reason::term()} | undefined. Default undefined. Info contains 'id'.
%%%             storage_put(Key,Value,TTLms) -> ok | error. (Default: undefined. Stores value with autodelete timer. If not exported then no storage put opreation).
%%%             storage_get(Key,Value,TTLms) -> (Key,Default) -> {Key,Value} | false | error. (Returns item from storage by key. If not exported then no storage get opreation).
%%%             storage_delete(Key) -> ok | error. (Deletes item from storage by key. If not exported then no storage delete opreation).

-module(scriptlib).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

% application
-export([start/0,
         stop/0]).

% application
-export([start/2,
         stop/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -----------------------------------------------------
%% LOADER FACADE
%% -----------------------------------------------------

%% -------------------------------------
%% Starts application
%% -------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% -------------------------------------
%% Stops application
%% -------------------------------------
stop() -> application:stop(?APP).

%% ===================================================================
%% Application callback functions
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup opts of dependency apps
%% --------------------------------------
setup_dependencies() -> ok.

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent),
    ok.